create table departamentos(
	dep_codigo int not null primary key,
	dep_nombre varchar(200) not null
);

create table municipios(
	mun_codigo int not null primary key,
	mun_nombre varchar(200) not null,
	dep_codigo int not null,
	foreign key (dep_codigo) references departamentos(dep_codigo)
	on delete no action
	on update cascade 
);

create table categoria(
	cat_codigo int not null primary key,
	cat_nombre varchar(200) not null
)

--INSERT INTO categoria VALUES('1','No Aplica');
--INSERT INTO categoria VALUES('2','Concejal');
--INSERT INTO categoria VALUES('3','Alcalde');
--INSERT INTO categoria VALUES('4','Empresario');
--INSERT INTO categoria VALUES('5','Candidato');

create table usuario(
	usu_codigo int not null primary key,
	usu_login varchar(100) not null,
	usu_nombre varchar(200) not null,
	usu_email varchar(100),
	usu_password varchar(70) not null,
	usu_nivel_acceso varchar(1) not null,
	usu_fecha_creacion date not null,
	usu_ruta_imagen varchar(100),
	usu_activo varchar(1) not null,
	mun_codigo int not null,
	cat_codigo int not null,
	usu_nota varchar(200),
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,
	foreign key (cat_codigo) references categoria(cat_codigo)
	on delete no action
	on update cascade
);

--INSERT INTO usuario VALUES('1','admin','admin','','240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9','S','2017-08-15','','S',22,'1','');

create table tipo_documento(
	tdo_codigo int not null primary key,
	tdo_nombre varchar(200)
)

--INSERT INTO tipo_documento VALUES('1','Cédula de Ciudadania');
--INSERT INTO tipo_documento VALUES('2','Cédula de Extranjería');
--INSERT INTO tipo_documento VALUES('3','NIT');
--INSERT INTO tipo_documento VALUES('4','Otro');

create table personas(
	per_codigo int not null primary key,
	per_ruta_foto varchar(200),
	tdo_codigo int not null,
	per_identificacion int not null,
	per_nombre varchar(200) not null,	
	per_apellido varchar(200) not null,	
	per_fecha_nacimiento date,
	mun_codigo int not null,
	per_estatura double,
	per_gsrh varchar(5),
	per_genero varchar(1) not null,
	per_fecha_expedicion date,		
	per_estado_civil varchar(2) not null,
	per_tipo_direccion varchar(2) not null,
	per_direccion_1 varchar(100),
	per_direccion_2 varchar(100),
	per_direccion_3 varchar(100),
	per_complemento_direccion varchar(200),
	per_barrio varchar(200) not null,		
	per_telefono_fijo varchar(50) ,
	per_celular varchar(50),
	per_email varchar(100),
	per_notas varchar(200),		
	per_ruta_cedula varchar(200),
	per_fecha_registro date not null,
	usu_codigo_registra int not null,
	per_pistola varchar(2) not null,
	foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
	on delete no action
	on update cascade,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
);

create table lider_personas(
	lpe_codigo int not null primary key,
	per_codigo int not null,
	lpe_fecha_registro datetime not null,
	usu_codigo int not null,	
	foreign key (per_codigo) references personas(per_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo) references usuario(usu_codigo)
	on delete no action
	on update cascade
)

create table calendario(
	cal_codigo int not null primary key,
	cal_fecha date not null,
	cal_hora_inicio time not null,
	cal_hora_fin time not null,
	cal_titulo varchar(100) not null,
	cal_comentario varchar(300),
	mun_codigo int not null,
	cal_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
)

create table coordinadores(
	coo_codigo int not null primary key,
	coo_ruta_foto varchar(200),
	tdo_codigo int not null,
	coo_identificacion int not null,
	coo_nombre varchar(200) not null,	
	coo_apellido varchar(200) not null,		
	coo_tipo_coordinador varchar(2) not null,
	mun_codigo int not null,	
	coo_tipo_direccion varchar(2) not null,
	coo_direccion_1 varchar(100),
	coo_direccion_2 varchar(100),
	coo_direccion_3 varchar(100),
	coo_complemento_direccion varchar(200),
	coo_barrio varchar(200) not null,		
	coo_telefono_fijo varchar(50) ,
	coo_celular varchar(50),
	coo_email varchar(100),
	coo_notas varchar(200),		
	coo_ruta_cedula varchar(200),
	coo_fecha_registro date not null,
	usu_codigo_registra int not null,	
	foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
	on delete no action
	on update cascade,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
);

create table gestionadores(
	ges_codigo int not null primary key,
	ges_ruta_foto varchar(200),
	tdo_codigo int not null,
	ges_identificacion int not null,
	ges_nombre varchar(200) not null,	
	ges_apellido varchar(200) not null,			
	mun_codigo int not null,	
	ges_tipo_direccion varchar(2) not null,
	ges_direccion_1 varchar(100),
	ges_direccion_2 varchar(100),
	ges_direccion_3 varchar(100),
	ges_complemento_direccion varchar(200),
	ges_barrio varchar(200) not null,		
	ges_telefono_fijo varchar(50) ,
	ges_celular varchar(50),
	ges_email varchar(100),
	ges_notas varchar(200),		
	ges_ruta_cedula varchar(200),
	ges_fecha_registro date not null,
	usu_codigo_registra int not null,	
	coo_codigo int not null,
	foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
	on delete no action
	on update cascade,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (coo_codigo) references coordinadores(coo_codigo)
	on delete no action
	on update cascade
);

create table numero_formularios(
	for_codigo int not null primary key,
	for_fecha date  not null,
	for_numero int not null,
	for_comentario varchar(1000),
	for_fecha_registro date not null,
	usu_codigo_registra int not null,	
	ges_codigo int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (ges_codigo) references gestionadores(ges_codigo)
	on delete no action
	on update cascade
)

create table entrega_formularios(
	efo_codigo int not null primary key,
	efo_fecha date not null,
	efo_numero int not null,
	efo_firnas int not null,
	efo_confirmadas int not null,	
	efo_error int not null,
	efo_pago varchar(1) not null,
	efo_comentario varchar(1000),
	efo_fecha_registro date not null,
	usu_codigo_registra int not null,	
	for_codigo int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (for_codigo) references numero_formularios(for_codigo)
	on delete no action
	on update cascade
)

ALTER TABLE `coordinadores` ENGINE=InnoDB;
ALTER TABLE `gestionadores` ENGINE=InnoDB;
ALTER TABLE `numero_formularios` ENGINE=InnoDB;
ALTER TABLE `entrega_formularios` ENGINE=InnoDB;

--INICIO CORDINADORES
ALTER TABLE coordinadores ADD foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
on delete no action
on update cascade;

ALTER TABLE coordinadores ADD foreign key (mun_codigo) references municipios(mun_codigo)
on delete no action
on update cascade;

ALTER TABLE coordinadores ADD foreign key (usu_codigo_registra) references usuario(usu_codigo)
on delete no action
on update cascade;
--FIN CORDINADORES

--INICIO GESTIONADORES
ALTER TABLE gestionadores ADD foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
on delete no action
on update cascade;

ALTER TABLE gestionadores ADD  foreign key (mun_codigo) references municipios(mun_codigo)
on delete no action
on update cascade;

ALTER TABLE gestionadores ADD  foreign key (usu_codigo_registra) references usuario(usu_codigo)
on delete no action
on update cascade;

ALTER TABLE gestionadores ADD  foreign key (coo_codigo) references coordinadores(coo_codigo)
on delete no action
on update cascade;
--FIN GESTIONADORES

--INICIO NUMERO FORMULARIOS
UPDATE numero_formularios SET usu_codigo_registra = '1' WHERE usu_codigo_registra = '0';

ALTER TABLE numero_formularios ADD foreign key (usu_codigo_registra) references usuario(usu_codigo)
on delete no action
on update cascade;

DELETE FROM numero_formularios WHERE ges_codigo not in (select ges_codigo from gestionadores);

ALTER TABLE numero_formularios ADD foreign key (ges_codigo) references gestionadores(ges_codigo)
on delete no action
on update cascade;
--FIN NUMERO FORMULARIOS

--INICIO ENTREGA FORMULARIOS
ALTER TABLE entrega_formularios ADD foreign key (usu_codigo_registra) references usuario(usu_codigo)
on delete no action
on update cascade;

DELETE FROM entrega_formularios WHERE for_codigo NOT IN (select for_codigo
from numero_formularios );

ALTER TABLE entrega_formularios ADD foreign key (for_codigo) references numero_formularios(for_codigo)
on delete no action
on update cascade;
--FIN ENTREGA FORMULARIOS

create table permisos(
	pmi_codigo int not null primary key,
	pmi_opcion varchar(3) not null,
	pmi_select varchar(1) not null,
	pmi_insert varchar(1) not null,
	pmi_update varchar(1) not null,
	pmi_delete varchar(1) not null,
	usu_codigo int not null,
	usu_codigo_registra int not null,
	foreign key (usu_codigo) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
) ENGINE=InnoDB;

alter table calendario add cal_identificacion varchar(10) null;
alter table calendario add cal_nombre varchar(100) null;
alter table calendario add cal_telefono varchar(100) null;



alter table personas add per_puesto varchar(100) null;
alter table personas add per_mesa_votacion varchar(100) null;

alter table personas add per_tipo varchar(2);
UPDATE personas SET per_tipo = "VT";
ALTER TABLE personas CHANGE per_tipo  per_tipo varchar(2) not null;

alter table personas add mun_codigo_exp int not null;
UPDATE personas SET mun_codigo_exp = '22';
ALTER TABLE personas ADD foreign key (mun_codigo_exp) references municipios(mun_codigo)
on delete no action
on update cascade;

create table paises(
	pai_codigo varchar(3) not null primary key,
	pai_nombre varchar(100) not null
) ENGINE=InnoDB;

--INSERTAR BD 

alter table personas add mun_codigo_res int not null;
UPDATE personas SET mun_codigo_res = '22';
ALTER TABLE personas ADD foreign key (mun_codigo_res) references municipios(mun_codigo)
on delete no action
on update cascade;

alter table personas add pai_codigo varchar(3) not null;
UPDATE personas SET pai_codigo = 'COL';
ALTER TABLE personas ADD foreign key (pai_codigo) references paises(pai_codigo)
on delete no action
on update cascade;

alter table personas ADD per_recomendado varchar(100) null;
alter table personas ADD per_credencial_numero varchar(100) null;

alter table personas add per_facebook varchar(100);
alter table personas add per_twiter varchar(100);
alter table personas add per_instagram varchar(100);
alter table personas add per_wsp varchar(100);

create table profesiones(
	pro_codigo int not null primary key,
	pro_nombre varchar(300) not null
) ENGINE=InnoDB;

insert into profesiones values('-1', 'no aplica');

--INSERTAR EN BD LAS profesiones

alter table personas add pro_codigo int not null;
UPDATE personas SET pro_codigo = '-1';
ALTER TABLE personas ADD foreign key (pro_codigo) references profesiones(pro_codigo)
on delete no action
on update cascade;

insert into municipios values('1142','CORDOBA','52');
insert into municipios values('1143','ANCUYA','52');
insert into municipios values('1144','COLON','86');
insert into municipios values('1145','SANTA CRUZ GUACHAVES','52');
insert into municipios values('1146','BELEN','52');
insert into municipios values('1147','VALLE DEL GUAMUES','86');
insert into municipios values('1148','BUESACO','52');
insert into municipios values('1149','CONTADERO','52');
insert into municipios values('1150','CUMBAL','52');
insert into municipios values('1151','SOTOMAYOR','52');

create table reuniones(
	reu_codigo int not null primary key,
	reu_titulo varchar(200) not null,
	reu_descripcion varchar(1000) null, 
	reu_fecha date not null,
	reu_hora time not null,
	mun_codigo int not null,
	reu_fecha_registro date not null,
	usu_codigo_registra int not null,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update CASCADE
)ENGINE=InnoDB;

create table reuniones_personas(
	rpe_codigo int not null primary key,
	reu_codigo int not null,
	per_codigo int not null,
	rpe_fecha_registro date not null,
	usu_codigo_registra int not null,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (reu_codigo) references reuniones(reu_codigo)
	on delete no action
	on update cascade,
	foreign key (per_codigo) references personas(per_codigo)
	on delete no action
	on update cascade
) ENGINE=InnoDB;

create table funciones(
fun_codigo int not null primary key,
fun_nombre varchar(200) not null
) ENGINE=InnoDB;

--ALTER TABLE funciones engine = InnoDB

insert into funciones values 
('1', 'Ningua'),
('2', 'Concejal'),
('3', 'Ex-Concejal'),
('4', 'Alcalde'),
('5', 'Ex-Alcalde'),
('6', 'Empresario'),
('7', 'Agricultor'),
('8', 'Lider'),
('20', 'Otro');

alter table personas add fun_codigo int not null;
UPDATE personas SET fun_codigo = '1';
ALTER TABLE personas ADD foreign key (fun_codigo) references funciones(fun_codigo)
on delete no action
on update cascade;

create table anotaciones(
	ano_codigo int not null PRIMARY key,
	ano_titulo varchar(200) null,
	ano_comentario varchar(500) null,
	ano_fecha_hora datetime not null,
	ano_fecha_alerta date null,
	ano_hora time null,
	ano_atendida varchar(1) not null,
	usu_codigo_registra int not null,	
	per_codigo int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (per_codigo) references personas(per_codigo)
	on delete no action
	on update cascade
)ENGINE=InnoDB;

/*----------------------------------------------------------------NUEVO CODERAPP 2017_11_15*/
/*----------------------------------------------------------------NUEVO MYRIAM PAREDES 2017_11_15*/
