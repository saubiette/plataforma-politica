create table usuario(
usu_codigo int not null primary key,
usu_login varchar(100) not null,
usu_nombre varchar(200) not null,
usu_email varchar(100),
usu_password varchar(70) not null,
usu_nivel_acceso varchar(1) not null,
usu_fecha_creacion date not null,
usu_ruta_imagen varchar(100),
usu_activo varchar(1) not null,
usu_codigo_depende int not null
);

create table departamentos(
	dep_codigo int not null primary key,
	dep_nombre varchar(200) not null
);

create table municipios(
	mun_codigo int not null primary key,
	mun_nombre varchar(200) not null,
	dep_codigo int not null,
	foreign key (dep_codigo) references departamentos(dep_codigo)
	on delete no action
	on update cascade 
);

create table tipo_documento(
	tdo_codigo int not null primary key,
	tdo_nombre varchar(200)
)

create table oficinas(
	ofi_codigo int not null primary key,
	ofi_nombre varchar(300) not null,
	ofi_presupuesto double not null,
	ofi_direccion varchar(200), 
	ofi_telefono varchar(100),
	usu_codigo int not null,
	foreign key (usu_codigo) references usuario(usu_codigo)
	on delete no action
	on update cascade
)

create table clientes(
	cli_codigo int not null primary key,
	tdo_codigo int not null,
	cli_identificacion int not null,
	cli_nombre varchar(200) not null,	
	cli_fecha_nacimiento date,
	cli_fecha_expedicion date,	
	cli_ruta_foto varchar(200),
	cli_genero varchar(1) not null,
	cli_estado_civil varchar(2) not null,
	cli_tipo_direccion varchar(2) not null,
	cli_direccion_1 varchar(100),
	cli_direccion_2 varchar(100),
	cli_direccion_3 varchar(100),
	cli_complemento_direccion varchar(200) not null,
	cli_barrio varchar(200) not null,	
	mun_codigo int not null,
	cli_telefono_fijo varchar(50) not null,
	cli_celular varchar(50) not null,
	cli_email varchar(100),
	cli_notas varchar(200),
	usu_codigo int not null,
	cli_referido varchar(200),
	cli_ruta_cedula varchar(200),
	cli_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
	on delete no action
	on update cascade,
	foreign key (mun_codigo) references municipios(mun_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
)

create table tipo_inmueble(
	tin_codigo int not null primary key,
	tin_nombre varchar(200) not null
)

create table inmuebles(
	 inm_codigo int not null primary key,
	 tin_codigo int not null,
	 cli_codigo int not null,
	 inm_area varchar(200),
	 inm_avaluo_comercial double,
	 inm_cupo_credito double,
	 inm_tipo_direccion varchar(2) not null,	 
	 inm_direccion_1 varchar(100),
	 inm_direccion_2 varchar(100),
	 inm_direccion_3 varchar(100),
	 inm_complemento_direccion varchar(200),
	 inm_barrio varchar(200),
	 mun_codigo int not null, 
	 inm_numero_matricula_inmobiliaria varchar(200),
	 inm_numero_predial varchar(200),
	 inm_limitaciones_predio varchar(300),
	 inm_numero_escritura varchar(200),
	 inm_fecha_escritura date,
	 inm_notaria varchar(200),
	 inm_descripcion_inmueble varchar(100),
	 inm_fecha_registro date not null,
	 usu_codigo_registra int not null,
	 foreign key (tin_codigo) references tipo_inmueble(tin_codigo)
	 on delete no action
	 on update cascade,
	 foreign key (cli_codigo) references clientes(cli_codigo)
	 on delete no action
	 on update cascade,
	 foreign key (mun_codigo) references municipios(mun_codigo)
	 on delete no action
	 on update cascade,
	 foreign key (usu_codigo_registra) references usuario(usu_codigo)
	 on delete no action
	 on update cascade
)

create table tipo_plan(
	tpl_codigo int not null primary key,
	tpl_nombre varchar(200)
)

create table creditos(
	cre_codigo int not null primary key,
	ofi_codigo int not null,
	inm_codigo int not null,
	cre_fecha_desembolso date not null,
	cre_fecha_pago_cuota date not null,
	cre_valor_desembolso double not null,
	tpl_codigo int not null,
	cre_tasa_interes_pactada double not null,
	cre_tasa_interes_mora double not null,
	cre_tiempo_pactado int,	
	cre_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (ofi_codigo) references oficinas(ofi_codigo)
	on delete no action
	on update cascade,
	foreign key (inm_codigo) references inmuebles(inm_codigo)
	on delete no action
	on update cascade,
	foreign key (tpl_codigo) references tipo_plan(tpl_codigo)
	on delete no action
	on update cascade,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
)

create table datos_laborales(
	dla_codigo int not null primary key,
	dla_profesion varchar(200),
	dla_nombre_empresa varchar(200),
	dla_direccion varchar(200),
	dla_telefono varchar(100),
	dla_cargo varchar(100),
	dla_antiguedad_laboral varchar(100),
	dla_salario double,
	dla_otros_ingresos double,
	dla_ciudad varchar(100),
	dla_nota varchar(200),
	cli_codigo int not null,
	dla_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (cli_codigo) references clientes(cli_codigo)
	on delete no action
	on update cascade
)

create table referencias(
	ref_codigo int not null primary key,
	ref_tipo varchar(1) not null,
	ref_nombre varchar(200),
	ref_parentesco varchar(100),
	ref_direccion varchar(200),
	ref_telefono varchar(200),
	ref_celular varchar(200),
	ref_ciudad varchar(100),
	ref_nota varchar(100),
	cli_codigo int not null,
	ref_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (cli_codigo) references clientes(cli_codigo)
	on delete no action
	on update cascade
)

create table referencias_comerciales(
	rco_codigo int not null primary key,
	rco_empresa varchar(200),
	rco_direccion varchar(200),
	rco_telefono varchar(200),
	rco_celular varchar(200),	
	rco_ciudad varchar(100),
	rco_nota varchar(100),
	cli_codigo int not null,
	rco_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade,
	foreign key (cli_codigo) references clientes(cli_codigo)
	on delete no action
	on update cascade
)

create table copropietarios(
	cop_codigo int not null primary key,
	inm_codigo int not null,
	tdo_codigo int not null,
	cop_identificacion int not null,
	cop_nombre varchar(200) not null,
	cop_ruta_foto varchar(200),	
	cop_ruta_cedula varchar(200),
	cop_fecha_registro date not null,
	usu_codigo_registra int not null,
	foreign key (tdo_codigo) references tipo_documento(tdo_codigo)
	on delete no action
	on update cascade,
	foreign key (inm_codigo) references inmuebles(inm_codigo)
	on delete no action
	on update cascade,	
	foreign key (usu_codigo_registra) references usuario(usu_codigo)
	on delete no action
	on update cascade
)