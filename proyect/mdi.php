<?php require './generalesPHP/cerrarSesion.php';?>

<!DOCTYPE html>
<html>
<?php require_once "./_mdi/1_header.php";  ?>

<body class="hold-transition skin-purple sidebar-mini">

  <?php if(isset($_SESSION['usu_codigo'])): ?>

  <script>
    var usu_codigo = "<?php echo $_SESSION['usu_codigo']; ?>";
    var con_codigo = "<?php echo $_SESSION['con_codigo']; ?>";
    var tipUsuario = "<?php echo $_SESSION["usu_nivel_acceso"]; ?>";
  </script>

  <?php    endif;  ?>

  <div class="wrapper">

    <?php require "./_mdi/2_encabezado.php"; ?>
    <?php require "./_mdi/3_menu.php"; ?>

    <!-- Contenedor Principal -->
    <div class="content-wrapper"></div>
    <!--Ventana Modal-->
    <div class="VenMod"></div>

    <?php require "./_mdi/4_footer.php"; ?>
    <?php //require "./_mdi/5_conf_usuario.php"; ?>

    <!-- Añadir el fondo de la barra lateral. 
             Esta div debe colocarse inmediatamente después de la barra lateral de control -->
    <div class="control-sidebar-bg"></div>

  </div>
  <!-- ./wrapper -->

  <!--VENTANA MODAL DE SUSPENCIÓN-->
  <div id="modSuspender" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <?php //require "./venMod/vmSuspender/index.php"; ?>
      </div>
    </div>
  </div>

  <?php require_once "./_mdi/plugins.php"; ?>

</body>

</html>