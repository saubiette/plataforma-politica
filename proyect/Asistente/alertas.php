<?php
include "../../php/conexion.php";

$conexion = new clsConexion();
session_start();

//--> PARA AUTOMOTORES
$sql = "SELECT   automotores.ide_automotor, aut_placa, tipo_documentos_ac.ide_tipo_documento_ac, MAX(doa_fecha_fin_vigencia) as doa_fecha_fin_vigencia, tdac_nombre, tdac_dias_alertar
        FROM 
                 automotores INNER JOIN
                 documentos_automotores ON automotores.ide_automotor = documentos_automotores.ide_automotor INNER JOIN
                 tipo_documentos_ac ON documentos_automotores.ide_tipo_documento_ac = tipo_documentos_ac.ide_tipo_documento_ac
        WHERE    automotores.ide_empresa = '".$_SESSION['ide_empresa']."' and tdac_dias_alertar <> '0'
        GROUP BY automotores.ide_automotor, aut_placa, tipo_documentos_ac.ide_tipo_documento_ac
        ORDER BY doa_fecha_fin_vigencia DESC";

$aletas = $conexion->prepare($sql);
$aletas->execute();

$array = $aletas->fetchAll(PDO::FETCH_ASSOC);

$items = "";
$contItemas = 0;

foreach($array as $key => $value){

    $hoy = date("d-m-Y");
    $fecha_actual = strtotime($hoy);

    $fechaCrear = date_create($value['doa_fecha_fin_vigencia']);
    $fechaCrear = date_format($fechaCrear, 'd-m-Y');
    $fecha_comparar = strtotime($fechaCrear);

    if($fecha_actual >= $fecha_comparar){
        //$items .= "<span style='background:red' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>".$value['aut_placa']." - ".$value['tdac_nombre']."</span><br>";
        
        $items .="<li data-placa='".$value['aut_placa']."' data-contenedor='conAutomotorDocumentos' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>
                    <a href='#'>
                      <i class='fa fa-bell text-red'></i> ".$value['aut_placa']." - ".$value['tdac_nombre']."
                    </a>
                  </li> ";

        $contItemas++;
        continue ;
    }

    $tdac_dias_alertar = $value['tdac_dias_alertar'];
    $fechaAumenta = strtotime("+".$tdac_dias_alertar." day", strtotime($hoy));
    //$nuevafecha = date ( 'Y-m-j' , $fechaAumenta );

    if($fechaAumenta >= $fecha_comparar){
        //$items .= "<span style='background:yellow' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>".$value['aut_placa']." - ".$value['tdac_nombre']."</span><br>";
        
        $items .="<li data-placa='".$value['aut_placa']."' data-contenedor='conAutomotorDocumentos' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>
                    <a href='#'>
                      <i class='fa fa-bell text-yellow'></i> ".$value['aut_placa']." - ".$value['tdac_nombre']."
                    </a>
                  </li> ";

        $contItemas++;
        continue ;
    }    

}

//--> PARA CONDUCTORES
$sql = "SELECT   conductores.ide_conductor, con_nombre, con_apellido, tipo_documentos_ac.ide_tipo_documento_ac, MAX(doc_fecha_fin_vigencia) as doc_fecha_fin_vigencia, tdac_nombre, tdac_dias_alertar
        FROM 
                conductores INNER JOIN
                documentos_conductores ON conductores.ide_conductor = documentos_conductores.ide_conductor INNER JOIN
                tipo_documentos_ac ON documentos_conductores.ide_tipo_documento_ac = tipo_documentos_ac.ide_tipo_documento_ac
        WHERE    conductores.ide_empresa = '".$_SESSION['ide_empresa']."' and tdac_dias_alertar <> '0'
        GROUP BY conductores.ide_conductor, con_nombre, tipo_documentos_ac.ide_tipo_documento_ac
        ORDER BY doc_fecha_fin_vigencia DESC";

$aletas = $conexion->prepare($sql);
$aletas->execute();

$array = $aletas->fetchAll(PDO::FETCH_ASSOC);

foreach($array as $key => $value){

    $hoy = date("d-m-Y");
    $fecha_actual = strtotime($hoy);

    $fechaCrear = date_create($value['doc_fecha_fin_vigencia']);
    $fechaCrear = date_format($fechaCrear, 'd-m-Y');
    $fecha_comparar = strtotime($fechaCrear);

    if($fecha_actual >= $fecha_comparar){
        //$items .= "<span style='background:red' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>".$value['aut_placa']." - ".$value['tdac_nombre']."</span><br>";
        
        $items .="<li data-nombre='".$value['con_apellido']." ".$value['con_nombre']."' data-contenedor='conConductoresDocmuentos' data-ide_conductor='".$value['ide_conductor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>
                    <a href='#'>
                      <i class='fa fa-bell text-red'></i> ".$value['con_apellido']." ".$value['con_nombre']." - ".$value['tdac_nombre']."
                    </a>
                  </li> ";

        $contItemas++;
        continue ;
    }

    $tdac_dias_alertar = $value['tdac_dias_alertar'];
    $fechaAumenta = strtotime("+".$tdac_dias_alertar." day", strtotime($hoy));
    //$nuevafecha = date ( 'Y-m-j' , $fechaAumenta );

    if($fechaAumenta >= $fecha_comparar){
        //$items .= "<span style='background:yellow' data-ide_automotor='".$value['ide_automotor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>".$value['aut_placa']." - ".$value['tdac_nombre']."</span><br>";
        
        $items .="<li data-nombre='".$value['con_apellido']." ".$value['con_nombre']."' data-contenedor='conConductoresDocmuentos' data-ide_conductor='".$value['ide_conductor']."' data-ide_tipo_documento_ac='".$value['ide_tipo_documento_ac']."'>
                    <a href='#'>
                      <i class='fa fa-bell text-yellow'></i> ".$value['con_apellido']." ".$value['con_nombre']." - ".$value['tdac_nombre']."
                    </a>
                  </li> ";

        $contItemas++;
        continue ;
    }    

}

$structura = "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                <i class='fa fa-bell-o'></i>
                <span class='label label-warning'>$contItemas</span>
              </a>
              <ul class='dropdown-menu'>
                <li class='header'>Tiene $contItemas alertas</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class='menu itemNotificaciones'>                 
                        $items                     
                    </ul>
                </li>
                
             </ul>";

echo $structura;

?>