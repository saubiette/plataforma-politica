// -----------------------------> SELECCIONAR CONTENEDOR <-----------------------------//
// función que actualiza el zoom del elemento body
function actualizarTama() {
  // alert(window.innerWidth )
  $('body').css('zoom', window.innerWidth / 1548)
}

$(document).ready(function () {
  $(document).keyup(function (event) {
    if (event.which == 27) {
      eliminarVM();
    }
  })

  // actualizaremos el zoom cuando la ventana cambie de tamaño
  $(window).on('resize', actualizarTama)

  // y al cargar la página
  actualizarTama()

  // ->NOTA: escoje el contenedor seleccionado
  $('.linkMenu').on({
    click: function () {

      //$('li.active ul.menu-open').css('display',"none");
      $('li.active').removeClass('active');

      $(this).parent().parent().parent('li').addClass('active')
      $(this).parent().addClass('active')

      eliminarVM();


      $('.content-wrapper').html('')

      $(this).LoadingOverlay('show')

      $.get('./Containers/' + $(this).data('page'), function (page) {
        $('.content-wrapper').html(page)

        $(this).LoadingOverlay('hide')
        $('.linkMenu').LoadingOverlay('hide')
        $.LoadingOverlay('hide')
      })

      return false
    }
  })

  // NOTA: Cual item inicia del menu
  // if (con_codigo != "")
  //->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>$("a[data-page='conCrearCalendario']").click()
  // else
  // $( "a[data-page='conEmpresa']" ).click();  

  // ->modificar el usuario 
  $('#confUsuario').on('click', function () {

    // alert(con_codigo)

    // if (con_codigo != '-1')
    //  modVM2('vmUsuarios', usu_codigo, 'S')
    // else
    // modVM('vmUsuarios', usu_codigo)

    var varJson = {}
    varJson.TipUsuario = tipUsuario

    modVM_JSON('vmUsuarios', usu_codigo, varJson)
    return false
  })

  $('#btnSuspender').on('click', function () {
    $('#modSuspender').modal('show')
    $('#modSuspender').click()
    return false
  })

  // ->NOTA: al cerrar todas la ventanad modales
  $('.VenMod').on('click', '.close', function () {
    eliminarVM();
  })
})

// -----------------------------> VALIDAR PERIODO <-----------------------------//

function valPerCerrado(idePeriodo) {
  $res = false

  $.ajaxSetup({
    async: false
  })

  $.post('./envioPHP/PHPPerCerrado.php', {
      idePeriodo: idePeriodo
    }, function (data) {
      if (data == false) {
        swal('El periodo se encuentra cerrado', 'Comuniquese con el administrador...', 'warning')
      } else {
        $res = true
      }
    })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  $.ajaxSetup({
    async: true
  })

  return $res
}

// -----------------------------> VENTANAS MODALES <-----------------------------//

// -----------------------------> Funciones

// ->NOTA: inseta el data en la etiqueta .VenMOod y abre el formulario
function abrirVM(data, idEtiqueta) {
  $('body').css('overflow-y', 'hidden')
  $('.VenMod').html(data)
}

// ->NOTA: Elimina la ventana modal que ya se creo
function eliminarVM() {
  $('.VenMod').html('')
  $('.modal-backdrop.fade.in').remove()
  $('body').css('overflow-y', 'auto')
}

// -----------------------------> Crea los contenedores

// ->NOTA:

function crearVM(VMRuta) {
  var varJson = {}
  objJSON.rutaVM = VMRuta
  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta, objJSON)
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      abrirVM(data)
    })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })
}

// con_parametro
function crearVM2(VMRuta, opc) {
  var objJSON = {}
  objJSON.rutaVM = VMRuta
  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta + '/', {
      opc: opc,
      objJSON: objJSON
    })
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      abrirVM(data)
    })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })
}

// con JSON
function crearVM_JSON(VMRuta, objJSON) {
  objJSON.rutaVM = VMRuta
  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta + '/', {
      objJSON: objJSON
    })
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      abrirVM(data)
    })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })
}

// ->cuando solo hay un id
function modVM(VMRuta, id) {
  var objJSON = {}
  objJSON.rutaVM = VMRuta
  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta + '/', {
      id: id,
      objJSON: objJSON
    })
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      console.log(data);
      abrirVM(data)
    })
}

// ->cuando hay mas de dos id
function modVM2(VMRuta, id, id_opc) {
  var objJSON = {}
  objJSON.rutaVM = VMRuta
  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta + '/', {
      id: id,
      opc: id_opc,
      objJSON: objJSON
    })
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      abrirVM(data)
    })
}

function modVM_JSON(VMRuta, id, objJSON) {
  console.log(objJSON)

  objJSON.rutaVM = VMRuta
  objJSON.id = id

  // console.log(objJSON)

  var objJSON = JSON.stringify(objJSON)
  $('body').LoadingOverlay('show')
  $.post('./Containers/' + VMRuta + '/', {
      id: id,
      objJSON: objJSON
    })
    .done(function (data) {
      $('body').LoadingOverlay('hide');
      abrirVM(data)
    })
}

// <--------------

// -----------------------------> DataTable 

$.extend(true, $.fn.dataTable.defaults, {
  'language': {
    'sProcessing': 'Procesando...',
    'sLengthMenu': 'Mostrar _MENU_ registros',
    'sZeroRecords': 'No se encontraron resultados',
    'sEmptyTable': 'Ningún dato disponible en esta tabla',
    'sInfo': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
    'sInfoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
    'sInfoFiltered': '(filtrado de un total de _MAX_ registros)',
    'sInfoPostFix': '',
    'sSearch': 'Buscar:',
    'sUrl': '',
    'sInfoThousands': ',',
    'sLoadingRecords': 'Cargando...',
    'oPaginate': {
      'sFirst': 'Primero',
      'sLast': 'Último',
      'sNext': 'Siguiente',
      'sPrevious': 'Anterior'
    },
    'oAria': {
      'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
      'sSortDescending': ': Activar para ordenar la columna de manera descendente'
    }
  }
})

// <--------------

// -----------------------------> VALIDATE
$.extend(jQuery.validator.messages, {
  required: 'Este campo es obligatorio.',
  remote: 'Por favor, rellena este campo.',
  email: 'Por favor, escribe una dirección de correo válida',
  url: 'Por favor, escribe una URL válida.',
  date: 'Por favor, escribe una fecha válida.',
  dateISO: 'Por favor, escribe una fecha (ISO) válida.',
  number: 'Por favor, escribe un número entero válido.',
  digits: 'Por favor, escribe sólo dígitos.',
  creditcard: 'Por favor, escribe un número de tarjeta válido.',
  equalTo: 'Por favor, escribe el mismo valor de nuevo.',
  accept: 'Por favor, escribe un valor con una extensión aceptada.',
  maxlength: jQuery.validator.format('Por favor, no escribas más de {0} caracteres.'),
  minlength: jQuery.validator.format('Por favor, no escribas menos de {0} caracteres.'),
  rangelength: jQuery.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
  range: jQuery.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
  max: jQuery.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
  min: jQuery.validator.format('Por favor, escribe un valor mayor o igual a {0}.')
})

// <--------------