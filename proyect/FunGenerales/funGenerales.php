<?php
require_once '../php/conexion.php';
$conexion = new clsConexion();

session_start();

$res = new stdClass();
$res->resultado = '';

if (isset($_POST['accion'])) {

	$accion = $_POST['accion'];

	try {

		switch ($accion) {

			case 'f_strCampo': //encontrar un campo en particular

				$tabla = $_POST['tabla'];
				$campo = $_POST['campo'];
				$condicion = $_POST['condicion'];

				$sql = " sELECT $campo FROM $tabla ";
				$condicion = ($condicion != '')?" WHERE $condicion ":'';
				
				$respuesta = $conexion->prepare($sql.$condicion);
				$respuesta->execute();				

				while($data = $respuesta->fetch()){
					$res->resultado = $data[0];
					break;
				}		

				break;			

			case 'f_intEliminar': //eliminar registro

				$tabla = $_POST['tabla'];				
				$condicion = $_POST['condicion'];

				$sql = " dELETE FROM $tabla ";
				$condicion = ($condicion != '')?" WHERE $condicion ":'';
				
				$respuesta = $conexion->prepare($sql.$condicion);
				$respuesta->execute();	

				$res->con = $sql.$condicion;	
				$res->resultado = 'S';					

				break;

			case 'f_JSONCon': //retornar json en consulta			

				$tabla = $_POST['tabla'];
				$campos = $_POST['campos'];
				$condicion = $_POST['condicion'];

				$res->campo = $campos;

				$campos =  ($campos != '')?" $campos ":'*';
				$sql = " sELECT $campos FROM $tabla ";
				$condicion = ($condicion != '')?" WHERE $condicion ":'';	

				$res->con = $sql.$condicion;	

				$respuesta = $conexion->prepare($sql.$condicion);
				$respuesta->execute();					

			    $res->filas = $respuesta->rowCount();

			    if($res->filas > 0){$res->resultado = 'S';}

				$res->JSON = $respuesta->fetchAll(PDO::FETCH_ASSOC);		

				break;

			case 'f_intUpdate': //eliminar registro

				$tabla = $_POST['tabla'];				
				$campos = $_POST['campos'];
				$condicion = $_POST['condicion'];

				$sql = " uPDATE $tabla SET ".$campos;
				$condicion = ($condicion != '')?" WHERE $condicion ":'';

				$res->sql = $sql.$condicion;
				
				$respuesta = $conexion->prepare($sql.$condicion);
				$respuesta->execute();				

				$res->resultado = 'S';					

				break;
				
			case 'f_fechaServidor': //obtener fecha servidor							

				$res->fecha = date("Y/m/d");					

				break;
			
			default:
				$res->resultado = 'A un no se a creado esta opcion';
				break;

		}
		
	} catch (Exception $e) {		
		$res->resultado = 'Error: '. $e->getMessage();
		echo json_encode($res);		
	}	

}
else{$res->resultado = 'HUBO UN ERROR';}

$_SESSION['dep'] = json_encode($res);
echo json_encode($res);	

?>