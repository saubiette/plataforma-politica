// res.resultado = S

var rutaDef = '../../FunGenerales/funGenerales.php'

function f_strCampo (tabla, campo, condicion, ruta) {

  // valor por defecto
  ruta = ruta || rutaDef

  var resultado = ''

  $.ajax({
    async: false,
    dataType: 'json',
    type: 'POST',
    url: ruta,
    data: {tabla: tabla,campo: campo,condicion: condicion,accion: 'f_strCampo'},
    success: function (res) {resultado = res.resultado;}
  })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  // devuelve un dato dependiendo del campo que se envie y pregunte
  return resultado
}

function f_intEliminar (tabla, condicion, ruta) {

  // valor por defecto
  ruta = ruta || rutaDef

  var resultado = ''

  $.ajax({
    async: false,
    dataType: 'json',
    type: 'POST',
    url: ruta,
    data: {tabla: tabla,condicion: condicion,accion: 'f_intEliminar'},
    success: function (res) {resultado = res.resultado;}
  })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  return resultado
}

function f_intUpdate (tabla, campos, condicion, ruta) {

  // valor por defecto
  ruta = ruta || rutaDef

  var resultado = ''

  $.ajax({
    async: false,
    dataType: 'json',
    type: 'POST',
    url: ruta,
    data: {tabla: tabla,campos: campos,condicion: condicion,accion: 'f_intUpdate'},
    success: function (res) {resultado = res;}
  })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  return resultado
}

function f_JSONCon (tabla, campos, condicion, ruta) {

  // valor por defecto
  ruta = ruta || rutaDef

  var resultado = ''

  $.ajax({
    async: false,
    dataType: 'json',
    type: 'POST',
    url: ruta,
    data: {tabla: tabla,campos: campos,condicion: condicion,accion: 'f_JSONCon'},
    success: function (res) {resultado = res;}
  })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  return resultado
}

function f_fechaServidor (ruta) {

  // valor por defecto
  ruta = ruta || rutaDef

  var resultado = ''

  $.ajax({
    async: false,
    dataType: 'json',
    type: 'POST',
    url: ruta,
    data: {accion: 'f_fechaServidor'},
    success: function (res) {resultado = res.fecha;}
  })
    .fail(function (xhr, textStatus, errorThrown) {
      alert(xhr.responseText)
    })

  return resultado
}

//sirve para ver que fila de la tabla esta seleccionada y que campo quires 
function selecTabla (tabla, campo) {
  var idx = tabla.row('.selected').index(); 
  var data = tabla.row(idx).data();
  varCampo = (data === undefined) ? "" :data[campo];    
  return varCampo;
} 
