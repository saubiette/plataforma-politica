    <!-- jQuery 2.1.4 -->
    <script src="../plugins/fullCalendar/moment.min.js"></script>
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>      

    <!--DataTable-->
    <script type="text/javascript" src="../plugins/DataTable/datatables.min.js"></script>		
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/dataTables.buttons.min.js"></script>		
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/dataTables.select.min.js"></script>	
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/jszip.min.js"></script>
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/pdfmake.min.js"></script>
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/vfs_fonts.js"></script><!--si no va esto no sale la ext -> pdf-->
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/buttons.html5.min.js"></script><!--si no va esto no sale la ext -> pdf-->
	<script type="text/javascript" src="../plugins/DataTable/Buttons-1.1.2/js/buttons.print.min.js"></script>

    <!-- DataTable - Bootstrap 3.3.5 -->
    <script src="../plugins/DataTable/dataTables.bootstrap.min.js"></script>    
    
     <!--NOTA: esta no va porque ya esta arriba Bootstrap 3.3.5 -->
    <!--script src="../plugins/bootstrap/js/bootstrap.min.js"></script-->  
    <script src="../plugins/bootstrap/js/bootstrap-modal.js"></script>

    <script src="../plugins/bootstrap/js/fileinput.min.js"></script>
      
    <!-- AdminLTE App -->
    <script src="../dist/js/app.js"></script>     
       
    <!-- SlimScroll 1.3.0 -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>    

    <!--loadingoverlay-->
    <script src="../plugins/loadingoverlay/loadingoverlay.js"></script>

    <!--lightbox-->
    <script src="../plugins/lightbox/lightbox.js"></script>

    <!-- Sweetalert -->
    <script src="../plugins/sweetalert/sweetalert.min.js"></script>

    <script src="../plugins/jQuery/jquery.validate.js"></script>

    <script src="../plugins/jQuery/typeahead.min.js"></script>

    <!--fullCalendar-->
    
    <script src="../plugins/fullCalendar/fullcalendar.js"></script>
    <script src="../plugins/fullCalendar/locale-all.js"></script>
    
    
    <!--Asistente: controla los eventos del mdi-->
    <script src="./Asistente/asistente.min.js"></script>
    <!--funciones generales-->
    <script src="./FunGenerales/funGenerales.js"></script>
    
	<!-- Plugins Alertify -->  
    <script src="../plugins/notification/plugins/alertifyjs/js/alertify.min.js"></script>     
    <script src="../plugins/jquery.canvasjs.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <?php
                    $con = new clsConexion();
                    $res = $con->prepare("SELECT * FROM calendario WHERE (cal_hora_inicio < CURTIME() AND cal_fecha = DATE_ADD(CURDATE(), INTERVAL 1 DAY)) OR (cal_hora_inicio > CURTIME() AND cal_fecha = CURDATE())");
                    $res->execute();

                    $c=0;
                                        
                    while($fila = $res->fetch()){
                        $c++;                    
                    } 

                    echo "<script>
                      document.getElementById('notification-count').innerHTML=".$c.";
                      document.getElementById('notification-header').innerHTML= 'Tienes ".$c." Notificaciones';
                      if(".$c." > 0){

                        // simula una fecha 5 minuto adelante (en tu caso es 2880 minutos)
                        const datedb = moment().add(".$c.", 'minutes');
                        // le restamos 3 segundos (en tu caso es 1440 minutos)
                        const custom = datedb.clone().subtract(".$c.", 'minutes');
                        // diferencia en ms
                        const diff = custom - moment();
                        
                        setTimeout(() => {
                          // creamos el objeto audio
                          var audioElement = document.createElement('audio');
                          // indicamos el archivo de audio a cargar
                          audioElement.setAttribute('src', '../plugins/notification/ping.mp3');
                          // Si deseamos que una vez cargado empieze a sonar...
                          audioElement.setAttribute('autoplay', 'autoplay');
                          alertify.error('Tiene ".$c." Reunion(es)', 'success', 15, function(){});
                        }, diff);
                        
               

                      }
                      </script>";

                    ?>         

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

function format(input)
{
var num = input.value.replace(/\./g,'');
if(!isNaN(num)){
num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
num = num.split('').reverse().join('').replace(/^[\.]/,'');
input.value = num;
}
  
else{ alert('Solo se permiten numeros');
input.value = input.value.replace(/[^\d\.]*/g,'');
}
}
</script>