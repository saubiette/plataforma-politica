      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel" style="min-height: 4.3em;">
            <div class="pull-left image">              

              <?php if(trim($_SESSION['ruta_img']) != ""): ?>       
                <img src="<?php echo './images/Perfiles/'.$_SESSION['ruta_img']; ?>" class="img-circle" alt="User Image">               
              <?php else: ?>    
                <img src="./images/Perfiles/user.png" class="img-circle" alt="User Image">               
              <?php endif ?>
              
            </div>
            <div class="pull-left info">
              <p style="width: 10em; overflow: hidden; text-overflow: ellipsis;"><?php echo $_SESSION["usu_nombre"];?></p>
              <a href="#"><i class="fa fa-hand-o-right text-success"></i>
                 <?php

                    $tipoUsuario = $_SESSION['usu_nivel_acceso'];                   
                   
                    switch($_SESSION["usu_nivel_acceso"]){
                      case 'S': echo 'Super Administrador';
                              break;
                      case 'A': echo 'Asistente';
                              break;
                      case 'L': echo 'Lider';
                              break; 
                      case 'E': echo 'Secretaria';
                              break;     
                      case 'F': echo 'Secretaria Firmas';
                              break;                 
                    }

                  ?>
              </a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form"  style="display:none;">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
            <li class="header">NAVEGACIÓN</li>  
            <li class="treeview"><a href="#" class='linkMenu' data-page="conDashboard"><i class="fa fa-tachometer" aria-hidden="true"></i><span> Dashboard</span></a></li>            
            <li class="treeview"><a href="#" class='linkMenu' data-page="conEnvioMensajes"><i class="fa fa-envelope-o"></i><span> Envío de Mensajes</span></a></li>
            <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearCalendario"><i class="fa fa-laptop"></i><span> Agenda</span></a></li>
            <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearReuniones"><i class="fa fa-users"></i><span> Reuniones</span></a></li>
            <!--li class="treeview"><a href="#" class='linkMenu' data-page="conCrearAfiliadosCR"><i class="fa fa-address-card-o"></i><span> Afiliados CR</span></a></li-->

            <?php if($tipoUsuario <> "L" ): ?><li class="treeview"><a href="#" class='linkMenu' data-page="conCrearPersonas"><i class="fa fa-address-card"></i><span> Reg. Votantes</span></a></li><?php endif; ?>

            <?php if($tipoUsuario == "L" ): ?> <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearLideresPersonas"><i class="fa fa-suitcase"></i><span> Reg. Votantes</span></a></li><?php endif; ?>
            <?php if($tipoUsuario <> "L" ): ?> <li class="treeview"><a href="#" class='linkMenu' data-page="conLideresPersonasSU"><i class="fa fa-suitcase"></i><span> Lideres - Votantes</span></a></li><?php endif; ?>                        
            
            
            <!--li class="treeview">
              <a href="#">
                <i class="fa fa-pencil-square-o"></i>
                <span>Adm. Firmas</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right cm_right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearCordinadoresGestionador"><i class="fa fa-hand-o-right"></i><span> Coordinadores y <br> Gestionadores</span></a></li>    
                <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearAdminFirmas"><i class="fa fa-hand-o-right"></i><span> Administración de <br> Firmas</span></a></li>                  
              </ul>
            </li-->           

            <?php if($tipoUsuario == "S"): ?> 

            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Reportes</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right cm_right"></i>
                </span>
              </a>
              <ul class="treeview-menu">                
                <li class=""><a href="./Containers/Reportes/repNombresCelulares.php" target="_blank" class='' ><i class="fa fa-hand-o-right"></i><span> Nombres y Celulares <br> Consolidado</span></a></li>                 
              </ul>
            </li> -->
			<li class="treeview"><a href="#" class='linkMenu' data-page="conBoletines"><i class="fa fa-file-text"></i><span> Boletines</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span class="nav-link">Adm. Usuarios</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right cm_right"></i>
                </span>
              </a>
              <ul class="treeview-menu">                
                <li class="treeview"><a href="#" class='linkMenu' data-page="conCrearUsuarios"><i class="fa fa-hand-o-right"></i><span> Usuarios</span></a></li> 
                <li class="treeview"><a href="#" class='linkMenu' data-page="conPermisos"><i class="fa fa-hand-o-right"></i><span> Permisos</span></a></li>
              </ul>
            </li>

            <?php endif; ?> 
                        
            <li class="treeview"><a href="./generalesPHP/salir" ><i class="fa fa-arrow-circle-left "></i><span> Salir</span></a></li>        
          </ul>
        </section>

        <!-- /.sidebar -->
      </aside>