<?php 
//session_start();
    require '../php/conexion.php';     
    //require "../generalesPHP/funciones.php";
?>

<header class="main-header">
  
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>P</b>P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Plataforma</b>Politica</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">--n</span>
      </a>

      
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu searchbar">
        <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
        <ul class="nav navbar-nav">

        <?php              
            // if($_SESSION['ide_empresa'] != ""):              
        ?>
                    
          <!-- Notifications: style can be found in dropdown.less -->
          <!--li class="dropdown notifications-menu">
            <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
              <i class='fa fa-bell-o'></i>              
            </a>
          </li-->
        
        <?php //endif; ?>

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#"   class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning" id="notification-count"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="notification-header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  
                  <?php
                    $con = new clsConexion();
                    $res = $con->prepare("SELECT * FROM calendario WHERE cal_fecha = DATE_ADD(CURDATE(), INTERVAL 1 DAY) OR (cal_hora_inicio > CURTIME() AND cal_fecha = CURDATE())");
                    $res->execute();

                    $c=0;
                                        
                    while($fila = $res->fetch()){
                      echo '<li><a href="#"><i class="fa fa-users text-aqua"></i>'.$fila["cal_titulo"].'<br>Fecha: '.$fila["cal_fecha"].'<br>Hora: '.$fila["cal_hora_inicio"].' - '.$fila["cal_hora_fin"].'</a></li>';
                                          
                        $c++;                    
                    } 



                    echo "<script>
                      document.getElementById('notification-count').innerHTML=".$c.";
                      document.getElementById('notification-header').innerHTML= 'Tienes ".$c." Notificaciones';
                        </script>";

                    ?> 
              
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>        
        
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

              <?php if(trim($_SESSION['ruta_img']) != ""): ?>
                <img src="<?php echo './images/Perfiles/'.$_SESSION['ruta_img']; ?>" class="user-image" alt="User Image">
              <?php else: ?>
                <img src="./images/Perfiles/user.png" class="user-image" alt="User Image">
              <?php endif ?>

              <span class="hidden-xs"><?php echo $_SESSION["usu_nombre"];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Footer-->
              <li class="user-footer">    
                <div class="pull-left">
                <div class="pull-right">
                  <a href="./generalesPHP/salir" class="btn btn-default btn-flat"> <i class="fa fa-sign-out text-success"></i> Cerrar sesión</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" id="confUsuario" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>