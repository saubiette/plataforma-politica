<?php
session_start();
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT * FROM  coordinadores "); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	


foreach ($array as $key => $value) {   

    $array[$key]['coo_identificacion'] = "<a href=javascript:Ver('". $value['coo_codigo']."') alt='Ver' title='Ver'><b>".$value['coo_identificacion']."</b></a>"; 

    if($array[$key]['coo_ruta_foto'] != "" ){
        $array[$key]['coo_ruta_foto'] = "<a data-lightbox='gal_".$array[$key]['coo_codigo']."' href='./images/Coordinadores/".$array[$key]['coo_ruta_foto'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Coordinadores/".$array[$key]['coo_ruta_foto'].$strNoCache."'></a>";
    }

    if($array[$key]['coo_ruta_cedula'] != "" ){
        $array[$key]['coo_ruta_cedula'] = "<a data-lightbox='gal_".$array[$key]['coo_codigo']."' href='./images/Coordinadores/Cedulas/".$array[$key]['coo_ruta_cedula'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Coordinadores/Cedulas/".$array[$key]['coo_ruta_cedula'].$strNoCache."'></a>";
    }           
    
    switch($array[$key]['coo_tipo_coordinador']){
        case "RU": $array[$key]['coo_tipo_coordinador'] = "RURAL";     break;
        case "UR": $array[$key]['coo_tipo_coordinador'] = "URBANO";    break;
        case "MU": $array[$key]['coo_tipo_coordinador'] = "MUNICIPAL"; break;
    };
   
    $array[$key]['eli'] = "<a href=javascript:Eliminar('". $value['coo_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
    //$array[$key]['eli'] = "-";      
    $array[$key]['mod'] = "<a href=javascript:Modificar('". $value['coo_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
   
}

echo  json_encode($array);

?>