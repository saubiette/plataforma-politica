$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  var table = $('#dtGeneric').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'url': rutaContenedorActual + '/arrays.php',
      'dataSrc': '' // obligatorio  para que cargue
    },
    'columns': [
      { 'data': 'coo_codigo', 'width': '2%' },
      { 'data': 'coo_identificacion', 'width': '6%' },
      { 'data': 'coo_nombre', 'width': '12%'  },
      { 'data': 'coo_apellido', 'width': '12%'  },
      { 'data': 'coo_tipo_coordinador' },
      { 'data': 'coo_celular' },
      { 'data': 'coo_telefono_fijo' },
      { 'data': 'coo_email' },

      { 'data': 'coo_ruta_foto', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            
      { 'data': 'coo_ruta_cedula', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            

      { 'data': 'mod', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }
      { 'data': 'eli', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false } // , sClass: "alignRight" }
    ],
    buttons: [],
    lengthMenu: [
      [ 10, 25, 50, -1 ],
      [ '10 filas', '25 filas', '50 filas', 'Todas' ]
    ],
    select: true
  })

  var idT = selecTabla(table, 'coo_codigo')

  $('#dtGeneric tbody').on('click', 'tr:not(.selected)', function () {
    tableGestinador.clear()

    var data = table.row(this).data()
    idT = data['coo_codigo']

    tableGestinador.ajax.reload()
  })

  var tableGestinador = $('#dtGestionador').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'type': 'POST',
      'url': rutaContenedorActual + '/arraysGestionadores.php',
      'dataSrc': '', // obligatorio  para que cargue
      'data': function (d) {
        d.id = idT
      }
    },
    'columns': [
      { 'data': 'ges_codigo', 'width': '2%' },
      { 'data': 'ges_identificacion', 'width': '6%' },
      { 'data': 'ges_nombre', 'width': '12%'  },
      { 'data': 'ges_apellido', 'width': '12%'  },
      { 'data': 'ges_celular' },
      { 'data': 'ges_telefono_fijo' },
      { 'data': 'ges_email' },

      { 'data': 'ges_ruta_foto', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            
      { 'data': 'ges_ruta_cedula', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            

      { 'data': 'mod', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }
      { 'data': 'eli', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false } // , sClass: "alignRight" }
    ],
    buttons: [],
    lengthMenu: [],
  // select: false
  })

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
    $('#dtGestionador').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  // ->NUEVO COORDIANDORES
  $('#nuevo').on('click', function () {
    var varJson = { }
    varJson.TipUsuario = 'S'

    crearVM_JSON('vmCoordinador', varJson)
    return false
  })

  // ->NUEVO GESTIONADOR
  $('#nuevoGestionador').on('click', function () {
    var varJson = { }
    varJson.TipUsuario = 'S'

    crearVM_JSON('vmGestionador', varJson)
    return false
  })
})

// ->INICIO DATOS CLIENTES

function Eliminar (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
  closeOnConfirm: true },
    function () {
      $.post('./Containers/vmCoordinador/PHPCrud.php', { opc: 'D', id: id }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        }else if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        }else {
          $('.dataTable').DataTable().ajax.reload()
        }
      }, 'json')
    })

  return false
}

function EliminarGestionador (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
  closeOnConfirm: true },
    function () {
      $.post('./Containers/vmGestionador/PHPCrud.php', { opc: 'D', id: id }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        }else if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        }else {
          $('.dataTable').DataTable().ajax.reload()
        }
      }, 'json')
    })

  return false
}

function Modificar (id) {
  modVM('vmCoordinador', id)
  return false
}

function ModificarGestionador (id) {
  modVM('vmGestionador', id)
  return false
}

function Ver (id) {
  var varJson = { }
  varJson.ver = true

  modVM_JSON('vmCoordinador', id, varJson)
  return false
}

function VerGestionador (id) {
  var varJson = { }
  varJson.ver = true

  modVM_JSON('vmGestionador', id, varJson)
  return false
}

// ->FIN DATOS CLIENTES
