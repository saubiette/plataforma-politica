    <?php
    session_start();
        require "../../../php/conexion.php";
        require "../../generalesPHP/funciones.php";
        
        $strCampo = fncPermiso("COG","R", new clsConexion()); 
        
        if(!$strCampo){
            echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
            exit();
        }               
    ?>

    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Coordinaodres y Gestionadores</li>            
        </ol>  
        
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td colspan='3'><input type="button" id="nuevo" value="Nuevo Coordinador"></td>							
                </tr>
                <tr>
                    <th>Codígo</th>
                    <th>Identificación</th>
                    <th>Nombres</th>                                               
                    <th>Apellido</th>                                               
                    <th>Tipo Coordinador</th>    
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    
                    <th>Foto</th>
                    <th>Cedula</th>							
                    
                    <th></th>
                    <th></th>
                </tr>
            </thead>					
        </table>   

        <table id="dtGestionador" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td colspan='3'><input type="button" id="nuevoGestionador" value="Nuevo Gestionador"></td>							
                </tr>
                <tr>
                    <th>Codígo</th>
                    <th>Identificación</th>
                    <th>Nombres</th>                                               
                    <th>Apellido</th>   
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    
                    <th>Foto</th>
                    <th>Cedula</th>							
                    
                    <th></th>
                    <th></th>
                </tr>
            </thead>					
        </table>      
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.js"></script>