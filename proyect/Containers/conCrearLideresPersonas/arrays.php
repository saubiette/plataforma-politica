<?php
session_start();
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT * 
                      FROM  personas INNER JOIN
                            lider_personas ON personas.per_codigo = lider_personas.per_codigo
                      WHERE usu_codigo = '".$_SESSION['usu_codigo']."' "); 

$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {   

    $array[$key]['per_identificacion'] = "<a href=javascript:Ver('". $value['per_codigo']."') alt='Ver' title='Ver'><b>".$value['per_identificacion']."</b></a>"; 

    if($array[$key]['per_ruta_foto'] != "" ){
        $array[$key]['per_ruta_foto'] = "<a data-lightbox='gal_".$array[$key]['per_codigo']."' href='./images/Personas/".$array[$key]['per_ruta_foto'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Personas/".$array[$key]['per_ruta_foto'].$strNoCache."'></a>";
    }

    if($array[$key]['per_ruta_cedula'] != "" ){
        $array[$key]['per_ruta_cedula'] = "<a data-lightbox='gal_".$array[$key]['per_codigo']."' href='./images/Personas/Cedulas/".$array[$key]['per_ruta_cedula'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Personas/Cedulas/".$array[$key]['per_ruta_cedula'].$strNoCache."'></a>";
    }        
    
    $array[$key]['mod'] = "<a href=javascript:Modificar('". $value['per_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";

    //if($_SESSION['usu_nivel_acceso'] == "S")
        $array[$key]['eli'] = "<a href=javascript:Eliminar('". $value['per_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
    //else
    //    $array[$key]['eli'] = "-";

}

echo  json_encode($array);

?>