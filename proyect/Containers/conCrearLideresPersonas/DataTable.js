$(document).ready(function() {
  rutaContenedorActual = "./Containers/" + rutaContenedorActual;

  var table = $("#dtGeneric").DataTable({
    dom: "Bfrtip",
    ajax: {
      url: rutaContenedorActual + "/arrays.php",
      dataSrc: "" // obligatorio  para que cargue
    },
    columns: [
      { data: "per_codigo", width: "6%" },
      { data: "per_identificacion", width: "6%" },
      { data: "per_nombre" },
      { data: "per_apellido" },
      { data: "per_fecha_nacimiento" },
      { data: "per_telefono_fijo" },
      { data: "per_celular" },
      { data: "per_email" },

      {
        data: "per_ruta_foto",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "per_ruta_cedula",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }

      {
        data: "mod",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } // , sClass: "alignRight" }
    ],
    buttons: [],
    lengthMenu: [],
    pageLength: 14,
    select: false
  });

  $(document).ready(function() {
    $("#dtGeneric")
      .removeClass("display")
      .addClass("table table-striped table-hover table-bordered");
  });

  // ->NUEVO PERSONA
  $("#nuevo").on("click", function() {
    var varJson = {};

    crearVM_JSON("vmLiderPersonas", varJson);
    return false;
  });
  
});

// ->INICIO DATOS CLIENTES

function Eliminar(id) {
  swal(
    {
      title: "¿Seguro que deseas eliminar este Registro?",
      text: "No podrás deshacer este paso...",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Eliminar",
      closeOnConfirm: true
    },
    function() {
      $.post(
        "./Containers/vmLiderPersonas/PHPCrud.php",
        { opc: "D", id: id },
        function(data) {
          if (data == false) {
            swal(
              "No se puede eliminar",
              "Este registro esta enlazado con otros...",
              "warning"
            );
          } else if (data == "DN") {
            swal(
              "Necesita permisos para realizar esta accion.",
              "Contacte al administrador del sistema",
              "warning"
            );
          } else {
            $(".dataTable")
              .DataTable()
              .ajax.reload();
          }
        },
        "JSON"
      );
    }
  );

  return false;
}

function Modificar(id) {
  modVM("vmLiderPersonas", id);
  return false;
}

function Ver(id) {
  var varJson = {};
  varJson.ver = true;

  modVM_JSON("vmLiderPersonas", id, varJson);
  return false;
}

// ->FIN DATOS CLIENTES
