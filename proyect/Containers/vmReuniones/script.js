$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')   

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)
    formData.append('tipo_usuario', auxJSON.TipUsuario)

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {       

        $('.dataTable').DataTable().ajax.reload()

        if ($('#id').val() == usu_codigo) {location.reload();}

        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
     
      var info = data.reu_fecha_registro.split("-");
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#usu_fecha_creacion').html(fechaFormateada)
      $("#reu_titulo").val(data.reu_titulo);      
      $("#reu_descripcion").val(data.reu_descripcion);      
      $("#reu_fecha").val(data.reu_fecha);
      $("#reu_hora").val(data.reu_hora);
      
      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo)       

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').prop('disabled', 'true')
        $('#idForm textarea').prop('disabled', 'true')        
      }
    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()
})
