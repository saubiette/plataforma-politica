<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'reuniones';
    $ideCampo = "reu_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }        

        $strCampo = fncPermiso("REU", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }   
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("reu_titulo",$_POST['reu_titulo']);  
            $SQLcomando->set_AddCampo("reu_descripcion",$_POST['reu_descripcion']);  
            $SQLcomando->set_AddCampo("reu_fecha",$_POST['reu_fecha']);  
            $SQLcomando->set_AddCampo("reu_hora",$_POST['reu_hora']);  
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']);  
                   
                       
            if($_POST['id'] != ""){                               

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                  

            }else{                

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1
                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);
                $SQLcomando->set_AddCampo("reu_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);              

                $SQLcomando->set_tipo_comando("INSERT");   

            }           

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':

            $resp = $conexion->prepare("SELECT * FROM reuniones_personas WHERE reu_codigo = '$ideValor'");
            $resp->execute();
            $r = $resp->rowCount();
            
            if($r > 0){
                echo json_encode(false);
                $conexion->rollBack();   
                exit();   
            }              
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * 
                        FROM $tabla INNER JOIN 
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

         case 'ACT':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","S");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
        
         case 'DES':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","N");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>