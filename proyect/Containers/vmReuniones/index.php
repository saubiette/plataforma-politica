<?php 
session_start();
    require '../../../php/conexion.php'; 
    require '../../generalesPHP/funciones.php'; 

    $strJSON = json_decode($_POST['objJSON']);

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso("REU","R", new clsConexion()); 
        else
             $strCampo = fncPermiso("REU","U", new clsConexion());    
    else:
         $strCampo = fncPermiso("REU","C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }

?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar Reunión</b></h2> (Fecha de Registro <span id='usu_fecha_creacion'></span>) 
                    <?php else:?>
                        <h2><b>Registrar Reunión</b></h2>
                    <?php endif?>

                <?php else:?>  

                    <h2><b>Reunión</b></h2> (Fecha de Registro <span id='usu_fecha_creacion'></span>) 

                <?php endif?>      
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                              

                <div class="form-group">
                    <label for="reu_titulo">(*)Titulo:</label>
                    <input type="text" class="form-control input-sm required" id="reu_titulo" name="reu_titulo" placeholder="Introduce un Titulo" class="required">  
                    <label for="reu_titulo" class="label label-danger error" generated="true"></label>                   
                </div>      

                <div class="form-group">
                    <label for="reu_descripcion">Descripción:</label>
                    <textarea class="form-control" rows="5" id="reu_descripcion" name="reu_descripcion"></textarea>
                </div>         

                <div class="form-group">
                    <label class="col-lg-6 paddin-no-left" for="reu_fecha">(*)Fecha:</label>
                    <label class="col-lg-6 paddin-no-left" for="reu_hora">(*)Hora (24hr):</label>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="date" class="form-control input-sm required" id="reu_fecha" name="reu_fecha" placeholder="Introduce una fecha">     
                            <label for="reu_fecha" class="label label-danger error" generated="true"></label>                     
                        </div>
                        <div class="col-lg-6 paddin-no-left">
                            <input type="time" class="form-control input-sm required"  id="reu_hora" name="reu_hora" placeholder="Introduce un Hora">
                            <label for="reu_hora" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div>                   
                    
                </div>     

                <div class="form-group">
                    <label for="dep_codigo">Departamento:</label>                    
                    <select class="form-control input-sm opcion required" id="dep_codigo" name="dep_codigo">
                       
                         <?php
                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM departamentos ORDER BY dep_nombre ");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['dep_codigo']."'>".$fila['dep_nombre']."</option>";
                            }           
                         ?> 

                    </select>                     
                </div>                

                <div class="form-group">
                    <label for="mun_codigo">Municipios:</label>                    
                    <select class="form-control input-sm" id="mun_codigo" name="mun_codigo">                        
                    </select>                     
                </div>                                
                                         
            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>            
                <?php endif?>   
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.min.js"></script>