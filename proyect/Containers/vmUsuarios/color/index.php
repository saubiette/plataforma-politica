<?php 
session_start();
require '../../../php/conexion.php';
require '../../generalesPHP/funciones.php'; ?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog modal-lg">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar <?php echo fncTipoUsuario($strJSON->TipUsuario); ?></b></h2> (Fecha de Registro <span id='usu_fecha_creacion'></span>) 
                    <?php else:?>
                        <h2><b>Registrar <?php echo fncTipoUsuario($strJSON->TipUsuario); ?></b></h2>
                    <?php endif?>

                <?php else:?>  

                    <h2><b><?php echo fncTipoUsuario($strJSON->TipUsuario); ?></b></h2> (Fecha de Registro <span id='usu_fecha_creacion'></span>) 

                <?php endif?>      
                
            </div>
            <div class="modal-body">
                <div class="row">       
                    

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>
                        <div class="form-group">
                            <label for="usu_nivel_acceso">Tipo Usuario:</label>                    
                            <select class="form-control input-sm opcion required" id="usu_nivel_acceso" name="usu_nivel_acceso" >                       
                                <option value='S'>Super Usuario</option>
                                <option value='A'>Asistente</option>
                                <option value='L' <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> selected <?php } ?> >Lider</option>
                                <option value='E'>Secretaria</option>
                                <option value='F'>Secretaria Firmas</option>                          
                            </select>                     
                        </div>  
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="usu_login">(*)Login/Usuario:</label>
                            <input type="text" class="form-control input-sm required" id="usu_login" name="usu_login" placeholder="Introduce un login/Usuario">     
                            <label for="usu_login" class="label label-danger error" generated="true"></label>               
                        </div> 
                    </div>  

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="usu_nombre">(*)Nombre:</label>
                            <input type="text" class="form-control input-sm required" id="usu_nombre" name="usu_nombre" placeholder="Introduce un nombre" class="required">  
                            <label for="usu_nombre" class="label label-danger error" generated="true"></label>                   
                        </div> 
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">             
                        <div class="form-group">
                            <label for="usu_email"> Email:</label>
                            <input type="email" class="form-control input-sm" id="usu_email" name="usu_email" placeholder="Introduce un nombre">  
                            <label for="usu_email" class="label label-danger error" generated="true"></label>                   
                        </div> 
                    </div>
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>
                        <div class="form-group">
                            <label for="dep_codigo">Departamento:</label>                    
                            <select class="form-control input-sm opcion required" id="dep_codigo" name="dep_codigo">
                               
                                 <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select>".$fila['dep_nombre']."</option>";
                                    }           
                                 ?> 

                            </select>                     
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>           
                        <div class="form-group">
                            <label for="mun_codigo">Municipios:</label>                    
                            <select class="form-control input-sm" id="mun_codigo" name="mun_codigo">                        
                            </select>                     
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="cat_codigo"> Categoría:</label>                    
                            <select class="form-control input-sm opcion required" id="cat_codigo" name="cat_codigo">
                               
                                 <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM categoria ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        echo "<option value='".$fila['cat_codigo']."'>".$fila['cat_nombre']."</option>";
                                    }           
                                 ?> 

                            </select>                     
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>
                        <?php if(!isset($strJSON->ver)): ?>

                        <?php if(isset($_POST['id'])):?>                

                            <div class="form-group">
                                <label for="contrasenna_anterior">(*)contraseña Anterior:</label>
                                <input type="password" class="form-control input-sm " id="contrasenna_anterior" name="contrasenna_anterior" placeholder="Introduce nuevamente tu contraseña" class="required">
                                <label for="contrasenna_anterior" class="label label-danger error" generated="true"></label>                     
                            </div>

                        <?php endif ?>
                    
                        <div class="form-group">
                            <label for="usu_contraseña">(*)Contraseña:</label>
                            <input type="password" minlength='8' class="form-control input-sm required" id="usu_contraseña" name="usu_contraseña" placeholder="Introduce una contraseña" class="required" <?php if(fncTipoUsuario($strJSON->TipUsuario)){ ?> value="12345678" <?php } ?>>
                            <label for="usu_contraseña" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div> 
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>
                        <div class="form-group">
                            <label for="usu_contraseña_2">(*)Repita contraseña:</label>
                            <input type="password" class="form-control input-sm required" id="usu_contraseña_2" name="usu_contraseña_2" placeholder="Introduce nuevamente tu contraseña" class="required" <?php if(fncTipoUsuario($strJSON->TipUsuario)){ ?> value="12345678" <?php } ?> >
                            <label for="usu_contraseña_2" class="label label-danger error" generated="true"></label>                     
                        </div>  
                    </div>
                    
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">   
                        
                        <?php if(isset($strJSON->ver)): ?>

                            <div class="form-group">                    
                                <img class="img-perfil" id="img-perfil" style="display:none;" />                    
                            </div>   

                        <?php else:?> 
                            
                            <div class="form-group">                    
                                <label for="usu_ruta_imagen">Foto:</label>
                                <input id="usu_ruta_imagen" name="usu_ruta_imagen" type="file" accept="image/gif, image/jpeg, image/png" class="file" data-show-upload="false" data-show-caption="true">
                                <label for="usu_ruta_imagen" id="extFoto" style="display:none;" class="label label-success">Posee una imagen</label>
                            </div> 

                        <?php endif?>   
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label>JEFE</label>
                            <select class="form-control input-sm opcion required" name="jefe">
                                <option value='S'>Seleccione</option>                        
                            </select>                         
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <label>COLOR</label>
                         <input type="text" id="hex_color">
                         <input type="button" id="colorSelector" value="Color">                        
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" <?php if(fncTipoUsuario($strJSON->TipUsuario) == 'Lider'){ ?> hidden <?php } ?>>
                         <div class="form-group">
                            <label for="usu_nota">Nota:</label>
                            <textarea class="form-control" rows="5" id="usu_nota" name="usu_nota"></textarea>
                        </div> 
                    </div>

                    
                <?php endif?>  
                </div>                         
            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>            
                <?php endif?>   
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->
<!-- <script src="color/colores.js" type="text/javascript"></script>
<script type="text/javascript" src="color/js/colorpicker.js"></script>   
<link rel="stylesheet" href="color/css/colorpicker.css" type="text/css" /> -->
<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>
<script type="text/javascript">

  $('#colorSelector').ColorPicker({
        color: '#0000ff',
        // onShow: function (colpkr) {
        //     $(colpkr).fadeIn(500);
        //     return false;
        // },
        // onHide: function (colpkr) {
        //     $(colpkr).fadeOut(500);
        //     return false;
        // },
        onChange: function (hsb, hex, rgb) {

            // $('body').css('background', '#' + hex);
            $("#hex_color").val('#'+hex);

        }
    });
 </script> 