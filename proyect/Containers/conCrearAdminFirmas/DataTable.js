$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  var tableGestinador = $('#dtGestionador').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'url': rutaContenedorActual + '/arraysGestionadores.php',
      'dataSrc': '' // obligatorio  para que cargue      
    },
    'columns': [{
      'data': 'ges_codigo',
      'width': '2%'
    },
      {
        'data': 'ges_identificacion',
        'width': '6%'
      },
      {
        'data': 'ges_nombre',
        'width': '12%'
      },
      {
        'data': 'ges_apellido',
        'width': '12%'
      },
      {
        'data': 'ges_celular'
      },
      {
        'data': 'ges_telefono_fijo'
      },
      {
        'data': 'ges_email'
      },
      {
        'data': 'coo_nombre'
      },
      {
        'data': 'reg_form',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      },
      {
        'data': 'ges_ruta_foto',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      } // , sClass: "alignRight" }   
    ],
    'columnDefs': [{
      'targets': [0],
      'visible': false,
      'searchable': false
    }],
    'pageLength': 7,
    buttons: [],
    lengthMenu: [],
    select: true
  })

  var idT = selecTabla(tableGestinador, 'ges_codigo')

  $('#dtGestionador tbody').on('click', 'tr:not(.selected)', function () {
    table.clear()
    tableRecibidas.clear()

    var data = tableGestinador.row(this).data()
    idT = data['ges_codigo']

    table.ajax.reload()

    idE = selecTabla(table, 'for_codigo')
    tableRecibidas.ajax.reload()
  })

  var table = $('#dtGeneric').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'type': 'POST',
      'url': rutaContenedorActual + '/arraysFirmas.php',
      'dataSrc': '', // obligatorio  para que cargue
      'data': function (d) {
        d.id = idT
      }
    },
    'columns': [{
      'data': 'for_codigo',
      'width': '2%'
    },
      {
        'data': 'for_fecha',
        'width': '25%'
      },
      {
        'data': 'for_numero'
      },
      {
        'data': 'total_entregados'
      },
      {
        'data': 'faltan'
      },

      {
        'data': 'rec',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      }, // , sClass: "alignRight" }
      {
        'data': 'eli',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      } // , sClass: "alignRight" }
    ],
    'columnDefs': [{
      'targets': [0],
      'visible': false,
      'searchable': false
    }],
    buttons: [],
    lengthMenu: [],
    select: true,
    'bPaginate': false,
    'searching': false
  })

  var idE = selecTabla(table, 'for_codigo')

  $('#dtGeneric tbody').on('click', 'tr:not(.selected)', function () {
    tableRecibidas.clear()

    var data = table.row(this).data()
    idE = data['for_codigo']

    tableRecibidas.ajax.reload()
  })

  var tableRecibidas = $('#dtResividos').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'type': 'POST',
      'url': rutaContenedorActual + '/arraysRecibidas.php',
      'dataSrc': '', // obligatorio  para que cargue
      'data': function (d) {
        d.id = idE
      }
    },
    'columnDefs': [{
      'targets': [0],
      'visible': false,
      'searchable': false
    }],
    'columns': [{
      'data': 'efo_codigo',
      'width': '2%'
    },
      {
        'data': 'efo_fecha',
        'width': '15%'
      },
      {
        'data': 'efo_numero'
      },

      {
        'data': 'efo_firnas'
      },
      {
        'data': 'efo_confirmadas'
      },
      {
        'data': 'efo_error'
      },

      {
        'data': 'efo_pago',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      }, // , sClass: "alignRight" }
      {
        'data': 'total',
        'sClass': 'alignRight'
      },
      {
        'data': 'cal',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      }, // , sClass: "alignRight" }
      {
        'data': 'eli',
        'width': '1%',
        'sClass': 'alignCenter pagCero',
        'bSortable': false
      } // , sClass: "alignRight" }
    ],
    buttons: [],
    lengthMenu: [],
    select: false,
    'bPaginate': false,
    'searching': false
  })

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
    $('#dtGestionador').removeClass('display').addClass('table table-striped table-hover table-bordered')
    $('#dtResividos').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  // ->NUEVO COORDIANDORES
  /*$('#nuevoFormulario').on('click', function () {
    var varJson = { }
    varJson.TipUsuario = 'S'

    crearVM_JSON('vmFormularioFirma', varJson)
    return false
  })*/

  $('#btnReporte').on('click', function () {
    var varJson = {}
    crearVM_JSON('vmReportes', varJson)
    return false
  });
})

function RegistrarFirmas (id) {
  var varJson = {}
  varJson.ges_codigo = id

  crearVM_JSON('vmFormularioFirma', varJson)
  return false
}

function formRecibidas (id, faltan) {
  var varJson = {}
  varJson.for_codigo = id
  varJson.faltantes = faltan

  crearVM_JSON('vmFormRecibidas', varJson)
  return false
}

// ->INICIO DATOS CLIENTES

function Eliminar (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
    closeOnConfirm: true
  },
    function () {
      $.post('./Containers/vmFormularioFirma/PHPCrud.php', {
        opc: 'D',
        id: id
      }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        } else if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        } else {
          $('.dataTable').DataTable().ajax.reload()
        }
      }, 'json')
    })

  return false
}

function EliminarRecibidas (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
    closeOnConfirm: true
  },
    function () {
      $.post('./Containers/vmFormRecibidas/PHPCrud.php', {
        opc: 'D',
        id: id
      }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        } else if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        }else {
          $('.dataTable').DataTable().ajax.reload()
        }
      }, 'json')
    })

  return false
}

function Calificar (id) {
  var varJson = {}
  varJson.Calificar = true
  modVM_JSON('vmFormRecibidas', id, varJson)
  return false
}

function Ver (id) {
  var varJson = {}
  varJson.ver = true

  modVM_JSON('vmFormularioFirma', id, varJson)
  return false
}

function VerGestionador (id) {
  var varJson = {}
  varJson.ver = true

  modVM_JSON('vmGestionador', id, varJson)
  return false
}

function VerRecibidas (id) {
  var varJson = {}
  varJson.ver = true

  modVM_JSON('vmFormRecibidas', id, varJson)
  return false
}

function Pagar (id) {
  swal({
    title: 'Información',
    text: '¿Desea pagar?',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Pagar',
    closeOnConfirm: true
  },
    function () {
      $.post('./Containers/vmFormRecibidas/PHPCrud.php', {
        opc: 'PAG',
        id: id
      }, function (data) {
        if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
          return false
        }
        $('.dataTable').DataTable().ajax.reload()
      }, "json")
    })

  return false
}

function Reembolsar (id) {
  swal({
    title: 'Información',
    text: '¿Desea Reembolsar?',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Reembolsar',
    closeOnConfirm: true
  },
    function () {
      $.post('./Containers/vmFormRecibidas/PHPCrud.php', {
        opc: 'REE',
        id: id
      }, function (data) {
        if (data == 'DN') {
          swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
          return false
        }
        $('.dataTable').DataTable().ajax.reload()
      },"json")
    })

  return false
}
