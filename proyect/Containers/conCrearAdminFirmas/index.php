    <?php
    session_start();
        require "../../../php/conexion.php";
        require "../../generalesPHP/funciones.php";
        
        $strCampo = fncPermiso("ADF","R", new clsConexion()); 
        
        if(!$strCampo){
            echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
            exit();
        }  
    ?>

    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Administracion de Firmas</li>            
        </ol>  
            
        <table id="dtGestionador" class="display" cellspacing="0" width="100%">
            <thead>        
                <tr>
                    <td></td>
                    <td><input type="button" id="btnReporte" value="Reporte"></td>
                </tr>
                <tr>
                    <th>Codígo</th>
                    <th>Identificación</th>
                    <th>Nombres</th>                                               
                    <th>Apellido</th>   
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Coordinadores</th>

                    <th></th> 
                    <th>Foto</th>                   
                </tr>
            </thead>					
        </table>  

         <row>            
            <div class="col-md-5">                   
                <h3><b>Listas Entregadas</b></h3>
            </div>
            <div class="col-md-7">         
                <h3><b>Listas Recibidas</b></h3>
            </div>
        </row>  

        <row>
            
            <div class="col-md-5">

                <table id="dtGeneric" class="display" cellspacing="0" width="100%">
                    <thead>               
                        <tr>
                            <th>Codígo</th>
                            <th>Fecha</th>
                            <th>No. Formularios</th>   
                            <th>Total Entregados</th>    
                            <th>Faltan</th>          						
                            
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>					
                </table>   

            </div>

            <div class="col-md-7">

                <table id="dtResividos" class="display" cellspacing="0" width="100%">
                    <thead>               
                        <tr>
                            <th>Codígo</th>
                            <th>Fecha</th>
                            <th>No. Recibidos</th>      

                            <th>No. Firmas</th>                                  
                            <th>No. Fir. Correctas</th>      
                            <th>No. Fir. Erroneas</th>      
                                      						
                            <th>Pago</th>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>					
                </table>   

            </div>

        </row>   
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.js"></script>