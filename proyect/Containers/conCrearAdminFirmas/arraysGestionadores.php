<?php
session_start();
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT * 
                      FROM  gestionadores INNER JOIN
                            coordinadores ON gestionadores.coo_codigo = coordinadores.coo_codigo "); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	


foreach ($array as $key => $value) {   

    $array[$key]['coo_nombre'] = $value['coo_nombre']." ".$value['coo_apellido'];

    $array[$key]['ges_identificacion'] = "<a href=javascript:VerGestionador('". $value['ges_codigo']."') alt='Ver' title='Ver'><b>".$value['ges_identificacion']."</b></a>"; 

    if($array[$key]['ges_ruta_foto'] != "" ){
        $array[$key]['ges_ruta_foto'] = "<a data-lightbox='gal_".$array[$key]['ges_codigo']."' href='./images/Coordinadores/".$array[$key]['ges_ruta_foto'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Coordinadores/".$array[$key]['ges_ruta_foto'].$strNoCache."'></a>";
    }

    if($array[$key]['ges_ruta_cedula'] != "" ){
        $array[$key]['ges_ruta_cedula'] = "<a data-lightbox='gal_".$array[$key]['ges_codigo']."' href='./images/Coordinadores/Cedulas/".$array[$key]['ges_ruta_cedula'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Coordinadores/Cedulas/".$array[$key]['ges_ruta_cedula'].$strNoCache."'></a>";
    }           
       
    $array[$key]['eli'] = "<a href=javascript:EliminarGestionador('". $value['ges_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
    $array[$key]['mod'] = "<a href=javascript:ModificarGestionador('". $value['ges_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
    $array[$key]['reg_form'] = "<a href=javascript:RegistrarFirmas('". $value['ges_codigo']."')><img src=./images/form_3.png   alt='Registrar No. Formularios' title='Registrar No. Formularios' border=0></a>";
   
}

echo  json_encode($array);

?>