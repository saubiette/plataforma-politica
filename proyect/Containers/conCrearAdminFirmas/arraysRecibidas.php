<?php
session_start();
require '../../../php/conexion.php';
$con = new clsConexion();

$id = $_POST['id'];

$pru = $con->prepare("SELECT * 
                      FROM  entrega_formularios 
                      WHERE for_codigo = '$id'"); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	


foreach ($array as $key => $value) {   

    $array[$key]['efo_fecha'] = "<a href=javascript:VerRecibidas('". $value['efo_codigo']."') alt='Ver' title='Ver'><b>".$value['efo_fecha']."</b></a>"; 
  
    $array[$key]['eli'] = "<a href=javascript:EliminarRecibidas('". $value['efo_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
    $array[$key]['cal'] = "<a href=javascript:Calificar('". $value['efo_codigo']."')><img src=./images/form_cal_2.png   alt='Calificar' title='Calificar' border=0></a>";

     $array[$key]['total'] = number_format(( $value['efo_confirmadas'] * 400), 0, ',', '.'); 

    if($array[$key]['efo_pago'] == "S"){
        $array[$key]['efo_pago'] = "<a href=javascript:Reembolsar('". $value['efo_codigo']."')><img src=./images/on.png   alt='Reembolsar' title='Reembolsar' border=0></a>";
    }
    else{
        $array[$key]['efo_pago'] = "<a href=javascript:Pagar('". $value['efo_codigo']."')><img src=./images/off.png   alt='Pagar' title='Pagar' border=0></a>";
    } 
  
}

echo  json_encode($array);

?>