    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Usarios
            </li>
            <li class="active">Permisos</li>
        </ol>  
        
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td><input type="button" id="nuevo" value="Nuevo"></td>							
                </tr>
                <tr>
                    <th>Login/Usuario</th>
                    <th>Modulo</th>
                   
                    <th>Ingresar</th>
                    <th>Crear</th>
                    <th>Modificar</th>
                    <th>Eliminar</th>

                    <th></th>
                    <th></th>
                </tr>
            </thead>					
        </table>       
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.min.js"></script>