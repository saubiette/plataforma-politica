<?php
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT pmi_codigo, usu_login, usu_nombre , pmi_opcion, pmi_select, pmi_insert, pmi_update, pmi_delete, usuario.usu_codigo
                      FROM   permisos INNER JOIN
                             usuario ON permisos.usu_codigo = usuario.usu_codigo"); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {

    $array[$key]['usu_nombre'] = $array[$key]['usu_login']." -> ".$array[$key]['usu_nombre'];

    $array[$key]['pmi_select'] =  ($array[$key]['pmi_select'] == "S")?"Si":"No";
    $array[$key]['pmi_insert'] =  ($array[$key]['pmi_insert'] == "S")?"Si":"No";
    $array[$key]['pmi_update'] =  ($array[$key]['pmi_update'] == "S")?"Si":"No";
    $array[$key]['pmi_delete'] =  ($array[$key]['pmi_delete'] == "S")?"Si":"No";  
    
    $pmiOpcion = "";
    
    switch ($array[$key]['pmi_opcion']) {
        case 'CAL': $pmiOpcion = "Calendario"; break;
        case 'RVO': $pmiOpcion = "Reg. Votantes"; break;
        case 'RVL': $pmiOpcion = "Reg. Votantes(Lider)"; break;
        case 'LIV': $pmiOpcion = "Lideres - Votantes"; break;
        case 'COG': $pmiOpcion = "Coordinadores y Gestionadores"; break;
        case 'ADF': $pmiOpcion = "Administracion de Firmas"; break;
        case 'ACR': $pmiOpcion = "Afiliados CR"; break;
        case 'REU': $pmiOpcion = "Reuniones"; break;
        default: $pmiOpcion = "Otro"; break;       
    }

    $array[$key]['pmi_opcion'] = $pmiOpcion;

    
    $array[$key]['mod'] = "<a href=javascript:Modificar('".$value['pmi_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
    $array[$key]['eli'] = "<a href=javascript:Eliminar('".$value['pmi_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
}

echo  json_encode($array);

?>