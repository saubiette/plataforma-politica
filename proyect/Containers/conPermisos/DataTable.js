$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  $("#dtGeneric").DataTable({
    dom: "Bfrtip",
    ajax: {
      url: rutaContenedorActual + "/arrays.php",
      dataSrc: "" // obligatorio  para que cargue
    },
    columns: [
      { data: "usu_nombre" },
      { data: "pmi_opcion" },

      {
        data: "pmi_select",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "pmi_insert",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "pmi_update",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "pmi_delete",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }

      {
        data: "mod",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } // , sClass: "alignRight" }
    ],
    buttons: [
      {
        extend: "collection",
        text: "Export",
        buttons: ["copy", "excel", "csv", "pdf", "print"],
        enabled: true
      }
    ],
    lengthMenu: [
      [10, 25, 50, -1],
      ["10 filas", "25 filas", "50 filas", "Todas"]
    ]
    // select: true
  });

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  $('#nuevo').on('click', function () {
    var varJson = { }
    crearVM_JSON('vmPermisos', varJson)
    return false
  })
})

function Eliminar (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
  closeOnConfirm: true },
    function () {
      $.post('./containers/vmPermisos/PHPCrud.php', { opc: 'D', id: id }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        }else {
          $('.dataTable').DataTable().ajax.reload()
        }
      })
    })

  return false
}

function Modificar (id) {
  modVM('vmPermisos', id);
  return false;
}
