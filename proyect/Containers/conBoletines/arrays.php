<?php
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT id_boletin, mesas_instaladas, mesas_informadas,potencial_sufragantes
                      FROM   boletines WHERE
                             id_boletin = 1"); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {

	$array[$key]['id_boletin'] = $array[$key]['id_boletin'];

    $array[$key]['mesas_instaladas'] = $array[$key]['mesas_instaladas'];

    $array[$key]['mesas_informadas'] = $array[$key]['mesas_informadas'];

    $array[$key]['potencial_sufragantes'] = $array[$key]['potencial_sufragantes'];

    $array[$key]['porcentaje_mesas'] = ($array[$key]['mesas_informadas'] * $array[$key]['mesas_informadas'] = $array[$key]['mesas_informadas']) / 100;
    }

echo  json_encode($array);