<?php
    session_start();
    require "../../../php/conexion.php";
?>

<div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Boletines</li>            
        </ol>
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td colspan='5'>
                        <input type="button" class="btn btn-success" id="nuevo" value="+ Nuevo Boletín">
                        <!--input type="button" id="enviarMsj" value="Enviar Mensaje"-->
                    </td>							
                </tr>
                <tr>
                    <th>Boletín Nº</th>
                    <th>Mesas instaladas</th>
                    <th>Mesas informadas</th>
                    <th>Potencial sufragantes</th>                            
                    <th>% Mesas informadas</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>					
        </table>
</div>

    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.js"></script>