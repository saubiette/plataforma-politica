<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'gestionadores';
    $ideCampo = "ges_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        $strCampo = fncPermiso("COG", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("ges_nombre",$_POST['ges_nombre']);              
            $SQLcomando->set_AddCampo("ges_apellido",$_POST['ges_apellido']);                  
            
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']);    
            $SQLcomando->set_AddCampo("ges_tipo_direccion",$_POST['ges_tipo_direccion']);
            $SQLcomando->set_AddCampo("ges_direccion_1",$_POST['ges_direccion_1']);
            $SQLcomando->set_AddCampo("ges_direccion_2",$_POST['ges_direccion_2']);
            $SQLcomando->set_AddCampo("ges_direccion_3",$_POST['ges_direccion_3']);
            $SQLcomando->set_AddCampo("ges_complemento_direccion",$_POST['ges_complemento_direccion']);
            $SQLcomando->set_AddCampo("ges_barrio",$_POST['ges_barrio']);             
            $SQLcomando->set_AddCampo("ges_telefono_fijo",$_POST['ges_telefono_fijo']);
            $SQLcomando->set_AddCampo("ges_celular",$_POST['ges_celular']);
            $SQLcomando->set_AddCampo("ges_email",$_POST['ges_email']);
            $SQLcomando->set_AddCampo("ges_notas",$_POST['ges_notas']);     
            $SQLcomando->set_AddCampo("coo_codigo",$_POST['coo_codigo']);       
                       
            if($_POST['id'] != ""){                              

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                       

            }else{                                                                                                       

                $resp = $conexion->prepare("SELECT $ideCampo 
                                            FROM   $tabla 
                                            WHERE  ges_identificacion = '".$_POST["ges_identificacion"]."'");
                $resp->execute();
                $maxV = $resp->rowCount();
            
                if ($maxV > 0){ 
                    echo false;    
                    $conexion->rollBack();   
                    exit();                   
                }

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);
                
                $SQLcomando->set_AddCampo("tdo_codigo",$_POST['tdo_codigo']);
                $SQLcomando->set_AddCampo("ges_identificacion",$_POST['ges_identificacion']);                
                $SQLcomando->set_AddCampo("ges_fecha_registro","CURRENT_DATE",false);                     

                $SQLcomando->set_AddCampo("ges_ruta_foto","");   
                $SQLcomando->set_AddCampo("ges_ruta_cedula","");                                                           
                                                                                   
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                $SQLcomando->set_tipo_comando("INSERT");   

            }

            if($_FILES['ges_ruta_foto']['name'] != ""){

                $foto = $_FILES['ges_ruta_foto'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;

                $RutImg = "per_".$RutImg;
                
                $ruta = "../../images/Gestionadores/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("ges_ruta_foto",$RutImg);                

            }   

            if($_FILES['ges_ruta_cedula']['name'] != ""){

                $foto = $_FILES['ges_ruta_cedula'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;
                
                $RutImg = "ced_".$RutImg;                

                $ruta = "../../images/Gestionadores/Cedulas/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("ges_ruta_cedula",$RutImg);                

            }       

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 
            
            echo (int)$ideValor;            
            break;   
            
        case 'D':                
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * , $tabla.mun_codigo as mun_codigo_Coo
                        FROM $tabla INNER JOIN 
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo INNER JOIN
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;    

        case 'clienteExiste':

             $strSQL = "SELECT * 
                        FROM $tabla                              
                        WHERE ges_identificacion = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();             

             $respestaJSON = new stdClass();
           
             if($resp->rowCount() > 0){
                $resM = $resp->fetchAll();
                $respestaJSON->datos = $resM[0];
                $respestaJSON->existe = true;
             }
             else{
                 $respestaJSON->existe = false;
            }          

             echo json_encode($respestaJSON);            
             break;    
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>