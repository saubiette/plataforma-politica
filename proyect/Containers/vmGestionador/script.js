$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  var porPistola = 'NO'

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    if (!/^([0-9])*$/.test($('#ges_identificacion').val())) {
      swal('Identificación no valida', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)

    console.log(porPistola)
    // return false

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        if (data == false) {
          swal('La Identificación ya existe', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }

        $('.dataTable').DataTable().ajax.reload()
        $('.dataTable').DataTable().search('').draw()
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('.file').fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif']
  })

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data.ges_ruta_foto != '') {
        $('#extFoto').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').prop('disabled', 'true')

        if (data.ges_ruta_foto != '') {
          $('#img-perfil').css('display', 'block')
          $('#img-perfil').attr('src', './images/Coordinadores/' + data.ges_ruta_foto)
        }
      }

      var info = data.ges_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#coo_codigo').val(data.coo_codigo)
      //$('#coo_codigo').prop('disabled', true)

      $('#tdo_codigo').val(data.tdo_codigo)
      $('#tdo_codigo').prop('disabled', true)

      $('#ges_identificacion').val(data.ges_identificacion)
      $('#ges_identificacion').prop('disabled', true)

      $('#ges_nombre').val(data.ges_nombre)
      $('#ges_apellido').val(data.ges_apellido)

      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      console.log(data.mun_codigo_Coo)
      $('#mun_codigo').val(data.mun_codigo_Coo)

      $('#ges_tipo_direccion').val(data.ges_tipo_direccion)

      $('#ges_direccion_1').val(data.ges_direccion_1)
      $('#ges_direccion_2').val(data.ges_direccion_2)
      $('#ges_direccion_3').val(data.ges_direccion_3)

      $('#ges_complemento_direccion').val(data.ges_complemento_direccion)
      $('#ges_barrio').val(data.ges_barrio)

      $('#ges_telefono_fijo').val(data.ges_telefono_fijo)
      $('#ges_celular').val(data.ges_celular)
      $('#ges_email').val(data.ges_email)
      $('#ges_notas').val(data.ges_notas)

      if (data.ges_ruta_cedula != '') {
        $('#extFotoCedula').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').attr('readonly', 'readonly')

        if (data.ges_ruta_cedula != '') {
          $('#img-cedula').css('display', 'block')
          $('#img-cedula').attr('src', './images/Coordinadores/Cedulas/' + data.ges_ruta_cedula)
        }
      }
    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()
})
