$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  $("#dtGeneric").DataTable({
    dom: "Bfrtip",
    ajax: {
      url: rutaContenedorActual + "/arrays.php",
      dataSrc: "" // obligatorio  para que cargue
    },
    columns: [
      { data: "usu_login" },
      { data: "usu_nombre" },
      { data: "usu_email" },
      { data: "dep_nombre" },
      { data: "mun_nombre" },
      { data: "categoria" },
      { data: "usu_nivel_acceso" },

      {
        data: "usu_activo",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "usu_ruta_imagen",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "mod",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } // , sClass: "alignRight" }
    ],
    buttons: [
      {
        extend: "collection",
        text: "Export",
        buttons: ["copy", "excel", "csv", "pdf", "print"],
        enabled: true
      }
    ],
    lengthMenu: [
      [15, 25, 50, -1],
      ["15 filas", "25 filas", "50 filas", "Todas"]
    ]
    // select: true
  });

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  $('#nuevo').on('click', function () {
    var varJson = { }
      varJson.TipUsuario = '-'

      crearVM_JSON('vmUsuarios', varJson)
      return false
    })
  })

  function Eliminar (id) {
    swal({
      title: '¿Seguro que deseas eliminar este Registro?',
      text: 'No podrás deshacer este paso...',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar',
    closeOnConfirm: true },
      function () {
        $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'D', id: id }, function (data) {
          if (data == false) {
            swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          }else {
            $('.dataTable').DataTable().ajax.reload()
          }
        })
      })

    return false
  }

  function Modificar (id, na) {
    var varJson = { }
      varJson.nivel_acceso = na;

      modVM_JSON('vmUsuarios', id, varJson)
      return false
    }

    function Activar (id) {
      swal({
        title: 'Información',
        text: '¿Desea activar este usuario?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Activar',
      closeOnConfirm: true },
        function () {
          $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'ACT', id: id }, function (data) {
            $('.dataTable').DataTable().ajax.reload()
          })
        })

      return false
    }

    function Desactivar (id) {
      swal({
        title: 'Información',
        text: '¿Desea desactivar este usuario?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Desactivar',
      closeOnConfirm: true },
        function () {
          $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'DES', id: id }, function (data) {
            $('.dataTable').DataTable().ajax.reload()
          })
        })

      return false
    }

    function Ver (id) {
      var varJson = { }
        varJson.TipUsuario = 'S'
        varJson.ver = true

        modVM_JSON('vmUsuarios', id, varJson)
        return false
      }
