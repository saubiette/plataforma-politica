    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Usuarios</li>            
        </ol>  
        
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td><input type="button" id="nuevo" value="Nuevo Usuario"></td>							
                </tr>
                <tr>
                    <th>Login</th>
                    <th>Nombre</th>                    
                    <th>Email</th>    
                    <th>Departamento</th> 
                    <th>Municipio</th> 
                    <th>Categoría</th> 
                    <th>Tipo Usuario</th> 
                    <th>Activo</th>
                    <th></th>							
                    <th></th>
                    <th></th>
                </tr>
            </thead>					
        </table>       
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.min.js"></script>