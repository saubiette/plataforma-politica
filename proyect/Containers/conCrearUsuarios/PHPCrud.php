<?php
session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'usuario';
    $ideCampo = "usu_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    session_start();
    
    switch ($opc) {         
            
        case 'D':
                                           
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;            
        

         case 'ACT':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","S");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
        
         case 'DES':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","N");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>