<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();     
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    $conexion->beginTransaction();   

    $tabla = 'anotaciones';
    $ideCampo = "ano_codigo";
    $ideValor = $_POST['id'];    

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        if(isset($_POST['tp']))
            if($_POST['tp'] != ""){
                $modPermiso = $_POST['tp'];                
            }
            else{
                $modPermiso = "RVO";
            }            
        else
            $modPermiso = "RVO";

        $strCampo = fncPermiso($modPermiso, $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    } 

    //session_start();   
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("ano_titulo",$_POST['ano_titulo']);              
            $SQLcomando->set_AddCampo("ano_comentario",$_POST['ano_comentario']);             
            $SQLcomando->set_AddCampo("ano_fecha_alerta",fncFechaNULL($_POST['ano_fecha_alerta']), false);          
            $SQLcomando->set_AddCampo("ano_hora",$_POST['ano_hora']); 
                             
            if($_POST['id'] != ""){                            

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                      

            }else{             

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);              
                                
                //session_start();
                $SQLcomando->set_AddCampo("ano_fecha_hora","NOW()",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);    
                $SQLcomando->set_AddCampo("ano_atendida",'N');  
                
                $perCodigo = $_POST['perCodigo'];
                $SQLcomando->set_AddCampo("per_codigo",$perCodigo);             
              
                $SQLcomando->set_tipo_comando("INSERT");   

            }       

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':                  
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;

        case 'AT':                  
                                    
            $strSQL = "UPDATE $tabla SET ano_atendida = 'S'  WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT *  , $tabla.mun_codigo as mun_codigo_Per
                        FROM $tabla INNER JOIN
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo INNER JOIN
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

    }

    $conexion->commit();   

} 
catch (Exception $e) {
    
    if($e->errorInfo[0] == '23000')
        echo "fk";    
    else
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";

    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>