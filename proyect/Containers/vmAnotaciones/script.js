$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  $('#cal_fecha').val(auxJSON.fecha)

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)
    formData.append("perCodigo", auxJSON.perCodigo);

    if (auxJSON.tp !== undefined) formData.append("tp", auxJSON.tp);

    // alert($('cal_fecha').val())
    // console.log(formData)
    // return false

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {        
        //$('#idCerrar').click()
        $("#ano_titulo").val("");
        $("#ano_comentario").val("");
        $("#ano_fecha_alerta").val("");
        $("#ano_hora").val("");

        $(".load").LoadingOverlay("hide");
        $('#dtNotas').DataTable().ajax.reload()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  });

  var table = $("#dtNotas").DataTable({
    dom: "Bfrtip",
    ajax: {
      type: "POST",
      url: rutaCarpetaActual + "/arrays.php",
      dataSrc: "", // obligatorio  para que cargue
      data: function(d) {
        d.id = auxJSON.perCodigo;
      }
    },
    order: [[3, "desc"]],
    columns: [
      { data: "ano_codigo", width: "1%" },

      { data: "ano_titulo", width: "10%" },
      { data: "ano_comentario", width: "20%" },
      { data: "ano_fecha_hora", width: "6%" },
      { data: "usu_nombre", width: "6%" },
      { data: "ano_fecha_alerta", width: "6%" },
      { data: "ano_hora", width: "6%" },
      {
        data: "ate",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } // , sClass: "alignRight" }
    ],
    columnDefs: [
      {
        targets: [0],
        visible: false,
        searchable: false
      }
    ],
    buttons: [],
    lengthMenu: [],
    select: false,
    searching: false
  });

  $('#idEliminar').on('click', function () {
    swal({
      title: '¿Seguro que deseas eliminar este Registro?',
      text: 'No podrás deshacer este paso...',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar',
    closeOnConfirm: true },
      function () {
        $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'D', id: $('#id').val() }, function (data) {
          if (data == false) {
            swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          }else if (data == 'DN') {
            swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
          }else {
            $('#calendar').fullCalendar('refetchEvents')
            $('#idCerrar').click()
          }
        }, 'json')
      })
    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data == 'DN') {
        swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        $('#idCerrar').click()
        return false
      }

      var info = data.cal_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#cal_fecha').val(data.cal_fecha)
      $('#cal_hora_inicio').val(data.cal_hora_inicio)
      $('#cal_hora_fin').val(data.cal_hora_fin)
      $('#cal_titulo').val(data.cal_titulo)
      $('#cal_comentario').val(data.cal_comentario)
      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo_Per)

      $('#cal_identificacion').val(data.cal_identificacion)
      $('#cal_nombre').val(data.cal_nombre)
      $('#cal_telefono').val(data.cal_telefono)

      if (auxJSON.ver != undefined) $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
    }, 'json')
  }

  // $('.formatoMiles').numberFM(true, 2, ',', '.')

  $("#ano_fecha_alerta").on("change", function() {
    if($("#ano_fecha_alerta").val() != ""){
       $("#ano_fecha_alerta").addClass("required");
       $("#ano_hora").addClass("required");
    }
    else{
      $("#ano_fecha_alerta").removeClass("required");
      $("#ano_hora").removeClass("required");
    }
  });

});

function EliminarAnotacion(id) {
  swal(
    {
      title: "¿Seguro que deseas eliminar este Registro?",
      text: "No podrás deshacer este paso...",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Eliminar",
      closeOnConfirm: true
    },
    function() {
      $.post(
        rutaCarpetaActual + "/PHPCrud.php",
        {
          tp: (auxJSON.tp !== undefined)?auxJSON.tp:"",
          opc: "D",
          id: id
        },
        function(data) {
          if (data == false) {
            //swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          } else if (data == "DN") {
            swal(
              "Necesita permisos para realizar esta accion.",
              "Contacte al administrador del sistema",
              "warning"
            );
          } else {
            $("#dtNotas")
              .DataTable()
              .ajax.reload();
          }
        },
        "json"
      );
    }
  );

  return false;
}

function darPorAtendida(id) {
  swal(
    {
      title: "¿Desea Dar Por Atendida Esta Alerta?",
      text: "No podrás deshacer este paso...",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Atender",
      closeOnConfirm: true
    },
    function() {
      $.post(
        rutaCarpetaActual + "/PHPCrud.php",
        {
          tp:  (auxJSON.tp !== undefined)? auxJSON.tp:"",
          opc: "AT",
          id: id
        },
        function(data) {
          if (data == false) {
            //swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          } else if (data == "DN") {
            swal(
              "Necesita permisos para realizar esta accion.",
              "Contacte al administrador del sistema",
              "warning"
            );
          } else {
            $("#dtNotas")
              .DataTable()
              .ajax.reload();
          }
        },
        "json"
      );
    }
  );

  return false;
}


