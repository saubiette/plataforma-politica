<?php 
 session_start();
    require '../../../php/conexion.php'; 
    require "../../generalesPHP/funciones.php";

    $strJSON = json_decode($_POST['objJSON']);

    if(isset($strJSON->tp))
        $modPermiso = $strJSON->tp;
    else
        $modPermiso = "RVO";

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso($modPermiso,"R", new clsConexion()); 
        else
             $strCampo = fncPermiso($modPermiso,"U", new clsConexion());    
    else:
         $strCampo = fncPermiso($modPermiso,"C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }   
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog modal-lg tamano-90 ">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>               

                <h2><b>Anotación</b></h2>         
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">       

                <table id="dtNotas" class="display" cellspacing="0" width="100%">
                    <thead>                       
                        <tr>
                            <th>Código</th>
                            <th>Titulo</th>
                            <th>Comentario</th>
                            <th>Fecha/Hora</th>
                            <th>Usuario</th>
                            <th>Fecha Alerta</th>
                            <th>Hora Alerta</th>                        	
                            
                            <th></th>
                            <th></th>                            
                        </tr>
                    </thead>					
                </table>                  
                
                <div class="form-group">
                    <label for="ano_titulo">(*)Titulo:</label>
                    <input type="text" step="any" class="form-control input-sm required" id="ano_titulo" name="ano_titulo" placeholder="Introduce un Título">  
                    <label for="ano_titulo" class="label label-danger error" generated="true"></label>                   
                </div>                 

                <div class="form-group">
                    <label for="ano_comentario">Comentario:</label>
                    <textarea class="form-control" rows="5" id="ano_comentario" name="ano_comentario"></textarea>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-lg-3" for="ano_fecha_alerta">Fecha Alerta:</label>
                        <label class="col-lg-3 paddin-no-left" for="ano_hora">Hora Alerta (24hr):</label>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <input type="date" class="form-control input-sm " id="ano_fecha_alerta" name="ano_fecha_alerta" placeholder="Introduce una fecha">     
                            <label for="ano_fecha_alerta" class="label label-danger error" generated="true"></label>                     
                        </div>
                        <div class="col-lg-3 paddin-no-left">
                            <input type="time" class="form-control input-sm "  id="ano_hora" name="ano_hora" placeholder="Introduce una Hora">
                            <label for="ano_hora" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div>                   
                    
                </div>                
                                         
            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Agregar</button>            
                <?php else: ?>
                    <button id='idEliminar' class="btn btn-danger load">Eliminar</button>            
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.min.js"></script>