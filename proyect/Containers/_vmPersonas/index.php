<?php 
    require '../../../php/conexion.php'; 
    require "../../generalesPHP/funciones.php";

    $strJSON = json_decode($_POST['objJSON']);

    if(isset($strJSON->tp))
        $modPermiso = $strJSON->tp;
    else
        $modPermiso = "RVO";

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso($modPermiso,"R", new clsConexion()); 
        else
             $strCampo = fncPermiso($modPermiso,"U", new clsConexion());    
    else:
         $strCampo = fncPermiso($modPermiso,"C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }  
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog modal-lg tamano-90">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>                              

                <?php
                    $nombre = "Votante";

                    switch($modPermiso){                   
                        case 'ACR': $nombre = "Afiliados CR"; break;                       
                    } 

                    if(!isset($strJSON->ver)):
                 ?>

                    <?php if(isset($_POST['id'])):?>
                        <div>
                            <h2><b>Modificar <?=$nombre;?> </b></h2> (Fecha de Registro <span id='per_fecha_registro'></span>)
                        </div>
                    <?php else:?>
                        <h2><b>Registrar <?=$nombre;?></b></h2>
                    <?php endif;?>         
                
                <?php else:?>  

                    <h2><b><?=$nombre;?></b></h2> 
                    (Fecha: <b><span id='per_fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif;?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>"> 

                <div class="row">
                    <div class="col-lg-2">    
                        <label for="tdo_codigo">Tipo Documento:</label>                                                         
                    </div>
                    <div class="col-lg-2 paddin-no-left">                    
                        <label for="per_identificacion">(*)No. Documento:</label>
                    </div>                 
                    <div class="col-lg-3 paddin-no-left">  
                        <label for="dep_codigo_exp">Departamento Exp.:</label>               
                    </div>                  
                    <div class="col-lg-3 paddin-no-left">    
                        <label for="mun_codigo_exp">Municipios Exp.:</label>                          
                    </div>
                    <div class="col-lg-2 paddin-no-left">    
                        <label for="per_fecha_expedicion">Fecha de Expedición:</label>
                    </div>
                </div>          

                <div class="form-group">                                    
                    <div class="row">
                        <div class="col-lg-2">                                               
                            <select class="form-control input-sm" id="tdo_codigo" name="tdo_codigo" required>
                                
                                <?php                           

                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM tipo_documento");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){                                        
                                        echo "<option value='".$fila['tdo_codigo']."' >".$fila['tdo_nombre']."</option>";
                                    }           
                                ?>

                            </select>                       
                        </div>
                        <div class="col-lg-2 paddin-no-left">                               
                                <input type="text" class="form-control input-sm required" id="per_identificacion" autofocus  name="per_identificacion" placeholder="Introduce una Identificación">     
                                <label for="per_identificacion" class="label label-danger error" generated="true"></label>                 
                        </div>                   
                        <div class="col-lg-3 paddin-no-left">                              
                            <select class="form-control input-sm opcion required" id="dep_codigo_exp" name="dep_codigo_exp">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select >".$fila['dep_nombre']."</option>";
                                    }           
                                ?> 

                            </select>         
                        </div>                  
                        <div class="col-lg-3 paddin-no-left">                    
                            <select class="form-control input-sm" id="mun_codigo_exp" name="mun_codigo_exp">                        
                            </select>                    
                        </div>   
                        <div class="col-lg-2 paddin-no-left">    
                            <input type="date" class="form-control input-sm" id="per_fecha_expedicion" name="per_fecha_expedicion" placeholder="Introduce una fecha de Expedición">
                            <label for="per_fecha_expedicion" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div>                                     
                </div>  

                <div class="row">
                    <div class="col-lg-4 ">  
                        <label for="per_nombre">(*)Nombre:</label>          
                    </div>                  
                    <div class="col-lg-3 paddin-no-left">    
                        <label for="per_apellido">(*)Apellido:</label>                     
                    </div>
                    <div class="col-lg-3 paddin-no-left">                               
                        <label for="per_fecha_nacimiento">Fecha de Nacimiento:</label>                                                   
                    </div>    
                     <div class="col-lg-2 paddin-no-left">                               
                        <label for="pro_codigo">Profesión:</label>                                                   
                    </div>                
                </div>    

                <div class="form-group">     
                    <div class="row">
                        <div class="col-lg-4 ">                              
                            <input type="text" class="form-control input-sm required" id="per_nombre" name="per_nombre" placeholder="Introduce un Nombre" class="required">  
                            <label for="per_nombre" class="label label-danger error" generated="true"></label>          
                        </div>                  
                        <div class="col-lg-3 paddin-no-left">                    
                            <input type="text" class="form-control input-sm required" id="per_apellido" name="per_apellido" placeholder="Introduce un Apellido" class="required">  
                            <label for="per_apellido" class="label label-danger error" generated="true"></label>                      
                        </div>                               
                    
                        <div class="col-lg-3 paddin-no-left">                                               
                            <input type="date" class="form-control input-sm" id="per_fecha_nacimiento" name="per_fecha_nacimiento" placeholder="Introduce una fecha de nacimiento">
                            <label for="per_fecha_nacimiento" class="label label-danger error" generated="true"></label>                        
                        </div>
                        <div class="col-lg-2 paddin-no-left">                              
                            <select class="form-control input-sm opcion required" id="pro_codigo" name="pro_codigo">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM profesiones ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['pro_codigo'] == '-1')?"selected":"";
                                        echo "<option value='".$fila['pro_codigo']."' $select>".$fila['pro_nombre']."</option>";
                                    }           
                                ?> 

                            </select>         
                        </div> 
                    </div>                                     
                </div>     

                <div class="row">
                    <div class="col-lg-1">                    
                        <label for="edad">Edad:</label>
                    </div> 
                    <div class="col-lg-1">                               
                        <label for="per_estatura">Estatura:</label>
                    </div>
                    <div class="col-lg-1 paddin-no-left">  
                        <label for="per_genero">Genero:</label>                    
                    </div>
                    <div class="col-lg-1 paddin-no-left">                    
                        <label for="per_gsrh">G.S. RH:</label>
                    </div>                       
                    <div class="col-lg-3 paddin-no-left">  
                        <label for="dep_codigo">Departamento:</label>               
                    </div>                  
                    <div class="col-lg-3 paddin-no-left">    
                        <label for="mun_codigo">Municipios:</label>                          
                    </div>
                    <div class="col-lg-2 paddin-no-left">    
                        <label for="per_estado_civil">Estado Civil:</label>                    
                    </div>
                </div>  
                
                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-1">                               
                            <input type="text" class="form-control input-sm" readonly id="edad" name="edad" placeholder="Edad">                
                        </div>  
                        <div class="col-lg-1 paddin-no-left">                               
                            <input type="number" step="any" class="form-control input-sm" id="per_estatura" name="per_estatura" placeholder="Ejm: 1.73">
                            <label for="per_estatura" class="label label-danger error" generated="true"></label>                     
                        </div>
                        <div class="col-lg-1 paddin-no-left">  
                            <select class="form-control input-sm" id="per_genero" name="per_genero" required>
                                <option value='M'>Masculino</option>
                                <option value='F'>Femenino</option>
                                <option value='O'>Otro</option>
                            </select>
                        </div> 
                        <div class="col-lg-1 paddin-no-left">                    
                            <input type="text" step="any" class="form-control input-sm" id="per_gsrh" name="per_gsrh" placeholder="Ejm: O+">
                            <label for="per_gsrh" class="label label-danger error" generated="true"></label>                     
                        </div>                         
                        <div class="col-lg-3 paddin-no-left">                              
                            <select class="form-control input-sm opcion required" id="dep_codigo" name="dep_codigo">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select>".$fila['dep_nombre']."</option>";
                                    }           
                                ?> 

                            </select>         
                        </div>                  
                        <div class="col-lg-3 paddin-no-left">                    
                            <select class="form-control input-sm" id="mun_codigo" name="mun_codigo">                        
                            </select>                    
                        </div>
                        <div class="col-lg-2 paddin-no-left">    
                            <select class="form-control input-sm" id="per_estado_civil" name="per_estado_civil" required>
                                <option value='S'>Soltero</option>
                                <option value='C'>Casado</option>
                                <option value='O'>Divorciado</option>
                                <option value='U'>Union Libre</option>
                                <option value='T'>Otro</option>
                            </select>                      
                        </div>
                    </div>     
                </div>               
                
                <!--INICIO DIRECCION -->

                <div class="form-group">
                    <label class="col-lg-12 paddin-no-left" for="per_tipo_direccion">Dirección:</label>                    
                    <div class="row">
                        <div class="col-lg-4">                            
                                <select class="form-control input-sm" id="per_tipo_direccion" name="per_tipo_direccion" required>
                                    <option value='CA'>Calle</option>
                                    <option value='CR'>Carrera</option>
                                    <option value='TR'>Transversal</option>
                                    <option value='DI'>Diagonal</option>
                                    <option value='AV'>Avenida</option>
                                    <option value='OT'>Otro</option>
                                </select>                             
                        </div>
                        <div class="col-lg-3 paddin-no-left">                                                                    
                            <input type="text" class="form-control input-sm" id="per_direccion_1" name="per_direccion_1" placeholder="Ejm: 40 Nor.">
                        </div>                   
                        <div class="col-lg-3 paddin-no-left"> 
                            <div class="row">
                                <div class="col-lg-1">
                                    <label class="paddin-no-left tamano20px" for="per_direccion_2">#</label>  
                                </div>       
                                <div class="col-lg-10 paddin-no-left">                   
                                    <input type="text" class="form-control input-sm" id="per_direccion_2" name="per_direccion_2" placeholder="Ejm: 73">
                                </div>
                                <div class="col-lg-1 paddin-no-left">
                                    <label class="paddin-no-left tamano20px" for="per_direccion_3">-</label>
                                </div> 
                            </div>
                        </div>                        
                        <div class="col-lg-2 paddin-no-left">                                   
                            <input type="text" class="form-control input-sm" id="per_direccion_3" name="per_direccion_3" placeholder="Ejm: 78">                           
                        </div>
                    </div>
                    <label for="cli_fecha_nacimiento" class="label label-danger error" generated="true"></label>                     
                </div> 

                <!--FIN    DIRECCION --> 

                <div class="row">
                    <div class="col-lg-2">                               
                        <label for="per_complemento_direccion">Complemento Dirección:</label>
                    </div>
                    <div class="col-lg-2 paddin-no-left">                    
                        <label for="per_barrio">Barrio:</label>
                    </div>            
                    <div class="col-lg-3 paddin-no-left">                    
                        <label for="dep_codigo_res">Departamento Residencia:</label>
                    </div> 
                    <div class="col-lg-3 paddin-no-left">                               
                        <label for="mun_codigo_res">Municipio Residencia:</label>
                    </div>  
                     <div class="col-lg-2 paddin-no-left">                               
                        <label for="pai_codigo">Pais:</label>
                    </div>                         
                </div>  

                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-2">                               
                            <input type="text" class="form-control input-sm " id="per_complemento_direccion" name="per_complemento_direccion" placeholder="Ejm:(6a # 47 ­ 205)" >
                            <label for="per_complemento_direccion" class="label label-danger error" generated="true"></label>                      
                        </div>
                        <div class="col-lg-2 paddin-no-left">                    
                            <input type="text" class="form-control input-sm " id="per_barrio" name="per_barrio" placeholder="Introduce un Barrio" >
                            <label for="per_barrio" class="label label-danger error" generated="true"></label>                       
                        </div>                                           
                        <div class="col-lg-3 paddin-no-left">                              
                            <select class="form-control input-sm opcion required" id="dep_codigo_res" name="dep_codigo_res">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select>".$fila['dep_nombre']."</option>";
                                    }           
                                ?> 

                            </select>         
                        </div>                  
                        <div class="col-lg-3 paddin-no-left">                    
                            <select class="form-control input-sm" id="mun_codigo_res" name="mun_codigo_res">                        
                            </select>                    
                        </div>  
                        <div class="col-lg-2 paddin-no-left">                              
                            <select class="form-control input-sm opcion required" id="pai_codigo" name="pai_codigo">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM paises ORDER BY pai_nombre ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['pai_codigo'] == 'COL')?"selected":"";
                                        echo "<option value='".$fila['pai_codigo']."' $select>".$fila['pai_nombre']."</option>";
                                    }           
                                ?> 

                            </select>         
                        </div> 
                    </div>     
                </div>    

                <div class="row">
                    <div class="col-lg-4">                               
                        <label for="per_telefono_fijo">Teléfono/s Fijo/s:</label>
                    </div>
                    <div class="col-lg-3 paddin-no-left">                    
                        <label for="per_celular">Celular/es:</label>
                    </div>                  
                    <div class="col-lg-3 paddin-no-left">                    
                        <label for="per_puesto">Puesto de Votación:</label>
                    </div>            
                    <div class="col-lg-2 paddin-no-left">  
                        <label for="per_mesa_votacion">Mesa de Votación:</label>
                    </div>  
                </div>               

                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-4">                               
                            <input type="text" class="form-control input-sm" id="per_telefono_fijo" name="per_telefono_fijo" placeholder="Ejm: 7302626, 7205693, ..." >
                            <label for="per_telefono_fijo" class="label label-danger error" generated="true"></label>                                       
                        </div>
                        <div class="col-lg-3 paddin-no-left">                    
                            <input type="text" class="form-control input-sm" id="per_celular" name="per_celular" placeholder="Ejm: 3017838383, 3174545454, ..." >
                            <label for="per_celular" class="label label-danger error" generated="true"></label> 
                        </div>                 
                        <div class="col-lg-3 paddin-no-left">                    
                            <input type="text" class="form-control input-sm" id="per_puesto" name="per_puesto" placeholder="Introduce el Puesto " >
                            <label for="per_puesto" class="label label-danger error" generated="true"></label> 
                        </div>            
                        <div class="col-lg-2 paddin-no-left">  
                            <input type="text" class="form-control input-sm " id="per_mesa_votacion" name="per_mesa_votacion" placeholder="Introduce Mesa de Votación">
                            <label for="per_mesa_votacion" class="label label-danger error" generated="true"></label>   
                        </div>         
                    </div>     
                </div> 

                <div class="row">
                    <div class="col-lg-2">  
                        <label for="per_email">Email:</label>
                    </div> 
                    <div class="col-lg-2 paddin-no-left">                               
                        <label for="per_facebook">Facebook:</label>
                    </div>
                    <div class="col-lg-3 paddin-no-left">                    
                        <label for="per_twiter">Twitter:</label>
                    </div>                          
                    <div class="col-lg-3 paddin-no-left">                    
                        <label for="per_instagram">Instagram:</label>
                    </div>            
                    <div class="col-lg-2 paddin-no-left">  
                        <label for="per_wsp">Whatsapp:</label>
                    </div>  
                </div> 

                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-2">  
                            <input type="email" class="form-control input-sm " id="per_email" name="per_email" placeholder="Introduce un Email">
                            <label for="per_email" class="label label-danger error" generated="true"></label>   
                        </div> 
                        <div class="col-lg-2 paddin-no-left">                               
                            <input type="text" class="form-control input-sm" id="per_facebook" name="per_facebook" placeholder="Introduce el Facebook" >
                            <label for="per_facebook" class="label label-danger error" generated="true"></label>                                       
                        </div>
                        <div class="col-lg-3 paddin-no-left">                    
                            <input type="text" class="form-control input-sm" id="per_twiter" name="per_twiter" placeholder="Introduce el Twitter" >
                            <label for="per_twiter" class="label label-danger error" generated="true"></label> 
                        </div>                            
                        <div class="col-lg-3 paddin-no-left">                    
                            <input type="text" class="form-control input-sm" id="per_instagram" name="per_instagram" placeholder="Introduce el Instagram " >
                            <label for="per_instagram" class="label label-danger error" generated="true"></label> 
                        </div>            
                        <div class="col-lg-2 paddin-no-left">  
                            <input type="text" class="form-control input-sm " id="per_wsp" name="per_wsp" placeholder="Introduce el Whatsapp">
                            <label for="per_wsp" class="label label-danger error" generated="true"></label>   
                        </div>         
                    </div>     
                </div> 
                
                <div class="row">
                    <div class="col-lg-10">                               
                        <label for="per_notas">Notas/Observaciones:</label>
                    </div>     
                    <div class="col-lg-2 paddin-no-left">                               
                        <label for="fun_codigo">Función:</label>
                    </div>                        
                </div> 

                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-10">                               
                            <input type="text" class="form-control input-sm " id="per_notas" name="per_notas" placeholder="Introduce una Nota">
                            <label for="per_notas" class="label label-danger error" generated="true"></label>                                    
                        </div>    
                        <div class="col-lg-2 paddin-no-left">                               
                            <select class="form-control input-sm opcion required" id="fun_codigo" name="fun_codigo">
                            
                                <?php
                                    $con = new clsConexion();
                                    $res = $con->prepare("SELECT * FROM funciones ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){                                        
                                        echo "<option value='".$fila['fun_codigo']."' >".$fila['fun_nombre']."</option>";
                                    }           
                                ?> 

                            </select>                                        
                        </div>                                 
                    </div>     
                </div> 

                <?php if($modPermiso == "ACR"): ?>

                    <div class="row">
                        <div class="col-lg-6">                               
                            <label for="per_recomendado">Recomendado Por:</label>
                        </div>
                        <div class="col-lg-6 paddin-no-left">                    
                            <label for="per_credencial_numero">Credencial Numero:</label>
                        </div>                
                    </div> 

                    <div class="form-group">  
                        <div class="row">
                            <div class="col-lg-6">                               
                                <input type="text" class="form-control input-sm " id="per_recomendado" name="per_recomendado" placeholder="Introduce quien lo Recomendo">
                                <label for="per_recomendado" class="label label-danger error" generated="true"></label>                                    
                            </div>
                            <div class="col-lg-6 paddin-no-left">                    
                                <input type="text" class="form-control input-sm" id="per_credencial_numero" name="per_credencial_numero" placeholder="Introduce la Credencial Numero " >
                                <label for="per_credencial_numero" class="label label-danger error" generated="true"></label> 
                            </div>                       
                        </div>     
                    </div>  

                <?php endif;?>                  

                <div class="row">
                    <div class="col-lg-6">                               
                        <label for="per_ruta_foto">Foto:</label>
                    </div>
                    <div class="col-lg-6 paddin-no-left">                    
                        <label for="per_ruta_cedula">Cedula:</label>
                    </div>               
                </div>   

                <div class="form-group">  
                    <div class="row">
                        <div class="col-lg-6">                               
                            <?php if(isset($strJSON->ver)): ?>
                                <div class="form-group">                    
                                    <img class="img-perfil" id="img-perfil" style="display:none;" />                    
                                </div>   
                            <?php else:?> 
                                <div class="form-group">                    
                                    <input id="per_ruta_foto" name="per_ruta_foto" type="file" accept="image/gif, image/jpeg, image/png" class="file" data-show-upload="false" data-show-caption="true">
                                    <label for="per_ruta_foto" id="extFoto" style="display:none;" class="label label-success">Posee una imagen</label>
                                </div>   
                            <?php endif?>                                    
                        </div>
                        <div class="col-lg-6 paddin-no-left">                    
                            <?php if(isset($strJSON->ver)): ?>
                                <div class="form-group">                    
                                    <img class="img-perfil" id="img-cedula" style="display:none;" />                    
                                </div>   
                            <?php else:?> 
                                <div class="form-group">                                    
                                    <input type="file" class="form-control input-sm file" id="per_ruta_cedula" accept="image/gif, image/jpeg, image/png"  name="per_ruta_cedula" data-show-upload="false" data-show-caption="true" >                                        
                                    <label for="per_ruta_cedula" id="extFotoCedula" style="display:none;" class="label label-success">Posee una imagen</label>
                                </div>  
                            <?php endif?> 
                        </div>                          
                    </div>     
                </div>      
                                         
            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>                            
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.min.js"></script>