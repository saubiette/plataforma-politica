<?php
header('Content-type: text/json');
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("select * from calendario"); 
$pru->execute();

$arrayCon = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

$array = [];

foreach ($arrayCon as $key => $value) {

    $array[$key]['id'] = $value['cal_codigo'];
    $array[$key]['title'] = $value['cal_titulo'];
    $array[$key]['start'] = $value['cal_fecha']."T00:00:00-04:00";
    $array[$key]['end'] = $value['cal_fecha']."T00:40:00-04:00";
    $array[$key]['commentary'] = $value['cal_comentario'];

}

echo  json_encode($array);

?>