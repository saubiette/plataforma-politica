<?php
require '../../../php/conexion.php';

//--------------------------------------------------------------------------------------------------
// This script reads event data from a JSON file and outputs those events which are within the range
// supplied by the "start" and "end" GET parameters.
//
// An optional "timezone" GET parameter will force all ISO8601 date stings to a given timezone.
//
// Requires PHP 5.2.0 or higher.
//--------------------------------------------------------------------------------------------------

// Require our Event class and datetime utilities
require dirname(__FILE__) . '/utils.php';

// Short-circuit if the client did not give us a date range.
if (!isset($_GET['start']) || !isset($_GET['end'])) {
	die("Please provide a date range.");
}

// Parse the start/end parameters.
// These are assumed to be ISO8601 strings with no time nor timezone, like "2013-12-29".
// Since no timezone will be present, they will parsed as UTC.
$range_start = parseDateTime($_GET['start']);
$range_end = parseDateTime($_GET['end']);

// Parse the timezone parameter if it is present.
$timezone = null;
if (isset($_GET['timezone'])) {
	$timezone = new DateTimeZone($_GET['timezone']);
}

// Read and parse our events JSON file into an array of event data arrays.
//$json = file_get_contents(dirname(__FILE__) . '/arrays.php');

$con = new clsConexion();

$pru = $con->prepare("SELECT * , j.usu_nombre AS j_nombre, d.usu_nombre AS d_nombre 
					  FROM calendario
					  INNER JOIN municipios ON calendario.mun_codigo = municipios.mun_codigo 
			   		  INNER JOIN departamentos ON municipios.dep_codigo = departamentos.dep_codigo
					  INNER JOIN detalles_agenda ON calendario.cal_codigo = detalles_agenda.agenda_id
					  INNER JOIN jefe_color ON jefe_color.jefe_id = detalles_agenda.jefe_id
					  INNER JOIN usuario AS j ON detalles_agenda.jefe_id = j.usu_codigo
					  INNER JOIN usuario AS d ON detalles_agenda.delegado_id = d.usu_codigo
			   		  "); 

$pru->execute();

$arrayCon = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

$array = [];

foreach ($arrayCon as $key => $value) {

	$hora_inicio = date("g:i a", strtotime($value['cal_hora_inicio']));
	$hora_fin = date("g:i a", strtotime($value['cal_hora_fin']));

    $array[$key]['id'] = $value['cal_codigo'];
    $array[$key]['title'] = $value['cal_titulo'];
    $array[$key]['start'] = $value['cal_fecha']."T".$value['cal_hora_inicio']."-04:00";
    $array[$key]['end'] = $value['cal_fecha']."T".$value['cal_hora_fin']."-04:00";
	$array[$key]['commentary'] = "<b>Identificación:</b> ".$value['cal_identificacion']." - <b>Nombre:</b> ".$value['cal_nombre']." - <b>Teléfono/Celular:</b> ".$value['cal_telefono']." <br> 
								  <b>Departamento:</b> ".$value['dep_nombre']." - <b>Municipio:</b> ".$value['mun_nombre']."<br> 
	                              <b>Comentario: </b>".$value['cal_comentario'];
	$array[$key]['color'] = $value['color'];

	// tooltip
	$array[$key]['municipio'] = $value['mun_nombre'];
	$array[$key]['jefe'] = $value['j_nombre'];
	$array[$key]['delegado'] = $value['d_nombre'];
	$array[$key]['fecha'] = $value['cal_fecha'];
	$array[$key]['hora_init'] = $hora_inicio;
	$array[$key]['hora_end'] = $hora_fin;
	$array[$key]['organizador'] = $value['cal_nombre'];
	$array[$key]['telefono'] = $value['cal_telefono'];

}

//echo  json_encode($array);

$input_arrays = $array;

// Accumulate an output array of event data arrays.
$output_arrays = array();
foreach ($input_arrays as $array) {

	// Convert the input array into a useful Event object
	$event = new Event($array, $timezone);

	// If the event is in-bounds, add it to the output
	if ($event->isWithinDayRange($range_start, $range_end)) {
		$output_arrays[] = $event->toArray();
	}
}

// Send JSON to the client.
echo json_encode($output_arrays);