$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  var initialLocaleCode = 'es'
 

  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today, Pdf',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    customButtons: {
      Pdf:{
        // icon: 'fadsd fa-file-pdf-o',
        text: "Generar PDF",
        click:function(){
          //window.open('Containers/conCrearCalendario/pdf.php', '_blank');
          var varJson = { }
          varJson.TipUsuario = '-'
          crearVM_JSON('vmPDF', varJson)
          return false
        }
      }
    },
    viewDisplay : function(view) {
      $(".fc-button-month").find(".fc-button-content").addClass("icocon");
  },
    timeFormat: 'hh:mm a',
    now: datetime_string,
    locale: initialLocaleCode,
    buttonIcons: false,
    weekNumbers: true,
    editable: false,
    navLinks: true, // can click day/week names to navigate views
    eventLimit: true, // allow "more" link when too many events
    eventRender: function(eventObj, $el, ) {
      $el.popover({
        title: eventObj.title,

          content: '<strong>Municipio:</strong> '
                  + eventObj.municipio
                  + '<br>'
                  +'<strong>Jefe:</strong> ' +eventObj.jefe
                  + '<br>'
                  + '<strong>Delegado:</strong> '+eventObj.delegado
                  + '<br>'
                  + '<strong>Fecha Reunión:</strong> ' + eventObj.fecha
                  + '<br>'
                  + '<strong>Inicio:</strong> ' + eventObj.hora_init 
                  + '&nbsp; &nbsp;' 
                  + '<strong>Final:</strong> ' + eventObj.hora_end
                  + '<br>'
                  + '<strong>Organizador:</strong> ' + eventObj.organizador
                  + '<br>'
                  + '<strong>Celular:</strong> '+ eventObj.telefono
        ,
        html: true,
        trigger: 'hover',
        placement: 'top',
        container: 'body'
      });
    },    
    events: {
      url: rutaContenedorActual + '/get-events.php',
      error: function () {
        $('#script-warning').show()
      },
      // mun_nombre: mun_nombre
    },
    loading: function (bool) {
      $('#loading').toggle(bool)
    },
    eventClick: function (event) {
      console.log(event.id);
      var id = event.id;
      var title = event.title;
      console.log(id)
      console.log(title)

      // alert('dd')
      // opens events in a popup window
      // window.open(event.url, 'gcalevent', 'width=700,height=600')
      // $('#calendar').fullCalendar('refetchEvents')      

      var varJson = { }
      if ($('#nivel_acceso').val() != 'S') {
        varJson.ver = true
      }
      

      if (title == '** Candidato no disponible  **') {
        modVM_JSON('vmBloqueo', id, varJson)
        return false
      }


      modVM_JSON('vmAgenda', id, varJson)
      return false
    },
    dayClick: function (date, jsEvent, view) {
      console.log(date.format())
    // console.log(date);				 
    // alert("dia")
    // $(this).css('background-color', 'red')
    }
  })
})
function pdf(data){
  var varJson = { }
  varJson.fecha = data

  crearVM_JSON('pdf')
  return false
}
function crearAgenda (data) {
  // console.log(data)
  // alert(data)
  var varJson = { }
  varJson.fecha = data

  crearVM_JSON('vmAgenda', varJson)
  return false
// $('#calendar').fullCalendar('refetchEvents')
}

function crearBloqueo (data) {
  // console.log(data)
  // alert(data)
  var varJson = { }
  varJson.fecha = data

  crearVM_JSON('vmBloqueo', varJson)
  return false
// $('#calendar').fullCalendar('refetchEvents')
}

function Modificar (id) {
  var varJson = { }
  varJson.TipUsuario = 'I'

  modVM_JSON('vmUsuarios', id, varJson)
  return false
}

function Ver (id) {
  var varJson = { }
  varJson.TipUsuario = 'L'
  varJson.ver = true

  modVM_JSON('vmUsuarios', id, varJson)
  return false
}
