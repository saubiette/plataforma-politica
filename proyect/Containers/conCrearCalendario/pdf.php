<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Cargamos la librería dompdf que hemos instalado en la carpeta dompdf
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;



session_start();
  require "../../../php/conexion.php";
  require "../../generalesPHP/funciones.php";
// Introducimos HTML de prueba

  $inicio = $_POST['pdf_inicio'];
  $fin = $_POST['pdf_fin'];

      $con = new clsConexion();

  $res = $con->prepare("SELECT * FROM calendario INNER JOIN detalles_agenda ON calendario.cal_codigo = detalles_agenda.agenda_id WHERE  cal_fecha >= '".$inicio."' AND cal_fecha <= '".$fin."' ORDER BY cal_fecha");


    $res->execute();
    

 $html='<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Agenda</title>
    <style media="screen">

      html, body {
          background-color: #fff;
          font-family: Verdana, Geneva, sans-serif;
          font-weight: 200;
          font-size: 11px;
      }
      .tableTitle{
        width: 100%;
        border: 2px solid;
        border-right: 0px;
        border-left: 0px;
      }
      table{
        width: 100%;
      }
      .title{
        text-align: center;
      }
      .pg{
        float: right;
      }
      .content{
        border: 2px solid;
        border-top: 0px;
        border-left: 0px;
        border-right: 0px;
      }
      table{
        border-collapse: collapse;
        table-layout:fixed;
      }
    </style>
  </head>
  <body>
    <p class="pg"><strong>
      
    </strong></p>
      <h2 class="title">INFORME DE AGENDA</h2>
<table>
  <thead>
    <tr>
      <th colspan="4">  
        <p><strong>COORDINADOR/JEFE: </strong></p> </th>
      <th colspan="5">
        <P><strong>DELEGADO: </strong></P>
      </th>    
  </tr>
  </thead>
</table>
<table >
  <thead class="tableTitle">
    <tr>
      <th>FECHA</th>
      <th>HORA INICIO</th>
      <th>HORA FIN</th>
      <th>ORGANIZADOR</th>
      <th>CELULARES</th>
      <th>DIRECCION</th>
      <th>No. perdoas</th>      
    </tr>
  </thead>
      <tbody>
      <tr><td colspan="7">&nbsp;</td></tr>';
$dato = array();
$c=0;
       foreach($res as $value){
        if ($c==0) {
          $dato1 ='<tr><td>'.$value["cal_fecha"].'</td><td>'.$value["cal_hora_inicio"].'</td><td>'.$value["cal_hora_fin"].'</td><td>'.$value["cal_nombre"].'</td><td>'.$value["cal_telefono"].'</td><td>'.$value["cal_direccion"].'</td><td>'.$value["n_personas"].'</td></tr>';
        }else{
          $dato[$c] ='<tr><td>'.$value["cal_fecha"].'</td><td>'.$value["cal_hora_inicio"].'</td><td>'.$value["cal_hora_fin"].'</td><td>'.$value["cal_nombre"].'</td><td>'.$value["cal_telefono"].'</td><td>'.$value["cal_direccion"].'</td><td>'.$value["n_personas"].'</td></tr>';
        }
  
        $c++;
      }
      $fin='</tbody>
</table>


  
  </body>
</html>';

$longitud = count($dato);
for ($i=1; $i <= $longitud; $i++) {
    if ($i == 1) {
      $datos=$dato1.$dato[$i];
    }else{
      $datos=$datos.$dato[$i];
    }
}

//$datos=$dato[1].$dato[2].$dato[3].$dato[4];
 

 if ($longitud = 1) {
   $html = $html.$dato1.$fin;
 }else{
  $html = $html.$datos.$fin;
 }

 
// Instanciamos un objeto de la clase DOMPDF.
$pdf = new DOMPDF();
 
// Definimos el tamaño y orientación del papel que queremos.
$pdf->set_paper("letter", "portrait");
//$pdf->set_paper(array(0,0,104,250));
 
// Cargamos el contenido HTML.
$pdf->load_html(utf8_decode($html));
 
// Renderizamos el documento PDF.
$pdf->render();
// Enviamos el fichero PDF al navegador.
$canvas = $pdf->get_canvas();
$font = $pdf->getFontMetrics()->get_font("helvetica", "bold");

$canvas->page_text(72, 18, "Pagina: {PAGE_NUM} de {PAGE_COUNT}", $font, 6, array(0,0,0));
//$pdf->stream('Agenda.pdf');

function file_get_contents_curl($url) {
	$crl = curl_init();
	$timeout = 5;
	curl_setopt($crl, CURLOPT_URL, $url);
	curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
	$ret = curl_exec($crl);
	curl_close($crl);
	return $ret;
}

$pdf->stream();