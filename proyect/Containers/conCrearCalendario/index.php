    <?php
    session_start();
        require "../../../php/conexion.php";
        require "../../generalesPHP/funciones.php";
        
        $strCampo = fncPermiso("CAL","R", new clsConexion()); 
        
        if(!$strCampo){
            echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
            exit();
        }

        $datetime = new DateTime("America/Bogota");
        $datetime_string = $datetime->format('c');
    ?>


    <script>
    var datetime_string = "<?php echo $datetime_string; ?>";        
    </script>

    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Calendario</li>            
        </ol>  
        
        <div id='script-warning'>
            <code>php/get-events.php</code> Paso algo, comuniquese con el administrador.
        </div>

        <div id='loading'>loading...</div>

        <div id='calendar'></div>           
        
    </div>

    
    <input type="hidden" id="nivel_acceso" name="nivel_acceso" value="<?php echo $_SESSION["usu_nivel_acceso"] ?>">   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>
    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/script.js"></script>