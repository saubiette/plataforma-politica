<?php
session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'personas';
    $ideCampo = "per_codigo";
    $ideValor = $_POST['id'];    

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        if(isset($_POST['tp']))
            $modPermiso = $_POST['tp'];
        else
            $modPermiso = "RVO";

        $strCampo = fncPermiso($modPermiso, $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }

    $strPerTipo = "VT";

    if(isset($_POST['tp'])){
        switch ($_POST['tp']) {
            case 'ACR': $strPerTipo = "CR"; break;                    
        }        
    }
       
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("per_nombre",$_POST['per_nombre']);              
            $SQLcomando->set_AddCampo("per_apellido",$_POST['per_apellido']);              
            $SQLcomando->set_AddCampo("per_fecha_nacimiento",fncFechaNULL($_POST['per_fecha_nacimiento']), false);
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']);

            $estatura = ($_POST['per_estatura'] != "")?$_POST['per_estatura']:0;
            $SQLcomando->set_AddCampo("per_estatura", $estatura);

            $SQLcomando->set_AddCampo("per_gsrh",$_POST['per_gsrh']);
            $SQLcomando->set_AddCampo("per_genero",$_POST['per_genero']);
            $SQLcomando->set_AddCampo("per_fecha_expedicion",fncFechaNULL($_POST['per_fecha_expedicion']), false);               
            $SQLcomando->set_AddCampo("per_estado_civil",$_POST['per_estado_civil']);
            $SQLcomando->set_AddCampo("per_tipo_direccion",$_POST['per_tipo_direccion']);
            $SQLcomando->set_AddCampo("per_direccion_1",$_POST['per_direccion_1']);
            $SQLcomando->set_AddCampo("per_direccion_2",$_POST['per_direccion_2']);
            $SQLcomando->set_AddCampo("per_direccion_3",$_POST['per_direccion_3']);
            $SQLcomando->set_AddCampo("per_complemento_direccion",$_POST['per_complemento_direccion']);
            $SQLcomando->set_AddCampo("per_barrio",$_POST['per_barrio']);             
            $SQLcomando->set_AddCampo("per_telefono_fijo",$_POST['per_telefono_fijo']);
            $SQLcomando->set_AddCampo("per_celular",$_POST['per_celular']);
            $SQLcomando->set_AddCampo("per_email",$_POST['per_email']);
            $SQLcomando->set_AddCampo("per_notas",$_POST['per_notas']);    
            
            $SQLcomando->set_AddCampo("per_puesto",$_POST['per_puesto']);    
            $SQLcomando->set_AddCampo("per_mesa_votacion",$_POST['per_mesa_votacion']);                 

            $SQLcomando->set_AddCampo("mun_codigo_exp",$_POST['mun_codigo_exp']);

            $SQLcomando->set_AddCampo("pai_codigo",$_POST['pai_codigo']);
            $SQLcomando->set_AddCampo("mun_codigo_res",$_POST['mun_codigo_res']);

            if(isset($_POST['per_recomendado'])) $SQLcomando->set_AddCampo("per_recomendado",$_POST['per_recomendado']);
            if(isset($_POST['per_credencial_numero'])) $SQLcomando->set_AddCampo("per_credencial_numero",$_POST['per_credencial_numero']);

            $SQLcomando->set_AddCampo("per_facebook",$_POST['per_facebook']);
            $SQLcomando->set_AddCampo("per_twiter",$_POST['per_twiter']);
            $SQLcomando->set_AddCampo("per_instagram",$_POST['per_instagram']);
            $SQLcomando->set_AddCampo("per_wsp",$_POST['per_wsp']);

            $SQLcomando->set_AddCampo("pro_codigo",$_POST['pro_codigo']);
            $SQLcomando->set_AddCampo("fun_codigo",$_POST['fun_codigo']);
                       
            if($_POST['id'] != ""){                              

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                       

            } else {                                                                                                       

                $resp = $conexion->prepare("SELECT $ideCampo 
                                            FROM   $tabla 
                                            WHERE  per_identificacion = '".$_POST["per_identificacion"]."' AND per_tipo = '$strPerTipo' ");
                $resp->execute();
                $maxV = $resp->rowCount();
            
                if ($maxV > 0){ 
                    echo false;    
                    $conexion->rollBack();   
                    exit();                   
                }

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);

                $SQLcomando->set_AddCampo("tdo_codigo",$_POST['tdo_codigo']);
                $SQLcomando->set_AddCampo("per_identificacion",$_POST['per_identificacion']);                
                $SQLcomando->set_AddCampo("per_fecha_registro","CURRENT_DATE",false);

                $SQLcomando->set_AddCampo("per_pistola",$_POST['per_pistola']);            

                $SQLcomando->set_AddCampo("per_ruta_foto","");   
                $SQLcomando->set_AddCampo("per_ruta_cedula","");                                                           
                                                                                   
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                $SQLcomando->set_AddCampo("per_tipo",$strPerTipo);

                $SQLcomando->set_tipo_comando("INSERT");   

            }

            if($_FILES['per_ruta_foto']['name'] != ""){

                $foto = $_FILES['per_ruta_foto'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;

                $RutImg = "per_".$RutImg;
                
                $ruta = "../../images/Personas/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("per_ruta_foto",$RutImg);                

            }   

            if($_FILES['per_ruta_cedula']['name'] != ""){

                $foto = $_FILES['per_ruta_cedula'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;
                
                $RutImg = "ced_".$RutImg;                

                $ruta = "../../images/Personas/Cedulas/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("per_ruta_cedula",$RutImg);     
            }                 
            
            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute();           
            
            //validar para agregar a reuniones
            if(isset($_POST['idReunion'])){

                if($_POST['idReunion'] != ""){

                    $exitReu = $conexion->prepare("SELECT rpe_codigo 
                                                    FROM   reuniones_personas 
                                                    WHERE  reu_codigo = '".$_POST["idReunion"]."' AND per_codigo = '".$ideValor."' ");

                    $exitReu->execute();
                    $maxV = $exitReu->rowCount();
                
                    if (!($maxV > 0)){ 
                        //TODO: Ccomando SQL desde cero
                        $SQLcomandoReu = new SQLComando();
                        $SQLcomandoReu->set_tabla("reuniones_personas");

                        $rpe_codigo = MaxCampo("reuniones_personas","rpe_codigo",$conexion);//te devuelve +1                
                        $SQLcomandoReu->set_AddCampo("rpe_codigo",$rpe_codigo);

                        $SQLcomandoReu->set_AddCampo("reu_codigo",$_POST['idReunion']);
                        $SQLcomandoReu->set_AddCampo("per_codigo",$ideValor);                
                        $SQLcomandoReu->set_AddCampo("rpe_fecha_registro","CURRENT_DATE",false);                                                                                
                                                                                        
                        $SQLcomandoReu->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);                       

                        $SQLcomandoReu->set_tipo_comando("INSERT");   

                        $resp = $conexion->prepare($SQLcomandoReu->get_sql());
                        $resp->execute();
                                       
                    }

                }
            } 
            
            //validar para agregar a lider votantes
            if($modPermiso == "LIV"){

                $strSQL = "SELECT * 
                            FROM lider_personas 
                            WHERE per_codigo = '$ideValor' && usu_codigo = '".$_POST['usuCodigoLider']."' ";
                
                $resLP = $conexion->prepare($strSQL);
                $resLP->execute();

                if($resLP->rowCount() == 0){                
                
                    //TODO: Ccomando SQL desde cero
                    $SQLcomando->itemsClear();
                    $SQLcomando->set_tabla('lider_personas');

                    $lpeCodigo = MaxCampo("lider_personas","lpe_codigo",$conexion);//te devuelve +1                
                    $SQLcomando->set_AddCampo("lpe_codigo",$lpeCodigo);

                    $SQLcomando->set_AddCampo("per_codigo",$ideValor);
                    $SQLcomando->set_AddCampo("lpe_fecha_registro","CURRENT_DATE",false);                                                                                   
                    $SQLcomando->set_AddCampo("usu_codigo",$_POST['usuCodigoLider']);

                    $SQLcomando->set_tipo_comando("INSERT");   

                    $resp = $conexion->prepare($SQLcomando->get_sql());
                    $resp->execute(); 

                }

            }
            
            echo (int)$ideValor;            
            break;   
            
        case 'D':                
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * , 
                             municipios.mun_codigo as mun_codigo_Per, municipios.dep_codigo as dep_codigo,
                             municipio_exp.dep_codigo as dep_codigo_exp,
                             municipio_res.dep_codigo as dep_codigo_res
                        FROM $tabla INNER JOIN 
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo INNER JOIN
                             municipios as municipio_exp ON $tabla.mun_codigo_exp = municipio_exp.mun_codigo INNER JOIN
                             municipios as municipio_res ON $tabla.mun_codigo_res = municipio_res.mun_codigo INNER JOIN
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;    

        case 'clienteExiste':

            $tp =  "";
            if(isset($_POST["tp"])){$tp = $_POST["tp"];}
            
            $WHERE = ($_POST['idReunion'] != "" && $tp != "LIV")? "":"AND per_tipo = '$strPerTipo'";           

             $strSQL = "SELECT * 
                        FROM $tabla                              
                        WHERE per_identificacion = '$ideValor' $WHERE";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();             

             $respestaJSON = new stdClass();
             $respestaJSON->TieneLider = false;

             session_start();
             $respestaJSON->nivelUsuario = $_SESSION['usu_nivel_acceso'];

             if($resp->rowCount() > 0){
                $resM = $resp->fetchAll();
                $respestaJSON->datos = $resM[0];
                $respestaJSON->existe = true;

                if($tp == "LIV"){                    

                    $strSQL = "SELECT * 
                               FROM lider_personas 
                               WHERE per_codigo = '".$resM[0][0]."' AND  usu_codigo != '".$_POST['usuCodigoLider']."' ";
                    
                    $resLP = $conexion->prepare($strSQL);
                    $resLP->execute();

                    if($resLP->rowCount() > 0){
                        $respestaJSON->TieneLider = true;                                                               
                    } 
                }
             }
             else{
                 $respestaJSON->existe = false;
            }          

             echo json_encode($respestaJSON);            
             break;    

        case 'AFI':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("per_tipo","CR");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
        
         case 'DFI':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("per_tipo","VT");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break; 
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>