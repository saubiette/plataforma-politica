<?php
header('Content-Type: text/html; charset=UTF-8');

	require '../../generalesPHP/cerrarSesion.php';
	require '../../generalesPHP/fpdf/fpdf.php';
	require_once "../../../php/conexion.php";
	
	$conexion = new clsConexion();
	
	class PDF extends FPDF {
		//Cabecera de p�gina
		function Header() {
			//Logo
			//$this->Image('../images/logocontrato.png',20,8,40);
			//Arial bold 15
			$this->SetFont('Arial','B',15);
			//Movernos a la derecha
			$this->Cell(80);
			//T�tulo
			$this->Cell(30,10,'',0,0,'C');
			//Salto de l�nea
			$this->Ln(3);
		}
	}

	ob_end_clean();
	ob_start();	
		
	$cooCodigo = (isset($_GET["cc"]))? $_GET["cc"]: "";	
	$gesCodigo = (isset($_GET["gc"]))? $_GET["gc"]: "";		
		
	//AUTOMOTORES
	$sql =	"SELECT * FROM	coordinadores ".(($cooCodigo != "")?"WHERE  coo_codigo = '$cooCodigo'":"");
	
	$cordinadores = $conexion->prepare($sql);
	$cordinadores->execute();	
		
	while($coo = $cordinadores->fetch()){$coordinadoresArray[] = $coo;}	
	
	$pdf=new PDF('L','mm',array(300, 535));
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(20,0,10);

	$pdf->SetFont('Arial','B',11);
	$pdf->Ln(15);
	$pdf->Cell(500,6, utf8_decode('REPORTE DE FIRMAS CONSOLIDADO'),1,0,'C');

	$pdf->SetFont('Arial','B',11);
	$pdf->Ln(10);	

	$TN_totForm = 0;
	$TN_totFormEnt = 0;
	$TN_totFormFal = 0;
	$TN_totFir = 0;
	$TN_totFirCon = 0;
	$TN_totFirErr = 0;

	$TN_SPtotValor = 0;
	$TN_PtotValor = 0;
	$TN_totValor = 0;

	foreach($coordinadoresArray as $clave => $valor){		

		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(170,5, "COORDINADOR->",1,0,'L');

		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(50,5, utf8_decode("IDENTIFICACIÓN:"),1,0,'L');

		$pdf->SetFont('Arial','',10);
		$pdf->Cell(45,5, utf8_decode(strtoupper($valor['coo_identificacion'])),1,0,'R');

		$pdf->SetFont('Arial','B',11);				
		$pdf->Cell(40,5, utf8_decode('NOMBRE:'),1,0,'L');	

		$pdf->SetFont('Arial','',10);	
		$pdf->Cell(90,5, utf8_decode(strtoupper($valor['coo_nombre']." ".$valor['coo_apellido'])),1,0,'L');

		$pdf->SetFont('Arial','B',11);				
		$pdf->Cell(75,5, utf8_decode('TIPO COORDINADOR:'),1,0,'L');	

		$pdf->SetFont('Arial','',10);	

		$tipCoordinador = "OTRO";

		switch ($valor['coo_tipo_coordinador']) {
			case 'RU': $tipCoordinador = "RURAL"; break;
			case 'UR': $tipCoordinador = "URBANO"; break;
			case 'MU': $tipCoordinador = "MUNICIPAL"; break;		
		}

		$pdf->Cell(30,5, utf8_decode(strtoupper($tipCoordinador)),1,0,'L');
		$pdf->Ln(10);

		$sqlTipDoc = "SELECT   *
					  FROM gestionadores
					  WHERE coo_codigo = '".$valor['coo_codigo']."' ".(($gesCodigo != "")?"AND ges_codigo = '$gesCodigo' ":"");
		
		$gestionador = $conexion->prepare($sqlTipDoc);
		$gestionador->execute();	

		$gestionadorArray = null;
			
		while($ges = $gestionador->fetch()){$gestionadorArray[] = $ges;}	

		if(count($gestionadorArray) == 0) continue;

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,5, "",0,0,'C');		
		$pdf->Cell(165,5, "> GESTIONADOR/ES",1,0,'C');
		$pdf->Cell(25,5, "Tot. Form.",0,0,'C');
		$pdf->Cell(40,5, "Tot. Form. Entregados",0,0,'C');
		$pdf->Cell(40,5, "Tot. Form. Faltantes",0,0,'C');
		$pdf->Cell(25,5, "Tot. Firmas",0,0,'C');
		$pdf->Cell(40,5, "Tot. Fir. Correctas",0,0,'C');
		$pdf->Cell(40,5, "Tot. Fir. Erroneas",0,0,'C');
		$pdf->Cell(40,5, "Total Sin Pagar",0,0,'C');
		$pdf->Cell(40,5, "Total Pagado",0,0,'C');
		$pdf->Cell(40,5, "Total Neto",0,0,'C');
		$pdf->Ln(5);

		$T_totForm = 0;
		$T_totFormEnt = 0;
		$T_totFormFal = 0;
		$T_totFir = 0;
		$T_totFirCon = 0;
		$T_totFirErr = 0;
		
		$T_SPtotValor = 0;
		$T_PtotValor = 0;
		$T_totValor = 0;

		foreach($gestionadorArray as $clave => $valorTP){			

			$pdf->SetFont('Arial','B',11);
			$pdf->Cell(5,5, "",0,0,'C');
			$pdf->Cell(36,5, utf8_decode("IDENTIFICACION:"),1,0,'C');

			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5, utf8_decode(strtoupper($valorTP['ges_identificacion'])),1,0,'R');

			$pdf->SetFont('Arial','B',11);
			$pdf->Cell(24,5, utf8_decode("NOMBRE:"),1,0,'C');

			$pdf->SetFont('Arial','',10);
			//$pdf->SetFillColor(255,100,255); 			
			$pdf->Cell(80,5, utf8_decode(strtoupper($valorTP['ges_nombre']." ".$valorTP['ges_apellido'])),1,0,'L');
		

			$sqlForm = "SELECT SUM(for_numero) as sumNumero
						FROM numero_formularios
						WHERE ges_codigo = '".$valorTP['ges_codigo']."'
						GROUP BY ges_codigo";
			
			$formularios = $conexion->prepare($sqlForm);
			$formularios->execute();

			$formulariosArray = null;
			while($for = $formularios->fetch()){$formulariosArray[] = $for;}	

			if(count($formulariosArray) == 0){
				$pdf->Cell(25,5,0,1,0,'C');	
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(25,5, 0,1,0,'C');
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1,0,'R');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1,0,'R');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1,0,'R');
				$pdf->Ln(5);
				continue;
			} 

			$totForm = $formulariosArray[0][0];
			$T_totForm  += $formulariosArray[0][0];
			
			$pdf->Cell(25,5, $formulariosArray[0][0],1,0,'C');	
			
			$sqlFormEnt = "SELECT SUM(efo_numero) as sumNumero, SUM(efo_firnas) as sumFirmas, SUM(efo_confirmadas) as sumFirConf, SUM(efo_error) as sumFirError
						   FROM entrega_formularios INNER JOIN
     							numero_formularios ON `entrega_formularios`.for_codigo = `numero_formularios`.for_codigo
						   WHERE ges_codigo = '".$valorTP['ges_codigo']."'
						   GROUP BY ges_codigo";
			
			$formulariosEnt = $conexion->prepare($sqlFormEnt);
			$formulariosEnt->execute();

			$formulariosEntArray = null;
			while($for = $formulariosEnt->fetch()){$formulariosEntArray[] = $for;}	

			if(count($formulariosEntArray) == 0){		
				$pdf->Cell(40,5, 0,1,0,'C');
				$T_totFormFal += $totForm;
				$pdf->Cell(40,5, $totForm,1,0,'C');
				$pdf->Cell(25,5, 0,1,0,'C');
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(40,5, 0,1,0,'C');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1,0,'R');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1,0,'R');
				$pdf->Cell(40,5, number_format(0,2,',','.'),1, 0.00 ,'R');		
				$pdf->Ln(5);
				continue;
			} 

			$totFormFal = $totForm - $formulariosEntArray[0][0];
			$T_totFormEnt += $formulariosEntArray[0][0];
			$T_totFormFal += $totFormFal;
			$T_totFir += $formulariosEntArray[0][1];
			$T_totFirCon += $formulariosEntArray[0][2];
			$T_totFirErr += $formulariosEntArray[0][3];

			$pdf->Cell(40,5, $formulariosEntArray[0][0],1,0,'C');
			$pdf->Cell(40,5, $totFormFal ,1,0,'C');
			$pdf->Cell(25,5, $formulariosEntArray[0][1],1,0,'C');
			$pdf->Cell(40,5, $formulariosEntArray[0][2],1,0,'C');
			$pdf->Cell(40,5, $formulariosEntArray[0][3],1,0,'C');	
			
			//TOTAL SIN PAGAR
			$sqlFormEnt = "SELECT SUM(efo_confirmadas) as sumFirConf
						   FROM entrega_formularios INNER JOIN
     							numero_formularios ON `entrega_formularios`.for_codigo = `numero_formularios`.for_codigo
						   WHERE ges_codigo = '".$valorTP['ges_codigo']."' AND  efo_pago = 'N' 
						   GROUP BY ges_codigo";
			
			$firmasSinPagar = $conexion->prepare($sqlFormEnt);
			$firmasSinPagar->execute();

			$firSinPagarArray = null;
			while($firSP = $firmasSinPagar->fetch()){$firSinPagarArray[] = $firSP;}	
			
			if(count($firSinPagarArray) > 0){	
				$totalValorSP = $firSinPagarArray[0][0] * 400;
				$T_SPtotValor += $totalValorSP;		
				$pdf->Cell(40,5, number_format($totalValorSP,2,',','.') ,1,0,'R');
			}else
				$pdf->Cell(40,5, number_format(0,2,',','.') ,1,0,'R');

			//TOTAL PAGADO
			$sqlFormEnt = "SELECT SUM(efo_confirmadas) as sumFirConf
						   FROM entrega_formularios INNER JOIN
     							numero_formularios ON `entrega_formularios`.for_codigo = `numero_formularios`.for_codigo
						   WHERE ges_codigo = '".$valorTP['ges_codigo']."' AND  efo_pago = 'S' 
						   GROUP BY ges_codigo";
			
			$firmaPagado = $conexion->prepare($sqlFormEnt);
			$firmaPagado->execute();

			$firPagarArray = null;
			while($forP = $firmaPagado->fetch()){$firPagarArray[] = $forP;}	
			
			if(count($firPagarArray) > 0){	
				$totalValorP = $firPagarArray[0][0] * 400;
				$T_PtotValor += $totalValorP;		
				$pdf->Cell(40,5, number_format($totalValorP,2,',','.') ,1,0,'R');
			}else
				$pdf->Cell(40,5, number_format(0,2,',','.') ,1,0,'R');

			//TOTAL NETO
			$totalValor = $formulariosEntArray[0][2] * 400;
			$T_totValor += $totalValor;			
			
			$pdf->Cell(40,5, number_format($totalValor,2,',','.') ,1,0,'R');			  
			$pdf->Ln(5);
		}

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,5, "",0,0,'C');		
		$pdf->Cell(165,5, "TOTAL",0,0,'R');
		$pdf->Cell(25,5, $T_totForm ,1,0,'C');	
		$pdf->Cell(40,5, $T_totFormEnt ,1,0,'C');
		$pdf->Cell(40,5, $T_totFormFal,1,0,'C');
		$pdf->Cell(25,5, $T_totFir,1,0,'C');
		$pdf->Cell(40,5, $T_totFirCon,1,0,'C');
		$pdf->Cell(40,5, $T_totFirErr,1,0,'C');
		//inicio
		$pdf->Cell(40,5, number_format($T_SPtotValor,2,',','.'),1, 0.00 ,'R');
		$pdf->Cell(40,5, number_format($T_PtotValor,2,',','.'),1, 0.00 ,'R');
		//fin
		$pdf->Cell(40,5, number_format($T_totValor,2,',','.'),1, 0.00 ,'R');	

		$TN_totForm += $T_totForm;
		$TN_totFormEnt += $T_totFormEnt;
		$TN_totFormFal += $T_totFormFal;
		$TN_totFir += $T_totFir;
		$TN_totFirCon += $T_totFirCon;
		$TN_totFirErr += $T_totFirErr;

		$TN_SPtotValor += $T_SPtotValor;
		$TN_PtotValor += $T_PtotValor;
		$TN_totValor += $T_totValor;

		$pdf->Ln(10);	

	}

	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(170,5, "",0,0,'C');	
	$pdf->Cell(330,5, "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",0,0,'C');	

	$pdf->Ln(5);
	
	$pdf->Cell(5,5, "",0,0,'C');	
	$pdf->Cell(165,5, "TOTAL NETO",0,0,'R');
	$pdf->Cell(25,5, $TN_totForm ,1,0,'C');	
	$pdf->Cell(40,5, $TN_totFormEnt ,1,0,'C');
	$pdf->Cell(40,5, $TN_totFormFal,1,0,'C');
	$pdf->Cell(25,5, $TN_totFir,1,0,'C');
	$pdf->Cell(40,5, $TN_totFirCon,1,0,'C');
	$pdf->Cell(40,5, $TN_totFirErr,1,0,'C');
	//incio
	$pdf->Cell(40,5, number_format($TN_SPtotValor,2,',','.'),1, 0.00 ,'R');	
	$pdf->Cell(40,5, number_format($TN_PtotValor,2,',','.'),1, 0.00 ,'R');	
	//fin
	$pdf->Cell(40,5, number_format($TN_totValor,2,',','.'),1, 0.00 ,'R');	
	

	/* --> FIN: RECORRIDO FACTORES */
		
	$pdf->Output();
	ob_end_flush(); 
?>