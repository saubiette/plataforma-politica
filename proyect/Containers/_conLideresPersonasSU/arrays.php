<?php
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT * 
                      FROM usuario INNER JOIN 
                           municipios ON usuario.mun_codigo = municipios.mun_codigo INNER JOIN
                           departamentos ON municipios.dep_codigo = departamentos.dep_codigo
                    WHERE usu_nivel_acceso = 'L' "); 
                    
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {

    $array[$key]['usu_login'] = "<a href=javascript:Ver('". $value['usu_codigo']."') alt='Ver' title='Ver'><b>".$value['usu_login']."</b></a>";

     switch($value['cat_codigo']){
        case '1': $categoria = "No Aplica";
                  break;
        case '2': $categoria = "Concejal";
                  break;
        case '3': $categoria = "Alcalde";
                  break;
        case '4': $categoria = "Empresario";
                  break;
        case '5': $categoria = "Candidato";
                  break;
    }    
    
    $array[$key]['categoria'] = $categoria;

    $array[$key]['usu_nivel_acceso'] =  ($array[$key]['usu_nivel_acceso'] == "S")?"Si":"No";

    if($array[$key]['usu_ruta_imagen'] != "" ){
        $array[$key]['usu_ruta_imagen'] = "<a data-lightbox='gal_".$array[$key]['usu_codigo']."' href='./images/perfiles/".$array[$key]['usu_ruta_imagen'].$strNoCache."' data-title='Foto'><img height=20 src='./images/perfiles/".$array[$key]['usu_ruta_imagen'].$strNoCache."'></a>";
    }

    if($array[$key]['usu_activo'] == "S"){
        $array[$key]['usu_activo'] = "<a href=javascript:Desactivar('". $value['usu_codigo']."')><img src=./images/on.png   alt='Desactivar' title='Desactivar' border=0></a>";
    }
    else{
        $array[$key]['usu_activo'] = "<a href=javascript:Activar('". $value['usu_codigo']."')><img src=./images/off.png   alt='Activar' title='Activar' border=0></a>";
    }   
    
    $array[$key]['mod'] = "<a href=javascript:Modificar('". $value['usu_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
    $array[$key]['eli'] = "<a href=javascript:Eliminar('". $value['usu_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
}

echo  json_encode($array);

?>