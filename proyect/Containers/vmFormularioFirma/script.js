$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM  

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        $('.dataTable').DataTable().ajax.reload()
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      var info = data.for_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#for_fecha').val(data.for_fecha)
      $('#for_numero').val(data.for_numero)
      $('#for_comentario').val(data.for_comentario)
      $('#ges_codigo').val(data.ges_codigo)

      if (auxJSON.ver != undefined) $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
    }, 'json')
  }

  $('#ges_codigo').val(auxJSON.ges_codigo)

  // $('.formatoMiles').numberFM(true, 2, ',', '.')

})
