<?php 
 session_start();
    require '../../../php/conexion.php'; 
    require "../../generalesPHP/funciones.php";

    $strJSON = json_decode($_POST['objJSON']);

    $modPermiso = "ADF";

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso($modPermiso,"R", new clsConexion()); 
        else
             $strCampo = fncPermiso($modPermiso,"U", new clsConexion());    
    else:
         $strCampo = fncPermiso($modPermiso,"C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar No. de Formularios</b></h2> (Fecha de Registro <span id='fecha_registro'></span>)
                    <?php else:?>
                        <h2><b>Registrar No. de Formularios</b></h2>
                    <?php endif?>   
                
                <?php else:?>  

                    <h2><b>Registro No. de Formularios</b></h2>
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                               
                
                <div class="form-group">
                    <label for="for_fecha">(*)Fecha:</label>
                    <input type="date" class="form-control input-sm required"  value="<?= date("Y-m-d"); ?>" id="for_fecha" name="for_fecha" >     
                    <label for="for_fecha" class="label label-danger error" generated="true"></label>               
                </div>       

                 <div class="form-group">
                    <label for="ges_codigo">Gestionador:</label>                    
                    <select class="form-control input-sm" id="ges_codigo" name="ges_codigo" required>
                         
                         <?php                           

                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM gestionadores");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['ges_codigo']."'>".$fila['ges_nombre']." ".$fila['ges_apellido']."</option>";
                            }           
                         ?>

                    </select>                     
                </div> 

                <div class="form-group">
                    <label for="for_numero">(*)No. Formularios:</label>
                    <input type="number" step="any" class="form-control input-sm required" id="for_numero" name="for_numero" placeholder="Introduce un No. de Formularios">  
                    <label for="for_numero" class="label label-danger error" generated="true"></label>                   
                </div>

                <div class="form-group">
                    <label for="for_comentario">Comentario:</label>
                    <textarea class="form-control" rows="5" id="for_comentario" name="for_comentario"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>           
                      
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>