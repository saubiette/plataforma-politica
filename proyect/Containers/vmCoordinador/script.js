$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  var porPistola = 'NO'

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    if (!/^([0-9])*$/.test($('#coo_identificacion').val())) {
      swal('Identificación no valida', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)

    console.log(porPistola)
    // return false

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        if (data == false) {
          swal('La Identificación ya existe', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }

        $('.dataTable').DataTable().ajax.reload()
        $('.dataTable').DataTable().search('').draw()
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('.file').fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif']
  })

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data.coo_ruta_foto != '') {
        $('#extFoto').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').prop('disabled', 'true')

        if (data.coo_ruta_foto != '') {
          $('#img-perfil').css('display', 'block')
          $('#img-perfil').attr('src', './images/Coordinadores/' + data.coo_ruta_foto)
        }
      }

      var info = data.coo_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#tdo_codigo').val(data.tdo_codigo)
      $('#tdo_codigo').prop('disabled', true)

      $('#coo_identificacion').val(data.coo_identificacion)
      $('#coo_identificacion').prop('disabled', true)

      $('#coo_nombre').val(data.coo_nombre)
      $('#coo_apellido').val(data.coo_apellido)
      $('#coo_tipo_coordinador').val(data.coo_tipo_coordinador)

      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      console.log(data.mun_codigo_Coo)
      $('#mun_codigo').val(data.mun_codigo_Coo)

      $('#coo_tipo_direccion').val(data.coo_tipo_direccion)

      $('#coo_direccion_1').val(data.coo_direccion_1)
      $('#coo_direccion_2').val(data.coo_direccion_2)
      $('#coo_direccion_3').val(data.coo_direccion_3)

      $('#coo_complemento_direccion').val(data.coo_complemento_direccion)
      $('#coo_barrio').val(data.coo_barrio)

      $('#coo_telefono_fijo').val(data.coo_telefono_fijo)
      $('#coo_celular').val(data.coo_celular)
      $('#coo_email').val(data.coo_email)
      $('#coo_notas').val(data.coo_notas)

      if (data.coo_ruta_cedula != '') {
        $('#extFotoCedula').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').attr('readonly', 'readonly')

        if (data.coo_ruta_cedula != '') {
          $('#img-cedula').css('display', 'block')
          $('#img-cedula').attr('src', './images/Coordinadores/Cedulas/' + data.coo_ruta_cedula)
        }
      }
    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()
})
