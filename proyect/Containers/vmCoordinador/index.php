<?php 
 session_start();
    require '../../../php/conexion.php'; 
    require "../../generalesPHP/funciones.php";

    $strJSON = json_decode($_POST['objJSON']);

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso("COG","R", new clsConexion()); 
        else
             $strCampo = fncPermiso("COG","U", new clsConexion());    
    else:
         $strCampo = fncPermiso("COG","C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }

?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <div>
                            <h2><b>Modificar Votante</b></h2> (Fecha de Registro <span id='fecha_registro'></span>)
                        </div>
                    <?php else:?>
                        <h2><b>Registrar Coordinador</b></h2>
                    <?php endif;?>         
                
                <?php else:?>  

                    <h2><b>Coordinador</b></h2> 
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif;?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                  

                <?php if(isset($strJSON->ver)): ?>

                    <div class="form-group">                    
                        <img class="img-perfil" id="img-perfil" style="display:none;" />                    
                    </div>   

                <?php else:?> 

                    <div class="form-group">                    
                        <label for="coo_ruta_foto">Foto:</label>
                        <input id="usu_ruta_imagen" name="coo_ruta_foto" type="file" accept="image/gif, image/jpeg, image/png" class="file" data-show-upload="false" data-show-caption="true">
                        <label for="coo_ruta_foto" id="extFoto" style="display:none;" class="label label-success">Posee una imagen</label>
                    </div>   

                <?php endif?>   

                <div class="form-group">
                    <label for="tdo_codigo">Tipo Documento:</label>                    
                    <select class="form-control input-sm" id="tdo_codigo" name="tdo_codigo" required>
                         
                         <?php                           

                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM tipo_documento");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['tdo_codigo']."'>".$fila['tdo_nombre']."</option>";
                            }           
                         ?>

                    </select>                     
                </div>                 
                
                <div class="form-group">
                    <label for="coo_identificacion">(*)Identificación:</label>
                    <input type="text" class="form-control input-sm required" id="coo_identificacion" name="coo_identificacion" placeholder="Introduce una Identificación">     
                    <label for="coo_identificacion" class="label label-danger error" generated="true"></label>               
                </div>    

                <div class="form-group">
                    <label for="coo_nombre">(*)Nombre:</label>
                    <input type="text" class="form-control input-sm required" id="coo_nombre" name="coo_nombre" placeholder="Introduce un Nombre" class="required">  
                    <label for="coo_nombre" class="label label-danger error" generated="true"></label>                   
                </div>                  

                <div class="form-group">
                    <label for="coo_apellido">(*)Apellido:</label>
                    <input type="text" class="form-control input-sm required" id="coo_apellido" name="coo_apellido" placeholder="Introduce un Apellido" class="required">  
                    <label for="coo_apellido" class="label label-danger error" generated="true"></label>                   
                </div>       

                <div class="form-group">
                    <label for="coo_tipo_coordinador">(*)Tipo de Coordinador:</label>
                    <select class="form-control input-sm" id="coo_tipo_coordinador" name="coo_tipo_coordinador" required>
                        <option value='RU'>Rural</option>
                        <option value='UR'>Urbano</option>
                        <option value='MU'>Municipal</option>                       
                    </select>  
                    <label for="coo_tipo_coordinador" class="label label-danger error" generated="true"></label>                   
                </div>                     

                <div class="form-group">
                    <label for="dep_codigo">Departamento:</label>                    
                    <select class="form-control input-sm opcion required" id="dep_codigo" name="dep_codigo">
                       
                         <?php
                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM departamentos ");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['dep_codigo']."'>".$fila['dep_nombre']."</option>";
                            }           
                         ?> 

                    </select>                     
                </div>                

                <div class="form-group">
                    <label for="mun_codigo">Municipios:</label>                    
                    <select class="form-control input-sm" id="mun_codigo" name="mun_codigo">                        
                    </select>                     
                </div>                                       
                
                <!--INICIO DIRECCION -->

                <div class="form-group">
                    <label class="col-lg-12 paddin-no-left" for="coo_tipo_direccion">Dirección:</label>                    
                    <div class="row">
                        <div class="col-lg-3">                            
                                <select class="form-control input-sm" id="coo_tipo_direccion" name="coo_tipo_direccion" required>
                                    <option value='CA'>Calle</option>
                                    <option value='CR'>Carrera</option>
                                    <option value='TR'>Transversal</option>
                                    <option value='DI'>Diagonal</option>
                                    <option value='AV'>Avenida</option>
                                    <option value='OT'>Otro</option>
                                </select>                             
                        </div>
                        <div class="col-lg-4 paddin-no-left">                                                                    
                            <input type="text" class="form-control input-sm" id="coo_direccion_1" name="coo_direccion_1" placeholder="Ejm: 40 Nor.">
                        </div>                                                
                        <label class="col-lg-1 paddin-no-left tamano20px" for="">#</label>
                        <div class="col-lg-2 paddin-no-left">                              
                            <input type="text" class="form-control input-sm" id="coo_direccion_2" name="coo_direccion_2" placeholder="Ejm: 73">
                        </div>
                        <label class="col-lg-1 paddin-no-left tamano20px" for="">-</label>
                        <div class="col-lg-2 paddin-no-left">
                            <input type="text" class="form-control input-sm" id="coo_direccion_3" name="coo_direccion_3" placeholder="Ejm: 78">
                        </div>
                    </div>
                    <label for="coo_direccion_1" class="label label-danger error" generated="true"></label>                     
                </div> 

                <!--FIN    DIRECCION --> 
                
                <div class="form-group">
                    <label for="coo_complemento_direccion">Complemento Dirección:</label>
                    <input type="text" class="form-control input-sm " id="coo_complemento_direccion" name="coo_complemento_direccion" placeholder="Introduce Complemento Dirección Ejm:(6a # 47 ­ 205)" >
                    <label for="coo_complemento_direccion" class="label label-danger error" generated="true"></label>                     
                </div>    

                <div class="form-group">
                    <label for="coo_barrio">Barrio:</label>
                    <input type="text" class="form-control input-sm " id="coo_barrio" name="coo_barrio" placeholder="Introduce un Barrio" >
                    <label for="coo_barrio" class="label label-danger error" generated="true"></label>                     
                </div>                  

                <div class="form-group">
                    <label for="coo_telefono_fijo">Teléfono/s Fijo/s:</label>
                    <input type="text" class="form-control input-sm" id="coo_telefono_fijo" name="coo_telefono_fijo" placeholder="Introduce Teléfono Fijo Ejm: 7302626, 7205693, ..." >
                    <label for="coo_telefono_fijo" class="label label-danger error" generated="true"></label>                     
                </div>    

                <div class="form-group">
                    <label for="coo_celular">Celular/es:</label>
                    <input type="text" class="form-control input-sm" id="coo_celular" name="coo_celular" placeholder="Introduce un Celular Ejm: 3017838383, 3174545454, ..." >
                    <label for="coo_celular" class="label label-danger error" generated="true"></label>                     
                </div> 

                <div class="form-group">
                    <label for="coo_email">Email:</label>
                    <input type="email" class="form-control input-sm " id="coo_email" name="coo_email" placeholder="Introduce un Email">
                    <label for="coo_email" class="label label-danger error" generated="true"></label>                     
                </div> 

                <div class="form-group">
                    <label for="coo_notas">Notas:</label>
                    <input type="text" class="form-control input-sm " id="coo_notas" name="coo_notas" placeholder="Introduce una Nota">
                    <label for="coo_notas" class="label label-danger error" generated="true"></label>                     
                </div>                
                
                <?php if(isset($strJSON->ver)): ?>

                    <div class="form-group">                    
                        <img class="img-perfil" id="img-cedula" style="display:none;" />                    
                    </div>   

                <?php else:?> 

                    <div class="form-group">
                        <label for="coo_ruta_cedula">Cedula:</label>
                        <input type="file" class="form-control input-sm file" id="coo_ruta_cedula" accept="image/gif, image/jpeg, image/png"  name="coo_ruta_cedula" data-show-upload="false" data-show-caption="true" >                                        
                        <label for="coo_ruta_cedula" id="extFotoCedula" style="display:none;" class="label label-success">Posee una imagen</label>
                    </div>   

                <?php endif?> 
                                         
            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>                            
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>