<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();    
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $conexion->beginTransaction();   

    $tabla = 'personas';
    $ideCampo = "per_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        $strCampo = fncPermiso("RVL", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("per_nombre",$_POST['per_nombre']);              
            $SQLcomando->set_AddCampo("per_apellido",$_POST['per_apellido']);              
            $SQLcomando->set_AddCampo("per_fecha_nacimiento",fncFechaNULL($_POST['per_fecha_nacimiento']), false);
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']);
            $SQLcomando->set_AddCampo("per_estatura",$_POST['per_estatura']);
            $SQLcomando->set_AddCampo("per_gsrh",$_POST['per_gsrh']);
            $SQLcomando->set_AddCampo("per_genero",$_POST['per_genero']);
            $SQLcomando->set_AddCampo("per_fecha_expedicion",fncFechaNULL($_POST['per_fecha_expedicion']), false);               
            $SQLcomando->set_AddCampo("per_estado_civil",$_POST['per_estado_civil']);
            $SQLcomando->set_AddCampo("per_tipo_direccion",$_POST['per_tipo_direccion']);
            $SQLcomando->set_AddCampo("per_direccion_1",$_POST['per_direccion_1']);
            $SQLcomando->set_AddCampo("per_direccion_2",$_POST['per_direccion_2']);
            $SQLcomando->set_AddCampo("per_direccion_3",$_POST['per_direccion_3']);
            $SQLcomando->set_AddCampo("per_complemento_direccion",$_POST['per_complemento_direccion']);
            $SQLcomando->set_AddCampo("per_barrio",$_POST['per_barrio']);             
            $SQLcomando->set_AddCampo("per_telefono_fijo",$_POST['per_telefono_fijo']);
            $SQLcomando->set_AddCampo("per_celular",$_POST['per_celular']);
            $SQLcomando->set_AddCampo("per_email",$_POST['per_email']);
            $SQLcomando->set_AddCampo("per_notas",$_POST['per_notas']);   
            
            $SQLcomando->set_AddCampo("per_puesto",$_POST['per_puesto']);    
            $SQLcomando->set_AddCampo("per_mesa_votacion",$_POST['per_mesa_votacion']);                 

            $SQLcomando->set_AddCampo("pai_codigo",$_POST['pai_codigo']);
            $SQLcomando->set_AddCampo("mun_codigo_res",$_POST['mun_codigo_res']);

            $SQLcomando->set_AddCampo("mun_codigo_exp",$_POST['mun_codigo_exp']);
            
            $SQLcomando->set_AddCampo("per_facebook",$_POST['per_facebook']);
            $SQLcomando->set_AddCampo("per_twiter",$_POST['per_twiter']);
            $SQLcomando->set_AddCampo("per_instagram",$_POST['per_instagram']);
            $SQLcomando->set_AddCampo("per_wsp",$_POST['per_wsp']);

            $SQLcomando->set_AddCampo("pro_codigo",$_POST['pro_codigo']);
            $SQLcomando->set_AddCampo("fun_codigo",$_POST['fun_codigo']);

            if($_POST['id'] != ""){                              

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                       

            }else{                                                                                                       

                /*$resp = $conexion->prepare("SELECT $ideCampo 
                                            FROM   $tabla 
                                            WHERE  per_identificacion = '".$_POST["per_identificacion"]."'");
                $resp->execute();
                $maxV = $resp->rowCount();
            
                if ($maxV > 0){ 
                    echo false;    
                    $conexion->rollBack();   
                    exit();                   
                }*/

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);

                $SQLcomando->set_AddCampo("tdo_codigo",$_POST['tdo_codigo']);
                $SQLcomando->set_AddCampo("per_identificacion",$_POST['per_identificacion']);                
                $SQLcomando->set_AddCampo("per_fecha_registro","CURRENT_DATE",false);

                $SQLcomando->set_AddCampo("per_pistola",$_POST['per_pistola']);      

                $SQLcomando->set_AddCampo("per_ruta_foto","");   
                $SQLcomando->set_AddCampo("per_ruta_cedula","");                                                           
                                                                                   
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                $SQLcomando->set_AddCampo("per_tipo",'VT');

                $SQLcomando->set_tipo_comando("INSERT");   

            }

            if($_FILES['per_ruta_foto']['name'] != ""){

                $foto = $_FILES['per_ruta_foto'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;

                $RutImg = "per_".$RutImg;
                
                $ruta = "../../images/Personas/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("per_ruta_foto",$RutImg);                

            }   

            if($_FILES['per_ruta_cedula']['name'] != ""){

                $foto = $_FILES['per_ruta_cedula'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;
                
                $RutImg = "ced_".$RutImg;                

                $ruta = "../../images/Personas/Cedulas/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("per_ruta_cedula",$RutImg);                

            }       

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            if($_POST['strNuevo'] == "true"){
                
                $SQLcomando->itemsClear();
                $SQLcomando->set_tabla('lider_personas');

                $lpeCodigo = MaxCampo("lider_personas","lpe_codigo",$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo("lpe_codigo",$lpeCodigo);

                $SQLcomando->set_AddCampo("per_codigo",$ideValor);
                $SQLcomando->set_AddCampo("lpe_fecha_registro","CURRENT_DATE",false);                                                                                   
                $SQLcomando->set_AddCampo("usu_codigo",$_SESSION['usu_codigo']);

                $SQLcomando->set_tipo_comando("INSERT");   

                $resp = $conexion->prepare($SQLcomando->get_sql());
                $resp->execute(); 
                
            }

            echo $ideValor;            
            break;   
            
        case 'D':        
        
            $strSQL = "SELECT per_codigo FROM personas WHERE  $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
            $respArray = $resp->fetchAll();

            $strPercodigo = $respArray[0][0];

            $strSQL = "DELETE FROM lider_personas WHERE per_codigo  ='$strPercodigo'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * , 
                             municipios.mun_codigo as mun_codigo_Per, municipios.dep_codigo as dep_codigo,
                             municipio_exp.dep_codigo as dep_codigo_exp,
                             municipio_res.dep_codigo as dep_codigo_res
                        FROM $tabla INNER JOIN 
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo INNER JOIN
                             municipios as municipio_exp ON $tabla.mun_codigo_exp = municipio_exp.mun_codigo INNER JOIN
                             municipios as municipio_res ON $tabla.mun_codigo_res = municipio_res.mun_codigo INNER JOIN
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;    

        case 'personaExiste':             

             $strSQL = "SELECT * 
                        FROM $tabla                              
                        WHERE per_identificacion = '$ideValor' AND per_tipo = 'VT'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();             

             $respestaJSON = new stdClass();

             $respestaJSON->existe = false;
             $respestaJSON->TieneLider = false;
             $respestaJSON->liderYo = false;

             if($resp->rowCount() > 0){
                $resM = $resp->fetchAll();
                $respestaJSON->datos = $resM[0];
                
                $respestaJSON->existe = true;

                $strSQL = "SELECT * 
                           FROM lider_personas 
                           WHERE per_codigo = '".$resM[0][0]."' ";
                
                $resLP = $conexion->prepare($strSQL);
                $resLP->execute();

                if($resLP->rowCount() > 0){
                    $respestaJSON->TieneLider = true;
                    $resMLP = $resLP->fetchAll();

                    session_start();
                    if($resMLP[0]['usu_codigo'] == $_SESSION['usu_codigo']) $respestaJSON->liderYo = true;                   
                }             
                
             }
          
             echo json_encode($respestaJSON);            
             break;    
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>