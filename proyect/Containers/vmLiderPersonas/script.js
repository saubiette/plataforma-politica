$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  var strNuevo = true
  var porPistola = 'NO'

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    if (!/^([0-9])*$/.test($('#per_identificacion').val())) {
      swal('Identificación no valida', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    // validar edad
    var edad = parseInt($('#edad').val() || 0)

    if (edad > 0 && edad < 18) {
      swal('Edad no valida', 'La persona debe ser mayor de edad ', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    var formData = new FormData(document.getElementById('idForm'))
    var opc = 'IU'
    formData.append('opc', opc)
    formData.append('strNuevo', strNuevo)
    formData.append('per_pistola', porPistola)

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        if (data == false) {
          swal('La Identificación ya existe', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }

        $('.dataTable').DataTable().ajax.reload()
        $('#idCerrar').click()

        // SE QUIERE CREAR UN DATO LABORAL
        /*swal({
            title: "Datos Laborales!",
            text: "Desea crear un Dato Laboral?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Crear Dato Laboral",
            cancelButtonText: "Salir",
            closeOnConfirm: true
        },
        function () {                    

            var varJson = new Object()
            varJson.cli_codigo = conCodigo

            crearVM_JSON("vmDatosLaborales", varJson)
            return false

        });  */

      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('.file').fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif']
  })

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    strNuevo = false
    llenarFormulario()
  }

  function llenarFormulario () {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data.per_ruta_foto != '') {
        $('#extFoto').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').prop('disabled', 'true')

        if (data.per_ruta_foto != '') {
          $('#img-perfil').css('display', 'block')
          $('#img-perfil').attr('src', './images/Personas/' + data.per_ruta_foto)
        }
      }

      var info = data.per_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#per_fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#tdo_codigo').val(data.tdo_codigo)
      $('#tdo_codigo').prop('disabled', true)

      $('#per_identificacion').val(data.per_identificacion)
      $('#per_identificacion').prop('disabled', true)

      $('#per_nombre').val(data.per_nombre)
      $('#per_apellido').val(data.per_apellido)
      $('#per_fecha_nacimiento').val(data.per_fecha_nacimiento)
      $('#per_fecha_nacimiento').focusout()

      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo_Per)

      $("#dep_codigo_exp").val(data.dep_codigo_exp);
      $("#dep_codigo_exp").change();
      $("#mun_codigo_exp").val(data.mun_codigo_exp);

      $("#dep_codigo_res").val(data.dep_codigo_res);
      $("#dep_codigo_res").change();
      $("#mun_codigo_res").val(data.mun_codigo_res);

      $("#pai_codigo").val(data.pai_codigo);

      $('#per_estatura').val(data.per_estatura)
      $('#per_gsrh').val(data.per_gsrh)

      $('#per_genero').val(data.per_genero)
      $('#per_fecha_expedicion').val(data.per_fecha_expedicion)
      $('#per_estado_civil').val(data.per_estado_civil)
      $('#per_tipo_direccion').val(data.per_tipo_direccion)

      $('#per_direccion_1').val(data.per_direccion_1)
      $('#per_direccion_2').val(data.per_direccion_2)
      $('#per_direccion_3').val(data.per_direccion_3)

      $('#per_complemento_direccion').val(data.per_complemento_direccion)
      $('#per_barrio').val(data.per_barrio)

      $('#per_telefono_fijo').val(data.per_telefono_fijo)
      $('#per_celular').val(data.per_celular)
      $('#per_email').val(data.per_email)
      $('#per_notas').val(data.per_notas)
      $("#fun_codigo").val(data.fun_codigo);

      $('#per_puesto').val(data.per_puesto)
      $('#per_mesa_votacion').val(data.per_mesa_votacion)

      $('#per_facebook').val(data.per_facebook)
      $('#per_twiter').val(data.per_twiter)
      $('#per_instagram').val(data.per_instagram)
      $('#per_wsp').val(data.per_wsp)

      $('#pro_codigo').val(data.pro_codigo)

      if (data.per_ruta_cedula != '') {
        $('#extFotoCedula').css('display', 'initial')
      }

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').attr('readonly', 'readonly')

        if (data.per_ruta_cedula != '') {
          $('#img-cedula').css('display', 'block')
          $('#img-cedula').attr('src', './images/Personas/Cedulas/' + data.per_ruta_cedula)
        }
      }

      $('#idModal').LoadingOverlay('hide')
    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()

  $('#dep_codigo_exp').on('change', function () {
    $('#mun_codigo_exp').val('')

    $.ajaxSetup({
      async: false
    })

    $.post(rutaCarpetaActual + '/municipios.php', {
      dep_codigo: $('#dep_codigo_exp').val()
    })
      .done(function (data) {
        $('#mun_codigo_exp').html(data)
      })

    $.ajaxSetup({
      async: true
    })
  })

  $('#dep_codigo_exp').change()

  $('#dep_codigo_res').on('change', function () {
    $('#mun_codigo_res').val('')

    $.ajaxSetup({
      async: false
    })

    $.post(rutaCarpetaActual + '/municipios.php', {
      dep_codigo: $('#dep_codigo_res').val()
    })
      .done(function (data) {
        $('#mun_codigo_res').html(data)
      })

    $.ajaxSetup({
      async: true
    })
  })

  $('#dep_codigo_res').change()

  $('#per_fecha_nacimiento').focusout(function () {
    $('#edad').val('0 Años')

    if ($('#per_fecha_nacimiento').val() == '') return false

    $.post('./generalesPHP/funcionesJS.php', { opc: 'fncEdad', fecha: $('#per_fecha_nacimiento').val()  })
      .done(function (data) {
        if (data > 0) $('#edad').val(data + ' Años')
      })
  })

  $('#per_identificacion').focusout(function () {
    if ($('#per_identificacion').val() == '') return false

    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'personaExiste', id: $('#per_identificacion').val() }, function (data) {
      console.log(data)
      console.log(data.TieneLider)

      if (data.TieneLider == true && data.liderYo == true) {
        strNuevo = false

        $('#per_identificacion').val('')
        // swal("Ya existe una persona con esta Identificación!", data.datos[5] + " " + data.datos[4] +" posee esta identificación, Consulta en la tabla para verificar información", "warning")
        swal({
          title: 'Ya existe una persona con esta Identificación! (' + data.datos[5] + ' ' + data.datos[4] + ')',
          text: 'Desea modificar los datos?',
          type: 'success',
          showCancelButton: true,
          confirmButtonText: 'Modificar',
          cancelButtonText: 'Salir',
          closeOnConfirm: true
        },
          function () {
            modVM('vmLiderPersonas', data.datos[0])
            return false
          })
      }
      else if (data.TieneLider == true && data.liderYo == false) {
        $('#per_identificacion').val('')
        swal('Ya existe un lider asignado a esta persona con esta Identificación!', data.datos[5] + ' ' + data.datos[4] + ' posee esta identificación y esta asignado a otro lider', 'warning')
      }
      else if (data.existe == true && data.TieneLider == false) {
        strNuevo = true
        $('#idModal').LoadingOverlay('show')
        $('#id').val(data.datos[0])
        llenarFormulario()
      // swal("Ya existe un lider asignado a esta persona con esta Identificación!", data.datos[5] + " " + data.datos[4] + " posee esta identificación, Consulta en la tabla para verificar información", "warning")
      }
    }, 'json')
  })

  var arrreglo = []
  var bandera = false

  $('#idModal').on('keydown', '#per_identificacion', function (e) {
    var keyCode = e.keyCode || e.which

    // alert(keyCode)
    porPistola = 'NO'

    if (keyCode == 9) {
      // alert(keyCode)   

      if (arrreglo.length > 0) {
        var valor = $('#per_identificacion').val().replace(arrreglo[0], '')
        console.log(arrreglo.length)
        $('#per_identificacion').val(valor)
      }

      arrreglo.push($('#per_identificacion').val())

      if (arrreglo.length > 0) $('#per_identificacion').val('')

      bandera = true

      e.preventDefault()
    // call custom function here
    }

    if (keyCode == 13) {
      console.log(arrreglo)
      console.log(arrreglo.length)
      console.log(bandera)

      if (bandera) $('#per_identificacion').val(arrreglo[0])
      $('#per_nombre').focus()
      bandera = false

      if (arrreglo.length > 1) {
        $('#per_nombre').val(arrreglo[3] + ' ' + arrreglo[4])
        $('#per_apellido').val(arrreglo[1] + ' ' + arrreglo[2])

        var formFecha = arrreglo[6].substr(4, arrreglo[6].length - 1) + '-' + arrreglo[6].substr(2, 2) + '-' + arrreglo[6].substr(0, 2)
        console.log(formFecha)

        $('#per_fecha_nacimiento').val(formFecha)
        $('#per_fecha_nacimiento').focusout()

        $('#per_genero').val(arrreglo[5])
        $('#per_gsrh').val(arrreglo[7])
        porPistola = 'SI'
      }

      arrreglo = []
      e.preventDefault()
    }
  })
})
