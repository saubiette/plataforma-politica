$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual

  var idT = '-1'

  var table = $('#dtGeneric').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'url': rutaContenedorActual + '/arrays.php',
      'dataSrc': '' // obligatorio  para que cargue
    },
    'columns': [
      { 'data': 'usu_codigo' },
      { 'data': 'usu_login' },
      { 'data': 'usu_nombre' },
      { 'data': 'usu_email' },
      { 'data': 'dep_nombre' },
      { 'data': 'mun_nombre' },
      { 'data': 'categoria' },
     
      { 'data': 'usu_ruta_imagen', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            
      {
        data: "add",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }

    ],
    buttons: [],
    lengthMenu: [],
    select: true
  })

  $('#dtGeneric tbody').on('click', 'tr:not(.selected)', function () {
    table2.clear()
    var data = table.row(this).data()
    idT = data['usu_codigo']
    table2.ajax.reload()
  })

  var table2 = $('#dtVotantes').DataTable({
    dom: 'Bfrtip',
    'ajax': {
      'type': 'POST',
      'url': rutaContenedorActual + '/arraysVotanes.php',
      'dataSrc': '', // obligatorio  para que cargue
      'data': function (d) {
        d.id = idT
      }
    },
    'columns': [
      { 'data': 'per_codigo', 'width': '2%' },
      { 'data': 'per_identificacion', 'width': '6%' },
      { 'data': 'per_nombre', 'width': '12%'  },
      { 'data': 'per_apellido', 'width': '12%'  },
      { 'data': 'per_fecha_nacimiento','width': '10%' },
      { 'data': 'per_telefono_fijo' },
      { 'data': 'per_celular' },
      { 'data': 'per_email' },
      { 'data': 'per_pistola','width': '2%' },

      { 'data': 'per_ruta_foto', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            
      { 'data': 'per_ruta_cedula', 'width': '1%', 'sClass': 'alignCenter pagCero', 'bSortable': false }, // , sClass: "alignRight" }            

    ],
    buttons: [],
    lengthMenu: [],
    select: false
  })

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
    $('#dtVotantes').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  $('#nuevo').on('click', function () {
    var varJson = { }
    varJson.TipUsuario = 'L'

    crearVM_JSON('vmUsuarios', varJson)
    return false
  });

  // ->NUEVO PERSONA
  $("#reporte").on("click", function() {
    var varJson = {};

    crearVM_JSON("vmReportesLider", varJson);
    return false;
  });
})

function Eliminar (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
  closeOnConfirm: true },
    function () {
      $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'D', id: id }, function (data) {
        if (data == false) {
          swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
        }else {
          $('.dataTable').DataTable().ajax.reload()
        }
      })
    })

  return false
}

function Modificar (id) {
  var varJson = { }
  varJson.TipUsuario = 'I'

  modVM_JSON('vmUsuarios', id, varJson)
  return false
}

function Activar (id) {
  swal({
    title: 'Información',
    text: '¿Desea activar este usuario?',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Activar',
  closeOnConfirm: true },
    function () {
      $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'ACT', id: id }, function (data) {
        $('.dataTable').DataTable().ajax.reload()
      })
    })

  return false
}

function Desactivar (id) {
  swal({
    title: 'Información',
    text: '¿Desea desactivar este usuario?',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Desactivar',
  closeOnConfirm: true },
    function () {
      $.post(rutaContenedorActual + '/PHPCrud.php', { opc: 'DES', id: id }, function (data) {
        $('.dataTable').DataTable().ajax.reload()
      })
    })

  return false
}

function Ver (id) {
  var varJson = { }
  varJson.TipUsuario = 'L'
  varJson.ver = true

  modVM_JSON('vmUsuarios', id, varJson)
  return false
}

function VerVotante (id) {
  var varJson = { }
  varJson.ver = true

  modVM_JSON('vmPersonas', id, varJson)
  return false
}

function Add(id) {
  var varJson = {};
  //varJson.TipUsuario = "S";
  varJson.tp = "LIV";
  varJson.usu_codigo = id;

  crearVM_JSON("vmPersonas", varJson);
  return false;
}
