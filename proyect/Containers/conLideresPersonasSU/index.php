    <?php
    session_start();

        require "../../../php/conexion.php";
        require "../../generalesPHP/funciones.php";
        
        $strCampo = fncPermiso("LIV","R", new clsConexion()); 
        
        if(!$strCampo){
            echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
            exit();
        }               
    ?>

    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Lideres - Votantes</li>            
        </ol>  
        
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead>              
                <tr>                    
                     <td><input type="button" class="btn btn-primary" id="reporte" value="Reporte"></td>
                    <td><input class="btn btn-success" type="button" id="nuevo" value="+ Nuevo Lider"></td>                         						
                </tr> 
                <tr>
                    <th>Codigo</th>
                    <th>Login</th>
                    <th>Nombre</th>                    
                    <th>Email</th> 
                    <th>Departamento</th> 
                    <th>Municipio</th>  
                    <th>Categoría</th>                       
                    <th></th>		
                    <th></th>					
                    <th></th>					
                    
                </tr>
            </thead>					
        </table>   

         <table id="dtVotantes" class="display" cellspacing="0" width="100%">
            <thead>               
                <tr>
                    <th>Codígo</th>
                    <th>Identificación</th>
                    <th>Nombres</th>                                               
                    <th>Apellido</th>                                               
                    <th>Fecha de Nacimiento</th>    
                    <th>Telefono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Escaner</th>

                    <th>Foto</th>
                    <th>Cedula</th>							                    
                    
                </tr>
            </thead>					
        </table>      
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.min.js"></script>
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/tooltip.js"></script>