<?php 
session_start();
    require '../../../php/conexion.php';     
    require "../../generalesPHP/funciones.php";
    
    if(isset($_POST['id'])):
        //$strCampo = fncPermiso("CAL","U", new clsConexion()); 
        $strCampo = "-";
    else:
         $strCampo = fncPermiso("CAL","C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }  
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar Agenda</b></h2> 
                    <?php else:?>
                        <h2><b>Registrar Agenda</b></h2>
                    <?php endif;?>   
                
                <?php else:?>  

                    <h2><b>Agenda</b></h2>
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                               
                
                <div class="form-group">
                    <label for="cal_fecha">(*)Fecha:</label>
                    <input type="date" class="form-control input-sm required" id="cal_fecha" name="cal_fecha" placeholder="Introduce una fecha">     
                    <label for="cal_fecha" class="label label-danger error" generated="true"></label>               
                </div>                    

                <div class="form-group">
                    <label class="col-lg-6 paddin-no-left" for="cal_hora_inicio">(*)Hora Inicial (24hr):</label>
                    <label class="col-lg-6 paddin-no-left" for="cal_hora_fin">(*)Hora Final (24hr):</label>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="time" class="form-control input-sm required" id="cal_hora_inicio" name="cal_hora_inicio" placeholder="Introduce un Tiempo Inicial">
                            <label for="cal_hora_inicio" class="label label-danger error" generated="true"></label>                     
                        </div>
                        <div class="col-lg-6 paddin-no-left">
                            <input type="time" class="form-control input-sm required"  id="cal_hora_fin" name="cal_hora_fin" placeholder="Introduce un Tiempo Final">
                            <label for="cal_hora_fin" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div>                   
                    
                </div> 
                
                <div class="form-group">
                    <label for="cal_titulo">(*)Titulo:</label>
                    <input type="text" step="any" class="form-control input-sm required" id="cal_titulo" name="cal_titulo" placeholder="Introduce una Dirección">  
                    <label for="cal_titulo" class="label label-danger error" generated="true"></label>                   
                </div>
                
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>            
                <?php else: ?>
                    <button id='idEliminar' class="btn btn-danger load">Eliminar</button>            
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>