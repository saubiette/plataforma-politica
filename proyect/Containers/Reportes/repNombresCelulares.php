<?php
set_time_limit(220);
ini_set('memory_limit', '556M');
//ob_start();
require_once '../../generalesPHP/PHPExcel/PHPExcel.php'; /** Incluye PHPExcel */	
$objPHPExcel = new PHPExcel(); // Crear nuevo objeto PHPExcel
include "../../../php/conexion.php";
include '../../generalesPHP/PHPExcel/configReportes.php'; /** Configurarion de PHPExcel */	

$conexion = new clsConexion();

$strSQL = "SELECT per_nombre, per_celular 
           FROM personas 
           WHERE per_celular != '' AND per_celular is not NULL           
           GROUP BY  per_nombre, per_celular 
           ORDER BY per_nombre";

$res = $conexion->prepare($strSQL);
$res->execute();

$vector = $res->fetchAll(PDO::FETCH_ASSOC);

$objPHPExcel->getActiveSheet()->setTitle('Nombres y Celulares');

//Encavezados
$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'NOMBRE')
                ->setCellValue('B1', 'CELULAR');

//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);

$numFila = 2;

foreach ($vector as $key => $value) {
    $sep = explode(" ", trim($value['per_nombre'])); 

    $objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A".$numFila, $sep[0] )
		->setCellValue("B".$numFila, $value['per_celular']);
			
		$numFila+=1;
}

// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);

//crea el documento en formato office 2007 en adelandte
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Nombre y Celulares.xlsx"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');
// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');	
ob_end_clean();
$objWriter->save('php://output');
exit;
?>