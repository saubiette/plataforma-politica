<?php
 session_start();
 require "../../../php/conexion.php";
 require "../../generalesPHP/SQLComando.php";
 require "../../generalesPHP/funciones.php";

 $tipoUsuario = $_SESSION['usu_nivel_acceso'];  
 $usuCodigo = $_SESSION['usu_codigo']; 
$_sql = "SELECT per_celular FROM personas p 
            inner join municipios mm on mm.mun_codigo = p.mun_codigo
            inner join departamentos d on d.dep_codigo = mm.dep_codigo";
//departamento
if($_POST["idEnvio"]==1) {
    $per_id="";
    $i=0;
    if($_POST["personas"]!=null){
        foreach($_POST["personas"] as $personas){
            if($i>0)
                $per_id=$per_id.",";
            $per_id = $per_id .$personas;       
            $i=$i+1;    
        }
        $_sql = $_sql." where p.per_codigo in (".$per_id.")";
        enviarMensaje($_sql);
        guardar();
    }else{
        echo "Seleccione persona";
    }
}else if($_POST["idEnvio"] == 2){
    $ids="";
    $i=0;
    if($_POST["lideres"]!=null){
        foreach($_POST["lideres"] as $lideres){
            if($i>0)
                $ids=$ids.",";
            $ids = $ids .$lideres;       
            $i=$i+1;    
        }
        $sql  = "select p.per_celular
                from lider_personas lp 
                inner join personas p on p.per_codigo = lp.per_codigo
                where usu_codigo in( ".$ids.")
                and p.per_celular is not null and  LENGTH(p.per_celular)>0";
                echo $sql;
        enviarMensaje($sql);
            
        guardar();
    }else{
        echo "Seleccione lider";
    }
}else if($_POST["idEnvio"]==3) {
    $dep_id="";
    $mun_id="";
    $barrios_id="";
    $i=0;
    
    if($_POST["departamentos"]!=null){
        foreach($_POST["departamentos"] as $departamentos){
            if($i>0)
                $dep_id=$dep_id.",";
            $dep_id = $dep_id .$departamentos;       
            $i=$i+1;    
        }
        $sql = " where d.dep_codigo in (".$dep_id.")";

        if($_POST["municipios"]!=null){
            $i=0;
            foreach($_POST["municipios"] as $municipios){
                if($i>0)
                    $mun_id=$mun_id.",";
                $mun_id = $mun_id .$municipios;       
                $i=$i+1;    
            }
            $sql = " where mm.mun_codigo in (".$mun_id.")";
        }
        if($_POST["barrios"]!=null){
            $i=0;
            foreach($_POST["barrios"] as $barrios){
                if($i>0)
                    $barrios_id=$barrios_id.",";
                $barrios_id = $barrios_id."'".$barrios."'";       
                $i=$i+1;    
            }

            $sql = " where mm.mun_codigo in (".$mun_id.") and 
            lower(case when REPLACE(per_barrio,'.', '') is null or REPLACE(per_barrio,'.', '')='' then 'sin barrio' else REPLACE(REPLACE(per_barrio,'.', ''),'2','') end) in (".$barrios_id.")";
        }
        $_sql = $_sql. $sql. " and p.per_celular is not null and  LENGTH(p.per_celular)>0";
        
        enviarMensaje($_sql);
            
        guardar();
        }//for if
        else{
        echo "Seleccione departamento";
        }   
    
}

function enviarMensaje($sql){
    $con = new clsConexion();
    $query = $con->prepare($sql);
        $query->execute();
         $array = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($array as $key => $value) {
            $data = [
                'phone' => '57'.$array[$key]['per_celular'], // Receivers phone
                'body' => $_POST["mensaje"], // Message
            ];
            $json = json_encode($data);
            $url = 'https://eu13.chat-api.com/instance51144/message?token=ccl159z7cy1btp8j';
            $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json
            ]
            ]);
            $result = file_get_contents($url, false, $options);
}
}

function guardar(){
    try {
        $conexion = new clsConexion();        
        $conexion->beginTransaction(); 
        $SQLcomando = new SQLComando();
        $SQLcomando->set_tabla("mensajes");
        $SQLcomando->set_AddCampo("mensaje",$_POST['mensaje']);              
        $SQLcomando->set_tipo_comando("INSERT");            
        $resp = $conexion->prepare($SQLcomando->get_sql());
        $resp->execute();   
        $conexion->commit();   
    }catch (Exception $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        $conexion->rollBack();
    } 
    catch (PDOException  $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        $conexion->rollBack();
    }
}

?>