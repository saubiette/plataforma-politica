<?php
    session_start();
    require "../../../php/conexion.php";
?>
<div class="container-fluid containers">

    <ol class="breadcrumb">
        <li>
            <i class="fa fa-fw fa-envelope-o"></i> Navegación
        </li>
        <li class="active">Envio de mensajes</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary">
                <i class="fa fa-fw fa-plus"></i>Mensajes predeterminados
            </button>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="txt-mensaje">Mensaje</label>
                <textarea class="form-control" id="txt-mensaje" rows="3" placeholder="Digite mensaje"></textarea>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Modo de envio</label>
                <select class="form-control" id="modo-envio">
                    <option value="">Seleccione opción</option>
                    <option value="1">Personas</option>
                    <option value="2">Líderes</option>
                    <option value="3">Departamento</option>
                </select>
            </div>
        </div>

        <div class="col-md-3" id="div-lider">
            <div class="form-group">
                <label>Líderes</label>
                <select class="form-control" id="codigo_lider">
                    <?php
                            $con = new clsConexion();
                            $res = $con->prepare("
                            select u.usu_codigo, u.usu_nombre
                                from lider_personas lp 
                                inner join usuario  u on u.usu_codigo = lp.usu_codigo
                                group by u.usu_codigo, u.usu_nombre
                                order by 2 ");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['usu_codigo']."'>".$fila['usu_nombre']."</option>";
                            }           
                         ?>
                </select>
            </div>

        </div>
        <div class="col-md-3" id="div-departamentos">
            <div class="form-group">
                <label>Departamento</label>
                <select class="form-control" id="dep_codigo_exp" name="dep_codigo_exp">
                    <?php
                    $con = new clsConexion();
                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select >".$fila['dep_nombre']."</option>";
                                    }           
                                ?>

                </select>
            </div>
        </div>
        <div class="col-md-3" id="div-municipio">
            <div class="form-group">
                <label>Municipio</label>
                <select class="form-control " id="mun_codigo_exp" name="mun_codigo_exp">
                </select>
            </div>
        </div>
        <div class="col-md-3" id="div-persona">
            <div class="form-group">
                <label>Número de celular</label>
                <input type="number" class="form-control" id="numero_celular" name="numero_celular" />
                
            </div>
        </div>
    </div>


    <div class="row" id="div-enviar">
        <div class="col-md-2">
            <div class="form-group">
                <button class="btn btn-success" id="btn-enviar">Enviar</button>
            </div>
        </div>
    </div>

</div>



<script src="./Containers/conEnvioMensajes/enviomensaje.js"></script>