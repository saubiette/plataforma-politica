<?php
    session_start();
    require "../../../php/conexion.php";
?>
<div class="container-fluid containers">

    <ol class="breadcrumb">
        <li>
            <i class="fa fa-fw fa-envelope-o"></i> Navegación
        </li>
        <li class="active">Envio de mensajes</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalmensajes">
                <i class="fa fa-fw fa-plus"></i>Mensajes enviados
            </button>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="txt-mensaje">Mensaje</label>
                <textarea class="form-control" id="txt-mensaje" rows="3" placeholder="Digite mensaje"></textarea>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Modo de envio</label>
                <select class="form-control" id="modo-envio">
                    <option value="">Seleccione opción</option>
                    <option value="1">Personas</option>
                    <option value="2">Líderes</option>
                    <option value="3">Departamento</option>
                </select>
            </div>
        </div>

        <div class="col-md-3" id="div-lider">
            <div class="form-group">
                <label>Líderes</label>
                <select class="form-control selectpicker" id="codigo_lider" name="codigo_lider" multiple data-size="5"
                    data-live-search="true" title="seleccione">
                    <?php
                            $con = new clsConexion();
                            $res = $con->prepare("
                            select u.usu_codigo, u.usu_nombre
                                from lider_personas lp 
                                inner join usuario  u on u.usu_codigo = lp.usu_codigo
                                group by u.usu_codigo, u.usu_nombre
                                order by 2 ");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['usu_codigo']."'>".$fila['usu_nombre']."</option>";
                            }           
                         ?>
                </select>
            </div>

        </div>
        <div class="col-md-3" id="div-departamentos">
            <div class="form-group">
                <label>Departamento</label>
                <select class="form-control selectpicker" id="dep_codigo" name="dep_codigo" multiple data-size="5"
                    data-live-search="true" title="seleccione">
                    <?php
                    $con = new clsConexion();
                    $res = $con->prepare("SELECT * FROM departamentos ");
                                    $res->execute();
                                    
                                    while($fila = $res->fetch()){
                                        $select = "";//$fila['dep_codigo'] == '52')?"selected":"";
                                        echo "<option value='".$fila['dep_codigo']."' $select >".$fila['dep_nombre']."</option>";
                                    }           
                                ?>

                </select>
            </div>
        </div>
        <div class="col-md-3" id="div-municipio">
            <div class="form-group">
                <label>Municipio</label>
                <select class="form-control selectpicker" id="mun_codigo" name="mun_codigo" multiple data-size="5"
                    data-live-search="true" title="seleccione">

                </select>
            </div>
        </div>
        <div class="col-md-3" id="div-barrio">
            <div class="form-group">
                <label>Barrio</label>
                <select class="form-control selectpicker" id="mun_barrio" name="mun_barrio" multiple data-size="5"
                    data-live-search="true" title="seleccione">

                </select>
            </div>
        </div>
        <div class="col-md-6" id="div-persona">
            <div class="form-group">
                <label>Personas</label>
                <select class="form-control selectpicker" id="mun_persona" name="mun_persona" multiple data-size="5"
                    data-live-search="true" title="seleccione">

                </select>

            </div>
        </div>
    </div>


    <div class="row" id="div-enviar">
        <div class="col-md-2">
            <div class="form-group">
                <button class="btn btn-success" id="btn-enviar">Enviar</button>
            </div>
        </div>
    </div>

</div>


<div class="modal" id="modalmensajes" tabindex="-1" role="dialog" aria-labelledby="modalmensajesLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modalmensajesLabel">Últimos mensajes enviados</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" id="tabla">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Mensaje</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM mensajes order by 1 desc limit 5 ");
                            $res->execute();
                              while($fila = $res->fetch()){
                               echo "<tr><td>".$fila["mensaje"]."</td></tr>";
                             }           
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<script src="./Containers/conEnvioMensajes/enviomensaje.js"></script>