<?php
    require '../../../php/conexion.php'; 
    session_start();
    $con = new clsConexion();
    $tipoUsuario = $_SESSION['usu_nivel_acceso'];  
    $usuCodigo = $_SESSION['usu_codigo'];  
    $sql = "SELECT p.* 
            from lider_personas lp 
            inner join personas p on p.per_codigo = lp.per_codigo
            WHERE  p.per_celular is not null and  LENGTH(p.per_celular)>0 ";
    if($tipoUsuario =='L')
        $sql = $sql." and lp.usu_codigo =".$usuCodigo;
        
    $res = $con->prepare($sql);

    $res->execute();
    
    while($fila = $res->fetch()){
        echo "<option value='".$fila['per_codigo']."'>".$fila['per_identificacion']." ".$fila['per_nombre']." ".$fila['per_apellido']."</option>";
    }           
?>