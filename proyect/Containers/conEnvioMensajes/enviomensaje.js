$(document).ready(function () {
    var idEnvio = 0;
    rutaCarpetaActual = './Containers/conEnvioMensajes';
    $('#dep_codigo').selectpicker();
    $('#mun_codigo').selectpicker();
    $('#mun_barrio').selectpicker();
    $('#codigo_lider').selectpicker();
    $('#div-lider').hide();
    $('#div-enviar').hide();
    $('#div-departamentos').hide();
    $('#div-municipio').hide();
    $('#div-barrio').hide();
    $("#div-persona").hide();
    loadMunicipios();
    $('#modo-envio').on('change', function () {
        $('#div-lider').hide();
        $('#div-enviar').hide();
        $('#div-departamentos').hide();
        $('#div-municipio').hide();
        $('#div-barrio').hide();
        $("#div-persona").hide();
        idEnvio = $('#modo-envio').val();
        if (idEnvio == 1) {
            loadPersonas();
            $("#div-persona").show();
        } else if (idEnvio == 2) {
            $("#div-lider").show();

        } else if (idEnvio == 3) {
            $("#div-departamentos").show();
            $('#div-municipio').show();
            $('#div-barrio').show();
        }
        if (idEnvio != 0) {
            $('#div-enviar').show();
        }
    });


    //envio
    $("#btn-enviar").click(function () {

        if ($("#txt-mensaje").val() == null || $("#txt-mensaje").val() == "") {
            swal("Digite mensaje!");
        } else {
            let departamentosSeleccionado = $('#dep_codigo').val();
            let municipiosSeleccionado = $('#mun_codigo').val();
            let barriosSeleccionado = $('#mun_barrio').val();
            let personasSeleccionada= $('#mun_persona').val();
            let lideresSeleccionada= $('#codigo_lider').val();
            $.LoadingOverlay("show");
            $.post(rutaCarpetaActual + '/enviomensaje.php', {
                idEnvio: idEnvio,
                departamentos: departamentosSeleccionado,
                municipios: municipiosSeleccionado,
                barrios: barriosSeleccionado,
                mensaje: $("#txt-mensaje").val(),
                personas:personasSeleccionada,
                lideres:lideresSeleccionada
            }).done(function (data) {
                if(data.includes("Seleccione")){
                    swal(data);
                }else{
                    swal("Enviado correctamente");
                }
                $.LoadingOverlay("hide");
                /*if (data != "Enviado correctamente")
                    swal("Error al enviar api no válida");
                else*/
                
            }).fail(function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
                $.LoadingOverlay("hide");
            });


            /*$.ajax({
                url: rutaCarpetaActual + '/enviomensaje.php?idEnvio=' + idEnvio + "&codigo_lider=" + $('#codigo_lider').val() + "&mensaje=" + $("#txt-mensaje").val() + "&municipio=" + $("#mun_codigo").val() + "&celular=" + $("#numero_celular").val(),
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                $.LoadingOverlay("hide");
                swal("Enviado correctamente!");
            })
                .fail(function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                    $.LoadingOverlay("hide");
                })*/
        }
    });


    $('#dep_codigo').on('change', function () {
        loadMunicipios();
    });

    $('#mun_codigo').on('change', function () {
        loadBarrios();
    });

    function loadMunicipios() {
        let departamentoSeleccionado = $('#dep_codigo').val();
        $('#mun_codigo').val('');
        $('#mun_codigo').html('');
        if (departamentoSeleccionado != null) {
            if (departamentoSeleccionado.length == 1) {
                $.post(rutaCarpetaActual + '/municipios.php', {
                    dep_codigo: departamentoSeleccionado[0]
                }).done(function (data) {
                    $('#mun_codigo').html(data);
                    $("#mun_codigo").selectpicker('refresh');
                });
            }
        }

        $("#mun_codigo").selectpicker('refresh');
    }

    function loadBarrios() {
        let municipiosSeleccionado = $('#mun_codigo').val();
        $('#mun_barrio').val('');
        $('#mun_barrio').html('');
        if (municipiosSeleccionado != null) {
            if (municipiosSeleccionado.length == 1) {
                $.post(rutaCarpetaActual + '/barrios.php', {
                    municipio: municipiosSeleccionado[0]
                }).done(function (data) {
                    $('#mun_barrio').html(data);
                    $("#mun_barrio").selectpicker('refresh');
                });
            }
        }

        $("#mun_barrio").selectpicker('refresh');
    }

    function loadPersonas() {
        let personas = $("mun_persona").val();
        if (personas == null) {
            $.post(rutaCarpetaActual + '/personas.php', {
                
            }).done(function (data) {
                $('#mun_persona').html(data);
                $("#mun_persona").selectpicker('refresh');
            });
        }


    }
    $("#tabla").find('tbody').on( 'click', 'td', function (data) {	
        
        $(this).closest('tr').find('td').each(function() {
            var textval = $(this).text(); // this will be the text of each <td>
            $("#txt-mensaje").val(textval);
            $('#close').trigger('click');
       });
    });

});