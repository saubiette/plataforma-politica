<?php
require '../../../php/conexion.php';
$con = new clsConexion();

$pru = $con->prepare("SELECT * 
                      FROM reuniones INNER JOIN 
                           municipios ON reuniones.mun_codigo = municipios.mun_codigo INNER JOIN
                           departamentos ON municipios.dep_codigo = departamentos.dep_codigo"); 
                    
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {       

    $array[$key]['reu_titulo'] = "<a href=javascript:Ver('". $value['reu_codigo']."') alt='Ver' title='Ver'><b>".$value['reu_titulo']."</b></a>"; 
    
    $array[$key]['add'] = "<a href=javascript:Add('". $value['reu_codigo']."')><img src=./images/add_user_2.png   alt='Añadir Usuario' title='Añadir Usuario' border=0></a>";
    $array[$key]['mod'] = "<a href=javascript:Modificar('". $value['reu_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
    $array[$key]['eli'] = "<a href=javascript:Eliminar('". $value['reu_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
}

echo  json_encode($array);

?>