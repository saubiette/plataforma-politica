<?php
session_start();
require '../../../php/conexion.php';
$con = new clsConexion();

$id = $_POST['id'];

$pru = $con->prepare("SELECT *,
                            depVive.dep_nombre as dep_nombre_vive, munVive.mun_nombre as mun_nombre_vive,
                            depRes.dep_nombre as dep_nombre_res, munRes.mun_nombre as mun_nombre_res 
                      FROM  personas INNER JOIN
                            reuniones_personas ON personas.per_codigo = reuniones_personas.per_codigo INNER JOIN
                            municipios AS munVive ON personas.mun_codigo = munVive.mun_codigo INNER JOIN
                            departamentos AS depVive ON munVive.dep_codigo = depVive.dep_codigo INNER JOIN
                            municipios AS munRes ON personas.mun_codigo_res = munRes.mun_codigo INNER JOIN
                            departamentos AS depRes ON munRes.dep_codigo = depRes.dep_codigo INNER JOIN
                            funciones ON personas.fun_codigo = funciones.fun_codigo
                      WHERE reu_codigo = '$id' "); 
$pru->execute();

$array = $pru->fetchAll(PDO::FETCH_ASSOC);

$fechaSegundos = time(); 
$strNoCache = "?nocache=$fechaSegundos"; 	

foreach ($array as $key => $value) {   

    $array[$key]['nota'] = "<a href=javascript:AgregarNotas('". $value['per_codigo']."')><img src=./images/notas.png   alt='Agregar Nota' title='Agregar Nota' border=0></a>";

    $array[$key]['per_identificacion'] = "<a href=javascript:VerVotante('". $value['per_codigo']."') alt='Ver' title='Ver'><b>".$value['per_identificacion']."</b></a>"; 

    if($array[$key]['per_ruta_foto'] != "" ){
        $array[$key]['per_ruta_foto'] = "<a data-lightbox='gal_".$array[$key]['per_codigo']."' href='./images/Personas/".$array[$key]['per_ruta_foto'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Personas/".$array[$key]['per_ruta_foto'].$strNoCache."'></a>";
    }

    if($array[$key]['per_ruta_cedula'] != "" ){
        $array[$key]['per_ruta_cedula'] = "<a data-lightbox='gal_".$array[$key]['per_codigo']."' href='./images/Personas/Cedulas/".$array[$key]['per_ruta_cedula'].$strNoCache."' data-title='Foto'><img height=20 src='./images/Personas/Cedulas/".$array[$key]['per_ruta_cedula'].$strNoCache."'></a>";
    }            

    if($_SESSION['usu_nivel_acceso'] == "S"){
        $array[$key]['eli'] = "<a href=javascript:Eliminar('". $value['per_codigo']."')><img src=./images/delete.png   alt='Eliminar' title='Eliminar' border=0></a>";      
        $array[$key]['mod'] = "<a href=javascript:Modificar('". $value['per_codigo']."')><img src=./images/edit.png   alt='Modificar' title='Modificar' border=0></a>";
    }
    else{
        $array[$key]['eli'] = "-";
        $array[$key]['mod'] = "-";
    }

}

echo  json_encode($array);

?>