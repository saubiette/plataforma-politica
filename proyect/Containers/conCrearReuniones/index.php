    <?php
    session_start();
        require "../../../php/conexion.php";
        require "../../generalesPHP/funciones.php";
        
        $strCampo = fncPermiso("REU","R", new clsConexion()); 
        
        if(!$strCampo){
            echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
            exit();
        }           
    ?>

    <div class="container-fluid containers">
              
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
            </li>
            <li class="active">Reuniones</li>            
        </ol>  
        
        <table id="dtGeneric" class="display" cellspacing="0" width="100%">
            <thead> 
                 <tr>
                    <td colspan='5' >
                        <input type="button"  id="nuevaReunion" class="btn btn-success" value="Nueva reunión">                        
                    </td>
                </tr>              
                <tr>
                    <th>Codigo</th>
                    <th>Titulo</th>
                    <th>Descripcion</th>                    
                    <th>Fecha</th> 
                    <th>Hora</th> 
                    <th>Departamento</th> 
                    <th>Municipio</th>  
                    
                     <th></th>	
                    <th></th>							
                    
                </tr>
            </thead>					
        </table>   

         <table id="dtVotantes" class="display table" cellspacing="0" width="100%">
            <thead class="thead-dark">               
                <tr>
                    <th>Codígo</th>
                    <th>Identificación</th>
                    <th>Nombres</th>                                               
                    <th>Apellido</th>                                                                   
                    <th>Celular</th>
                    
                    <th>Departamento Pro.</th>                    
                    <th>Municipio Pro.</th>

                    <th>Departamento Res.</th>                    
                    <th>Municipio Res.</th>  
                    <th>Función</th>  

                    <th>Foto</th>
                    <th>Cedula</th>		
                    <th></th>					                    
                    
                </tr>
            </thead>					
        </table>      
        
    </div>   	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script>var rutaContenedorActual = "<?php echo $vec[count($vec) - 1]; ?>";</script>
    <script>var r = "<?php echo $_SESSION['usu_nivel_acceso']; ?>";</script>
    
    <script src="./Containers/<?php echo  $vec[count($vec) - 1]; ?>/DataTable.min.js"></script>