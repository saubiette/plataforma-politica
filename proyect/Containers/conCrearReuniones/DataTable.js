$(document).ready(function () {
  rutaContenedorActual = './Containers/' + rutaContenedorActual
  var exp = r == "S" ? true : false;

  var idT = '-1'

  var table = $("#dtGeneric").DataTable({
    dom: "Bfrtip",
    ajax: {
      url: rutaContenedorActual + "/arrays.php",
      dataSrc: "" // obligatorio  para que cargue
    },
    columns: [
      { data: "reu_codigo" },
      { data: "reu_titulo" },
      { data: "reu_descripcion" },
      { data: "reu_fecha" },
      { data: "reu_hora" },
      { data: "dep_nombre" },
      { data: "mun_nombre" },
      {
        data: "add",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } ,
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } 
    ],
    buttons: [],
    lengthMenu: [],
    select: true
  });

  $('#dtGeneric tbody').on('click', 'tr:not(.selected)', function () {
    table2.clear()
    var data = table.row(this).data()
    idT = data["reu_codigo"];
    table2.ajax.reload()
  })

  var table2 = $("#dtVotantes").DataTable({
    dom: "Bfrtip",
    ajax: {
      type: "POST",
      url: rutaContenedorActual + "/arraysVotanes.php",
      dataSrc: "", // obligatorio  para que cargue
      data: function(d) {
        d.id = idT;
      }
    },
    columns: [
      { data: "per_codigo", width: "2%" },
      { data: "per_identificacion", width: "6%" },
      { data: "per_nombre", width: "12%" },
      { data: "per_apellido", width: "12%" },
      { data: "per_celular" },

      { data: "dep_nombre_vive", width: "12%" },
      { data: "mun_nombre_vive", width: "12%" },
      { data: "dep_nombre_res", width: "12%" },
      { data: "mun_nombre_res", width: "12%" },
      { data: "fun_nombre", width: "8%" },

      {
        data: "per_ruta_foto",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "per_ruta_cedula",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "nota",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }
    ],
    buttons: [
      {
        extend: "collection",
        text: "Export",
        buttons: ["copy", "excel", "csv", "pdf", "print"],
        enabled: exp
      }
    ],
    columnDefs: [
      {
        targets: [0],
        visible: false,
        searchable: false
      }
    ],
    lengthMenu: [],
    select: false
  });

  $(document).ready(function () {
    $('#dtGeneric').removeClass('display').addClass('table table-striped table-hover table-bordered')
    $('#dtVotantes').removeClass('display').addClass('table table-striped table-hover table-bordered')
  })

  $("#nuevaReunion").on("click", function() {
    var varJson = {};
    varJson.TipUsuario = "L";

    crearVM_JSON("vmReuniones", varJson);
    return false;
  });
})

function Eliminar (id) {
  swal({
    title: '¿Seguro que deseas eliminar este Registro?',
    text: 'No podrás deshacer este paso...',
    type: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Eliminar',
  closeOnConfirm: true },
    function () {
      $.post("./Containers/vmReuniones/PHPCrud.php", { opc: "D", id: id}, function(data) {
          if (data == false) {
            swal("No se puede eliminar", "Este registro esta enlazado con otros...", "warning");
          } else if (data == "DN") {
            swal("Necesita permisos para realizar esta accion.", "Contacte al administrador del sistema", "warning");
          } else {
            $(".dataTable")
              .DataTable()
              .ajax.reload();
          }
        }, "json");
    })

  return false
}

//TODO: falta modificar reunion
function Modificar (id) {
  var varJson = { }
  varJson.TipUsuario = 'I'

  modVM_JSON('vmUsuarios', id, varJson)
  return false
}


function Ver (id) {
  var varJson = { }
  varJson.ver = true

  modVM_JSON('vmReuniones', id, varJson)
  return false
}

function Add(id) {
  var varJson = {};
  //varJson.TipUsuario = "S";
  varJson.tp = "REU";
  varJson.id_reunion = id;

  crearVM_JSON("vmPersonas", varJson);
  return false;  
}

function VerVotante (id) {
  var varJson = { }
  varJson.ver = true

  modVM_JSON('vmPersonas', id, varJson)
  return false
}

function AgregarNotas(id) {
  var varJson = {};
  varJson.tp = "REU";
  varJson.perCodigo = id;

  crearVM_JSON("vmAnotaciones", varJson);
  return false;
}
