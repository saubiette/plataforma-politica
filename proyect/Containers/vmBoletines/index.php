<?php require '../../../php/conexion.php'; ?>

<div id="idModal" class="modal fade">

    <div class="modal-dialog">

        <div class="modal-content">

            <form role="form" id="idForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <!-- <?php $strJSON = json_decode($_POST['objJSON']); ?> -->
                    <h2><b>Nuevo boletín</b></h2>

                </div>
                <div class="modal-body">

                    <!-- <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>"> -->

                    <div class="form-group">
                        
                        <?php                           

                            $con = new clsConexion();
                            $res = $con->prepare("SELECT count(id_boletin) as total_boletines FROM boletines");
                            $res->execute();
                            $fila = $res->fetch();

                            if($fila['total_boletines'] > 0){$readonly = "readonly";}else{$readonly = "";}?>
                                <div class="form-group">
                                    <label for="mesas_instaladas">Mesas instaladas:</label>
                                    <input class="form-control" id="mesas_instaladas" name="mesas_instaladas" <?php echo $readonly;?>></input>
                                </div>
                                <div class="form-group">
                                    <label for="potencial_sufragantes">Potencial sufragantes:</label>
                                    <input class="form-control" id="potencial_sufragantes" name="potencial_sufragantes"></input>
                                </div>

                                <div class="form-group">
                                    <label for="mesas_informadas">Mesas informadas:</label>
                                    <input class="form-control" id="mesas_informadas" name="mesas_informadas" <?php echo $readonly;?>></input>
                                </div>

                                <div class="form-group">
                                    <label for="mesas_informadas">Candidatos:</label>
                                    <select name="candidatos" id="candidatos" class="candidatos">

                            <?php    
                                $res = $con->prepare("SELECT * FROM usuario WHERE cat_codigo = 5");
                                $res->execute();
                                while($fila = $res->fetch())
                                {?>
                                    <option value='<?php echo $fila["usu_codigo"];?>'><?php echo $fila["usu_nombre"];?></option>
                        <?php   }?>
                         
                                    </select>

                        </div>

                        <div class="form-group">
                            <label for="votos_candidato">Votos candidato:</label>
                            <input class="form-control" class="votos_candidatos" id="votos_candidatos" class="votos_candidatos" name="votos_candidatos"></input>
                            <button class="btn btn-success agregarCandidato">Agregar candidato</button>
                            <button class="btn btn-danger quitarCandidato">Quitar candidato</button>

                            <ul class="lista_candidatos"></ul>
                        </div>
                        <!-- <select class="form-control input-sm" id="per_codigo" name="per_codigo[]" multiple size="18" disabled>
                         
                         

                        </select> -->

                         <div class="form-group">
                            <label for="votos_blanco">Votos en blanco:</label>
                            <input class="form-control" id="votos_blanco" name="votos_blanco"></input>
                        </div>

                        <div class="form-group">
                            <label for="votos_nulos">Votos nulos:</label>
                            <input class="form-control" id="votos_nulos" name="votos_nulos"></input>
                        </div>

                        <div class="form-group">
                            <label for="votos_anulados">Votos anulados:</label>
                            <input class="form-control" id="votos_anulados" name="votos_anulados"></input>
                        </div>

                        <div class="form-group">
                            <label for="votos_no_marcados">Votos no marcados:</label>
                            <input class="form-control" id="votos_no_marcados" name="votos_no_marcados"></input>
                        </div>

                        <div class="form-group">
                            <label for="total_votos">Total de votos:</label>
                            <input class="form-control" id="total_votos" name="total_votos" readonly></input>
                        </div>

                        <div class="form-group">
                            <label for="total_sufragantes">Total de sufragantes:</label>
                            <input class="form-control" id="total_sufragantes" name="total_sufragantes" readonly></input>
                        </div>
                    </div>                    

                </div>
                <div class="modal-footer">
                    <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>

                    <?php endif?>
                    <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
                </div>
            </form>

        </div>

    </div>

</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>
<script>
    $('.agregarCandidato').click(function(e){
        e.preventDefault();

        var candidatos;
        var voto_candidato;
        var candidate = [];
        
        var votos = $('#votos_candidatos').val();
        console.log(votos);
        var ul = $('.lista_candidatos');
            candidate['usu_codigo'] = $('.candidatos option:selected').val();
            candidate['candidato'] = $('.candidatos option:selected').text();
        var li = '<li data-votos='+votos+' data-candidato='+candidate['candidato']+'>'+candidate['candidato']+' '+votos+'</li>';
        ul.append(li);

  return false;
});

$('.quitarCandidato').click(function(e){
        e.preventDefault();
    var ul = $('.lista_candidatos');
  var candidate = $('.candidatos option:selected').val();
  console.log(candidate);
  var item = document.getElementById(candidate);
  ul.remove(item);
});
</script>