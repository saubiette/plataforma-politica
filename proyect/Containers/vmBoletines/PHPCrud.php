<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'permisos';
    $ideCampo = "pmi_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    session_start();
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("pmi_select",$_POST['pmi_select']);                         
            $SQLcomando->set_AddCampo("pmi_insert",$_POST['pmi_insert']);
            $SQLcomando->set_AddCampo("pmi_update",$_POST['pmi_update']);
            $SQLcomando->set_AddCampo("pmi_delete",$_POST['pmi_delete']);
            
            if($_POST['id'] != ""){

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");  

            }else{ 
                
                $resp = $conexion->prepare("SELECT $ideCampo 
                                            FROM   $tabla 
                                            WHERE  pmi_opcion = '".$_POST["pmi_opcion"]."' AND usu_codigo = '".$_POST["usu_codigo"]."' ");
                $resp->execute();
                $maxV = $resp->rowCount();
            
                if ($maxV > 0){ 
                    echo false;    
                    $conexion->rollBack();   
                    exit();                   
                }
                
                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);
                                
                $SQLcomando->set_AddCampo("usu_codigo",$_POST['usu_codigo']);
                $SQLcomando->set_AddCampo("pmi_opcion",$_POST['pmi_opcion']);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);
                
                $SQLcomando->set_tipo_comando("INSERT");   

            }           

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':                         
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * 
                        FROM $tabla 
                        WHERE $ideCampo = '$ideValor' ";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;       
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>