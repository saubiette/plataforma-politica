<?php require '../../../php/conexion.php'; ?>

<div id="idModal" class="modal fade">

    <div class="modal-dialog">

        <div class="modal-content">

            <form role="form" id="idForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <?php $strJSON = json_decode($_POST['objJSON']); ?>
                    <h2><b>Envio de Mensajes</b></h2>

                </div>
                <div class="modal-body">

                    <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">

                    <div class="form-group">

                        <label for="chkSelec1"><input id="chkSelec1" name="chkSelec" type="radio" value="TO" checked> Todos</label><br>
                        <label for="chkSelec2"><input id="chkSelec2" name="chkSelec" type="radio" value="AL"> Un solo/ Algunos</label>
                        
                        <select class="form-control input-sm" id="per_codigo" name="per_codigo[]" multiple size="18" disabled>
                         
                         <?php                           

                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM personas WHERE per_celular <> '' AND per_tipo = '".$strJSON->perTipo."' ORDER BY per_nombre ");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['per_codigo']."'>".$fila['per_nombre']." ".$fila['per_apellido']."</option>";
                            }           
                         ?>

                        </select>

                        <div class="form-group">
                            <label for="msj">Mensaje:</label>
                            <textarea class="form-control" maxlength="50"  rows="4" id="msj" name="msj"></textarea>
                        </div>

                    </div>                    

                </div>
                <div class="modal-footer">
                    <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>

                    <?php endif?>
                    <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
                </div>
            </form>

        </div>

    </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script>
    var auxJSON = (<?php echo $_POST['objJSON']; ?>);
    console.log(auxJSON);
</script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.min.js"></script>