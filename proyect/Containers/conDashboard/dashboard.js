
$(document).ready(function () {
    rutaCarpetaActual = './Containers/conDashboard';
    loadMunicipios();
    loadDashboard();
    loadDashboardBarrio();

    function loadDashboard() {
        loadDashboardDepartamento();
    }
    function loadMunicipios() {
        $.post(rutaCarpetaActual + '/municipios.php', {
            dep_codigo: $('#dep_codigo').val()
        }).done(function (data) {
            $('#mun_codigo').html(data)
        });
    }

    function loadDashboardDepartamento() {
        $.ajax({
            url: rutaCarpetaActual + '/dashboardMunicipios.php?departamento=' + $('#dep_codigo').val(),
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            $("#chartMunicipio").CanvasJSChart({
                title: {
                    text: "Votantes municipio",
                    fontFamily: "helvetica",
                },
                toolTip: {
                    fontSize: 10,
                },
                legend: {
                    fontSize: 10,
                },
                animationEnabled: true,
                theme: "light2",
                data: [
                    {
                        type: "doughnut",/*pie*/
                        indexLabelFontFamily: "Garamond",
                        indexLabelFontSize: 12,
                        indexLabel: "{label} {y} votante(s)",
                        startAngle: -20,
                        showInLegend: true,
                        toolTipContent: "{legendText} {y} votante(s)",
                        dataPoints: data
                    }

                ]
            });

        })
            .fail(function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
                $.LoadingOverlay("hide");
            })
    }


    function loadDashboardBarrio() {
        $.ajax({
            url: rutaCarpetaActual + '/dashboardBarrio.php?municipio=' + ($('#mun_codigo').val() == null ? "22" : $('#mun_codigo').val()),
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            $("#chartBarrio").CanvasJSChart({
                title: {
                    text: "Votantes por barrios",
                    fontFamily: "helvetica",
                },
                animationEnabled: true,
                legend: {
                    fontSize: 10,
                },
                theme: "light2",
                data: [
                    {
                        type: "pie",
                        indexLabelFontFamily: "Garamond",
                        indexLabelFontSize: 12,
                        indexLabel: "{label} {y} votante(s)",
                        startAngle: -20,
                        showInLegend: true,
                        toolTipContent: "{legendText} {y} votante(s)",
                        dataPoints: data
                    }
                ]
            });

        })
            .fail(function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
                $.LoadingOverlay("hide");
            })
    }



    $('#dep_codigo').on('change', function () {
        $('#mun_codigo').val('')

        $.ajaxSetup({
            async: false
        })

        loadMunicipios();
        loadDashboardDepartamento();
        loadDashboardBarrio();
        $.ajaxSetup({
            async: true
        })
    });
    $('#mun_codigo').on('change', function () {
        $.ajaxSetup({
            async: false
        })

        loadDashboardBarrio();
        $.ajaxSetup({
            async: true
        })
    });
});