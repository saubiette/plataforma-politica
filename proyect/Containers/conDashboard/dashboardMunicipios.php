<?php
header('Content-Type: application/json');

session_start();
require "../../../php/conexion.php";
$con    = new clsConexion();
//consulta votantes por municipio
$munSql = $con->prepare("select count(m.mun_codigo) as total, m.mun_nombre
                            from personas p 
                            inner join municipios  m on m.mun_codigo = p.mun_codigo
                            where m.dep_codigo =". $_GET["departamento"]."
                            group by m.mun_nombre ");

$munSql->execute();

$array = $munSql->fetchAll(PDO::FETCH_ASSOC);


$dataMun = array();
foreach ($array as $key => $value) {
    array_push($dataMun, array(
        "y" => $array[$key]['total'],
        "legendText" => $array[$key]['mun_nombre'],
        "label" => $array[$key]['mun_nombre']
    ));
}
echo json_encode($dataMun, JSON_NUMERIC_CHECK);
?>
