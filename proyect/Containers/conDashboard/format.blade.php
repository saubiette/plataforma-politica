<div class="container-fluid containers">

    <ol class="breadcrumb">
        <li>
            <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
        </li>
        <li class="active">Dashboard</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div id="chartContainer"></div>
        </div>
    </div>
    <!--row-->
    <div class="row" style="margin-top:450px;margin-bottom:500px;">
        <div class="col-md-6 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <label>Departamento</label>
                </div>
                <div class="card-body">

                    <select class="form-control" id="dep_codigo">
                        <?php
                $con = new clsConexion();
                $res = $con->prepare("SELECT * FROM departamentos ");
                                $res->execute();
                                
                                while($fila = $res->fetch()){
                                    $select = ($fila['dep_codigo'] == '52')?"selected":"";
                                    echo "<option value='".$fila['dep_codigo']."' $select >".$fila['dep_nombre']."</option>";
                                }           
                            ?>
                    </select>
                </div>
                <div id="chartMunicipio">
                </div>
            </div>


        </div>
        <div class="col-md-6 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <label>Municipio</label>
                </div>
                <div class="card-body">
                    <select class="form-control " id="mun_codigo" name="mun_codigo">
                    </select>
                    <div id="chartBarrio"></div>
                </div>
            </div>
        </div>
        <!--row-->


    </div>
</div>