<?php
header('Content-Type: application/json');

session_start();
require "../../../php/conexion.php";
$con    = new clsConexion();
//consulta votantes por municipio
$sql = "select count(REPLACE(REPLACE(per_barrio,'.', ''),'2','')) as total, 
        lower(case when REPLACE(per_barrio,'.', '') is null or REPLACE(per_barrio,'.', '')='' then 'sin barrio' else REPLACE(REPLACE(per_barrio,'.', ''),'2','') end) as barrio 
        from personas p 
        inner join municipios  m on m.mun_codigo = p.mun_codigo 
        where m.mun_codigo = '".$_GET["municipio"]."'
         group by 2 ";
         
        $barSql = $con->prepare($sql);
        $barSql->execute();

        $array = $barSql->fetchAll(PDO::FETCH_ASSOC);
    
    
        $dataBar = array();
        foreach ($array as $key => $value) {
            array_push($dataBar,array("y" => $array[$key]['total'], "legendText" => $array[$key]['barrio'], "label" => $array[$key]['barrio']));
        }
        echo json_encode($dataBar, JSON_NUMERIC_CHECK);
?>
