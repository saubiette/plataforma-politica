<?php
    session_start();
    require "../../../php/conexion.php";

    /*$data = [
        'phone' => '573142198395', // Receivers phone
        'body' => 'Hello, i am test!', // Message
    ];
    $json = json_encode($data); // Encode data to JSON
    // URL for request POST /message
    $url = 'https://eu13.chat-api.com/instance51144/message?token=ccl159z7cy1btp8j';
    // Make a POST request
    $options = stream_context_create(['http' => [
            'method'  => 'POST',
            'header'  => 'Content-type: application/json',
            'content' => $json
        ]
    ]);
    // Send a request
    $result = file_get_contents($url, false, $options);
    print_r($result);*/
    /**Send message */
   /* require '../../../twilio-php-master/Twilio/autoload.php';
    use Twilio\Rest\Client;

    $sid = 'AC9de4a88c47c70a00a27b42010fca12af';
    $token = 'af2b3a257aae32b8a5d05d96b9ff3d1f';
    $client = new Client($sid, $token);

    // Use the client to do fun stuff like send text messages!
    $client->messages->create(
        // the number you'd like to send the message to
        '+573142198395',
        array(
            // A Twilio phone number you purchased at twilio.com/console
            'from' => '+17632194048',
            // the body of the text message you'd like to send
            'body' => "Testing"
        )
    );*/

?>

<?php
    $con = new clsConexion();
     //consulta votantes por departamento
    $pru = $con->prepare("select count(d.dep_codigo) as total , d.dep_nombre
                        from personas p 
                        inner join municipios  m on m.mun_codigo = p.mun_codigo
                        inner join departamentos d on d.dep_codigo = m.dep_codigo
                        group by d.dep_nombre order by 1 desc ");

    $pru->execute();

    $array = $pru->fetchAll(PDO::FETCH_ASSOC);

    
    $dataPoints = array();
    foreach ($array as $key => $value) {
        array_push($dataPoints,array("y" => $array[$key]['total'], "label" => $array[$key]['dep_nombre']));
    }

   
?>
<script type="text/javascript">

    $(function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "light2",
            animationEnabled: true,
            type: "bar",
            showInLegend:true,
             legend: {
                    fontSize: 10,
                },
            axisYType: "secondary",
            axisX:{
		        interval: 1
	        },
            axisY2:{
                interlacedColor: "rgba(1,77,101,.2)",
                gridColor: "rgba(1,77,101,.1)",
                
            },
            title: {
                fontFamily: "helvetica",
            },
            toolTip:{   
                content: "{y} votante(s)",
            },
            data: [
            {
                indexLabel: "{y} votante(s)",
                type: "doughnut",  
                indexLabelFontColor: "white",              
                dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
            },
            ]
        });
        chart.render();

    

    });
</script>

<div class="container-fluid containers">

    <ol class="breadcrumb">
        <li>
            <i class="fa fa-fw fa-bar-chart-o"></i> Navegación
        </li>
        <li class="active">Dashboard</li>
    </ol>
    <div class="row">
        <div class="col-md-4">
            <div>
                    <label>Departamentos</label>
                </div>
            <div style="margin-top: 10%;" id="chartContainer"></div>
        </div>
        <div class="col-md-4">

            <div class="card">
                <div class="card-header">
                    <label>Departamento</label>
                </div>
                <div class="card-body">

                    <select class="form-control" id="dep_codigo">
                        <?php
                $con = new clsConexion();
                $res = $con->prepare("SELECT * FROM departamentos ");
                                $res->execute();
                                
                                while($fila = $res->fetch()){
                                    $select = ($fila['dep_codigo'] == '3')?"selected":"";
                                    echo "<option value='".$fila['dep_codigo']."' $select >".$fila['dep_nombre']."</option>";
                                }           
                            ?>
                    </select>
                </div>
                <div id="chartMunicipio">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <label>Municipio</label>
                </div>
                <div class="card-body">
                    <select class="form-control " id="mun_codigo" name="mun_codigo">
                    </select>
                    <div id="chartBarrio"></div>
                </div>
            </div>
        </div>
    </div>
</div>	
    
    <?php
        $ruta = str_replace('\\',"/",__DIR__);
        $vec= explode('/',$ruta);                
    ?>

    <script src="./Containers/conDashboard/dashboard.js"></script>
