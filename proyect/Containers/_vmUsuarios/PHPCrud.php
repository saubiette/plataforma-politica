<?php

require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();        
    $conexion->beginTransaction();   

    $tabla = 'usuario';
    $ideCampo = "usu_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    session_start();
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("usu_nombre",$_POST['usu_nombre']);  
            $SQLcomando->set_AddCampo("usu_email",$_POST['usu_email']);  
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']);  
            $SQLcomando->set_AddCampo("cat_codigo",$_POST['cat_codigo']);  
            $SQLcomando->set_AddCampo("usu_nota",$_POST['usu_nota']);  

            if($_POST['usu_contraseña'] != ""){
                $SQLcomando->set_AddCampo("usu_password", hash("sha256",$_POST['usu_contraseña']));
            }            
                       
            if($_POST['id'] != ""){


                if($_POST['contrasenna_anterior'] != ""){
                    $strSQL = "SELECT $ideCampo 
                               FROM $tabla 
                               WHERE usu_password = '".hash("sha256",$_POST['contrasenna_anterior'])."' AND 
                                     usu_codigo = '$ideValor'";

                    $resp = $conexion->prepare($strSQL);
                    $resp->execute();
                    $maxV = $resp->rowCount();
                
                    if ($maxV == 0){ 
                        echo '-2';  //significa que contraseña no coincidio
                        $conexion->rollBack();   
                        exit();                   
                    }

                }                

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");  

                if($_POST['id'] == $_SESSION["usu_codigo"] ){       
                                
                    $_SESSION["usu_nombre"] = utf8_decode($_POST['usu_nombre']);          		

                }                       

            }else{

                $resp = $conexion->prepare("SELECT $ideCampo FROM $tabla WHERE UPPER(usu_login) = '".strtoupper($_POST["usu_login"])."'");
                $resp->execute();
                $maxV = $resp->rowCount();
            
                if ($maxV > 0){ 
                    echo false;    
                    $conexion->rollBack();   
                    exit();                   
                }

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1
                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);
                $SQLcomando->set_AddCampo("usu_login",$_POST['usu_login']);
                $SQLcomando->set_AddCampo("usu_activo","S");
                $SQLcomando->set_AddCampo("usu_fecha_creacion","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_ruta_imagen","");

                $SQLcomando->set_AddCampo("usu_nivel_acceso", $_POST['usu_nivel_acceso']);
                $_SESSION["usu_nivel_acceso"] = $_POST['usu_nivel_acceso'];  

                $SQLcomando->set_tipo_comando("INSERT");   

            }

            if($_FILES['usu_ruta_imagen']['name'] != ""){

                $foto = $_FILES['usu_ruta_imagen'];
                $tmp_name = $foto["tmp_name"]; 
                $tipo = $foto['type']; 	

                $nombre = $foto['name'];
                $tmp =  explode('.', $nombre);
                $extImg = end($tmp);

                $RutImg = $ideValor.'.'.$extImg;

                if($_POST['emp'] == "S"):
                    $RutImg = "usu_".$RutImg;
                else:
                    $RutImg = "adm_".$RutImg;
                endif;

                $ruta = "../../images/Perfiles/".$RutImg;
                if(file_exists($ruta)){unlink($ruta);}
                if(!copy($tmp_name, $ruta)){echo "Error al copiar foto";}

                $SQLcomando->set_AddCampo("usu_ruta_imagen",$RutImg);

                if($_POST['id'] == $_SESSION["usu_codigo"] ){$_SESSION["ruta_img"] = $RutImg;}                

            }     
            else{
                //$SQLcomando->set_AddCampo("usu_ruta_imagen","");
            }

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':
            $resp = $conexion->prepare("SELECT * FROM permisos WHERE $ideCampo = '$ideValor'");
            $resp->execute();
            $r = $resp->rowCount();
            
            if($r > 0){
                echo false;
                $conexion->rollBack();   
                exit();   
            }              
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT * 
                        FROM $tabla INNER JOIN 
                             municipios ON $tabla.mun_codigo = municipios.mun_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

         case 'ACT':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","S");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
        
         case 'DES':

             $SQLcomando->set_tipo_comando("UPDATE");
             $SQLcomando->set_AddCampo("usu_activo","N");
             $SQLcomando->set_where($ideCampo." = '".$_POST['id']."'");
             
             $resp = $conexion->prepare($SQLcomando->get_sql());
             $resp->execute();            
                         
             break;
    
    }

    $conexion->commit();   

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>