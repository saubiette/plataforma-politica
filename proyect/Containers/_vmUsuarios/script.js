$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    var res = $.trim($('#usu_usuario').val()).indexOf(' ')

    if (res != '-1') {
      swal('El Login/Usuario no puede contener espacios', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    if ($('#usu_contraseña').val() != $('#usu_contraseña_2').val()) {
      swal('No coinciden las contraseñas', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    if ($('#usu_contraseña').val() != '' && $('#id').val() != '' && $('#contrasenna_anterior').val() == '') {
      swal('Ingrese su contraseña anterior', 'Revisa e intenta nuevamente', 'warning')
      $('.load').LoadingOverlay('hide')
      return false
    }

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)
    formData.append('tipo_usuario', auxJSON.TipUsuario)

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        if (data == false) {
          swal('El nombre de login/usuario ya existe', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }

        if (data == '-2') {
          swal('Contraseña anterior no coincide.', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }

        $('.dataTable').DataTable().ajax.reload()

        if ($('#id').val() == usu_codigo) {location.reload();}

        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('.file').fileinput({
    allowedFileExtensions: ['jpg', 'png', 'gif']
  })

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {

      // ./images/Perfiles / user.png

      if (data.usu_ruta_imagen != '') {
        $('#extFoto').css('display', 'initial')
      }

      var info = data.usu_fecha_creacion.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#usu_fecha_creacion').html(fechaFormateada)

      $('#usu_login').val(data.usu_login)
      $('#usu_login').prop('disabled', true)

      $('#usu_nivel_acceso').val(data.usu_nivel_acceso)
      $('#usu_nivel_acceso').prop('disabled', true)

      $('#usu_nombre').val(data.usu_nombre)
      $('#usu_admin').val(data.usu_admin)
      $('#usu_email').val(data.usu_email)

      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo)

      $('#cat_codigo').val(data.cat_codigo)
      $('#usu_nota').val(data.usu_nota)

      /*if(data.ide_empresa != "")
          $("#ide_empresa").val(data.ide_empresa);*/

      $('#usu_contraseña').removeClass('required')
      $('#usu_contraseña_2').removeClass('required')

      if (auxJSON.ver != undefined) {
        $('#idForm input').attr('readonly', 'readonly')
        $('#idForm select').prop('disabled', 'true')
        $('#idForm textarea').prop('disabled', 'true')

        if (data.usu_ruta_imagen != '') {
          $('#img-perfil').css('display', 'block')
          $('#img-perfil').attr('src', './images/Perfiles/' + data.usu_ruta_imagen)
        }
      }
    }, 'json')
  }

  $('#usu_login').focusout(function () {
    if ($('#usu_login').val().search(' ') != -1) {
      var texto = $('#usu_login').val()
      texto = texto.replace(/\ /g, '')
      $('#usu_login').val(texto)
    }

    if ($('#usu_nivel_acceso').val() == 'S') {
      if ($('#usu_login').val().search('admin_') != -1) return false
      $('#usu_login').val('admin_' + $('#usu_login').val())
    }
    else if ($('#usu_nivel_acceso').val() == 'I') {
      if ($('#usu_login').val().search('admin') == -1) return false
      $('#usu_login').val('')
    }
  })

  $('#usu_nivel_acceso').on('change', function () {
    $('#usu_login').val('')
  })

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()
})
