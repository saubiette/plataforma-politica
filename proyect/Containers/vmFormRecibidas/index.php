<?php 
 session_start();
    require '../../../php/conexion.php';
    require "../../generalesPHP/funciones.php";

    $strJSON = json_decode($_POST['objJSON']);

    $modPermiso = "ADF";

    if(isset($_POST['id'])):
        if(isset($strJSON->ver))
             $strCampo = fncPermiso($modPermiso,"R", new clsConexion()); 
        else
             $strCampo = fncPermiso($modPermiso,"C", new clsConexion());    
    else:
         $strCampo = fncPermiso($modPermiso,"C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Calificar Listas Recibidas</b></h2> (Fecha de Registro <span id='fecha_registro'></span>)
                    <?php else:?>
                        <h2><b>Registrar Listas Recibidas</b></h2>
                    <?php endif?>   
                
                <?php else:?>  

                    <h2><b>Registro Listas Recibidas</b></h2>
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                               
                
                <div class="form-group">
                    <label for="efo_fecha">(*)Fecha:</label>                
                    <input type="date" class="form-control input-sm required"  value="<?= date("Y-m-d"); ?>"  id="efo_fecha" name="efo_fecha" >     
                    <label for="efo_fecha" class="label label-danger error" generated="true"></label>               
                </div>     

                <?php if(!isset($strJSON->Calificar) && !isset($strJSON->ver)): ?>

                    <div class="form-group">
                        <label class="col-lg-6 paddin-no-left" for="form_faltantes">No. Formularios Faltantes:</label>
                        <label class="col-lg-6 paddin-no-left" for="efo_numero">(*)No. Formularios Recibidos:</label>
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="number" class="form-control input-sm" readonly id="form_faltantes" name="form_faltantes" placeholder="0">
                            </div>
                            <div class="col-lg-6 paddin-no-left">
                                <input type="number" class="form-control input-sm required" min=1  id="efo_numero" name="efo_numero" value="0">
                                <label for="efo_numero" class="label label-danger error" generated="true"></label>   
                            </div>
                        </div>                                    
                    </div> 

                <?php endif;?> 

                <?php if(isset($strJSON->Calificar) || isset($strJSON->ver)): ?>

                     <div class="form-group">
                        <label for="efo_numero">No. Formularios Recibidos:</label>
                        <input type="number" step="any" class="form-control input-sm required" id="efo_numero" name="efo_numero" value="0">  
                        <label for="efo_numero" class="label label-danger error" generated="true"></label>                   
                    </div>

                    <div class="form-group">
                        <label for="efo_firnas">(*)No. Firmas:</label>
                        <input type="number" step="any" class="form-control input-sm required" id="efo_firnas" min=1  name="efo_firnas" placeholder="Introduce un No. de Firmas">  
                        <label for="efo_firnas" class="label label-danger error" generated="true"></label>                   
                    </div>

                    <div class="form-group">
                        <label for="efo_confirmadas">(*)No. Firmas Confirmadas:</label>
                        <input type="number" step="any" class="form-control input-sm required" id="efo_confirmadas" min=0  name="efo_confirmadas" placeholder="Introduce un No. de Firmas Confirmadas">  
                        <label for="efo_confirmadas" class="label label-danger error" generated="true"></label>                   
                    </div>

                    <div class="form-group">
                        <label for="efo_error">(*)No. Firmas Erroneas:</label>
                        <input type="number" step="any" class="form-control input-sm required" id="efo_error"min=0  name="efo_error" placeholder="Introduce un No. de Firmas Erroneas">  
                        <label for="efo_error" class="label label-danger error" generated="true"></label>                   
                    </div>
                
                <?php endif;?> 
                
                <div class="form-group">
                    <label for="efo_comentario">Comentario:</label>
                    <textarea class="form-control" rows="5" id="efo_comentario" name="efo_comentario"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>              
                <?php endif?>
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.min.js"></script>