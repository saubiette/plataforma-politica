<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();     
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    $conexion->beginTransaction();   

    $tabla = 'entrega_formularios';
    $ideCampo = "efo_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S","PAG","REE"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
            case 'PAG':  $aux= "C"; break;
            case 'REE':  $aux= "C"; break;
        }

        $strCampo = fncPermiso("ADF", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }
    
    switch ($opc) {
        
        case 'IU':                     
                 
            if($_POST['id'] != ""){     
                
                $SQLcomando->set_AddCampo("efo_firnas",$_POST['efo_firnas']);              
                $SQLcomando->set_AddCampo("efo_confirmadas",$_POST['efo_confirmadas']); 
                $SQLcomando->set_AddCampo("efo_error",$_POST['efo_error']);                   

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                      

            }else{             

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);              
                                
                session_start();
                $SQLcomando->set_AddCampo("efo_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);    
                
                $SQLcomando->set_AddCampo("efo_fecha",$_POST['efo_fecha']);              
                $SQLcomando->set_AddCampo("efo_numero",$_POST['efo_numero']); 
                $SQLcomando->set_AddCampo("efo_pago","N");  
                $SQLcomando->set_AddCampo("for_codigo",$_POST['for_codigo']); 
              
                $SQLcomando->set_tipo_comando("INSERT");   

            }   
            
             $SQLcomando->set_AddCampo("efo_comentario",$_POST['efo_comentario']);   

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':                      
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT *  
                        FROM $tabla INNER JOIN 
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;
        
        case 'PAG':

             $strSQL = "UPDATE $tabla SET efo_pago = 'S' WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();            
            
             echo true;            
             break;

        case 'REE':

             $strSQL = "UPDATE $tabla SET efo_pago = 'N' WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();            
            
             echo true;            
             break;

    }

    $conexion->commit();   

} 
catch (Exception $e) {
    
    if($e->errorInfo[0] == '23000')
        echo "fk";    
    else
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";

    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>