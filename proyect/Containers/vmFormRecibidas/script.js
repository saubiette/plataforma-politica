$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  $('#form_faltantes').val(auxJSON.faltantes)

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    if (parseInt($('#efo_numero').val()) > parseInt($('#form_faltantes').val())) {
      swal('El No. de formularios recibidos no puede ser mayor al No. de formularios faltantes', 'Revisa e intenta nuevamente', 'warning')
      return false
    }

    $('#efo_firnas').prop('disabled', false);

    $('.load').LoadingOverlay('show')

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)
    formData.append('for_codigo', auxJSON.for_codigo)

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        $('.dataTable').DataTable().ajax.reload()
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false;
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show');  

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      var info = data.efo_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#efo_fecha').val(data.efo_fecha);
     
      $numFirmas = parseInt(data.efo_numero) * 15;
      $("#efo_firnas").val($numFirmas);
      $('#efo_firnas').prop('disabled', true)

      $('#efo_fecha').prop('disabled', true)
      $('#efo_numero').val(data.efo_numero)
      $('#efo_numero').prop('disabled', true)
     // $('#efo_firnas').val(data.efo_firnas)
      $('#efo_confirmadas').val(data.efo_confirmadas)
      $('#efo_error').val(data.efo_error)
      $('#efo_comentario').val(data.efo_comentario)

      if (auxJSON.ver != undefined) $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
    }, 'json')
  }

  // $('.formatoMiles').numberFM(true, 2, ',', '.')

  $('#efo_firnas').on('keyup', function () {
    $('#efo_error').val(0)
    $('#efo_confirmadas').val(0)
  })

  $('#efo_confirmadas').on('keyup', function () {
    $('#efo_error').val(parseInt($('#efo_firnas').val()) - parseInt($('#efo_confirmadas').val()))
  })

  $('#efo_error').on('keyup', function () {
    $('#efo_confirmadas').val(parseInt($('#efo_firnas').val()) - parseInt($('#efo_error').val()))
  })   

})
