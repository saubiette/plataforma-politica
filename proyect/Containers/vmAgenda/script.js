$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  $('#cal_fecha').val(auxJSON.fecha)

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)

    // alert($('cal_fecha').val())
    // console.log(formData)
    // return false

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        $('#calendar').fullCalendar('refetchEvents')
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })
      alertify.success('Agenda Actualizada')
    return false
  })

  $('#idEliminar').on('click', function () {
    swal({
      title: '¿Seguro que deseas eliminar este Registro?',
      text: 'No podrás deshacer este paso...',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar',
    closeOnConfirm: true },
      function () {
        $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'D', id: $('#id').val() }, function (data) {
          if (data == false) {
            swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          }else if (data == 'DN') {
            swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
          }else {
            $('#calendar').fullCalendar('refetchEvents')
            $('#idCerrar').click()
          }
        }, 'json')
      })
    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  $('#idEliminar').hide();

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {



    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data == 'DN') {
        swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        $('#idCerrar').click()
        return false
      }

      var info = data.cal_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]


      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#cal_fecha').val(data.cal_fecha)
      $('#cal_hora_inicio').val(data.cal_hora_inicio)
      $('#cal_hora_fin').val(data.cal_hora_fin)
      $('#cal_titulo').val(data.cal_titulo)
      $('#cal_direccion').val(data.cal_direccion)
      $('#observacion').val(data.observacion)
      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo_Per)

      $('#cal_identificacion').val(data.cal_identificacion)
      $('#cal_nombre').val(data.cal_nombre)
      $('#cal_telefono').val(data.cal_telefono)

      $('#jefes').val(data.jefe_id)
      $('#delegado').val(data.delegado_id)
      $('#n_personas').val(data.n_personas)

      $('#sillas_cantidad').val(data.sillas_cantidad)
      $('#sillas_dinero').val(data.sillas_dinero)
      $('#sonido_cantidad').val(data.sonido_cantidad)
      $('#sonido_dinero').val(data.sonido_dinero)
      $('#tarima_cantidad').val(data.tarima_cantidad)
      $('#tarima_dinero').val(data.tarima_dinero)
      $('#carpa_cantidad').val(data.carpa_cantidad)
      $('#carpa_dinero').val(data.carpa_dinero)
      $('#video_cantidad').val(data.video_cantidad)
      $('#video_dinero').val(data.video_dinero)
      $('#pantalla_cantidad').val(data.pantalla_cantidad)
      $('#pantalla_dinero').val(data.pantalla_dinero)
      $('#artista_cantidad').val(data.artista_cantidad)
      $('#artista_dinero').val(data.artista_dinero)
      $('#avanzada_cantidad').val(data.avanzada_cantidad)
      $('#avanzada_dinero').val(data.avanzada_dinero)
      $('#transporte_cantidad').val(data.transporte_cantidad)
      $('#transporte_dinero').val(data.transporte_dinero)

      if (data.desayuno) {
        $('#desayuno').prop("checked", true);
      }
      if (data.refrigerio) {
        $('#refrigerio').prop("checked", true);
      }
      if (data.almuerzo) {
        $('#almuerzo').prop("checked", true);
      }
      if (data.cena) {
        $('#cena').prop("checked", true);
      }
      $('#cantidad_alimentos').val(data.cantidad_alimentos)

      if (!(auxJSON.ver == undefined || data.usu_codigo_registra == $('#codigo').val())){
        $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
        $('#desayuno').attr('disabled','true')
        $('#refrigerio').attr('disabled','true')
        $('#almuerzo').attr('disabled','true')
        $('#cena').attr('disabled','true')
        $('#idGuardar').hide()
      }

      if (auxJSON.ver == undefined || data.usu_codigo_registra == $('#codigo').val()){
        $('#idEliminar').show()
      }


    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  });

  $('#n_personas').on('change', function () {
    // $('#cantidad_alimentos').prop('disabled', true);
    $('#cantidad_alimentos').val($('#n_personas').val());
  });

  function delegado() {
    $('#delegado').val('')

    $.ajaxSetup({ async: false })

    $.get(rutaCarpetaActual + '/delegados.php')
      .done(function (data) {
        $('#delegado').html(data)
      })

    $.ajaxSetup({ async: true })
  }

  function jefes() {
    $('#jefes').val('')

    $.ajaxSetup({ async: false })

    $.get(rutaCarpetaActual + '/jefes.php')
      .done(function (data) {
        $('#jefes').html(data)
      })

    $.ajaxSetup({ async: true })
  }

  $('#cal_hora_inicio').on('change', function () {

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/horas.php',{inicio: $('#cal_hora_inicio').val, fin: $('#cal_hora_fin').val})
      .done(function (data) {

          if (data == '-2') {
            swal(data, 'Contacte al administrador del sistema', 'warning')
            return false
          }


      })

    $.ajaxSetup({ async: true })
  });


  delegado();
  jefes();

  $('#dep_codigo').change();

  // $('.formatoMiles').numberFM(true, 2, ',', '.')

})
