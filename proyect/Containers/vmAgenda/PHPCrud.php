<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');

require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();     
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    $conexion->beginTransaction();   

    $tabla = 'calendario';
    $tablaDet = 'detalles_agenda';
    $tablaLogistica = 'logistica';
    $tablaAlimentos = 'alimentos';
    $ideCampo = "cal_codigo";
    $ideValor = $_POST['id'];

    $agenda_id = MaxCampo($tabla,$ideCampo,$conexion);

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $SQLcomandoDet = new SQLComando();
    $SQLcomandoDet->set_tabla($tablaDet);

    $SQLcomandoLogistica = new SQLComando();
    $SQLcomandoLogistica->set_tabla($tablaLogistica);

    $SQLcomandoAlimentos = new SQLComando();
    $SQLcomandoAlimentos->set_tabla($tablaAlimentos);

    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        $strCampo = fncPermiso("CAL", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }    

    //session_start();   
    
    switch ($opc) {
        
        case 'IU': 

            $SQLcomando->set_AddCampo("cal_fecha",$_POST['cal_fecha']);              
            $SQLcomando->set_AddCampo("cal_hora_inicio",$_POST['cal_hora_inicio']); 
            $SQLcomando->set_AddCampo("cal_hora_fin",$_POST['cal_hora_fin']); 
            $SQLcomando->set_AddCampo("cal_titulo",$_POST['cal_titulo']); 
            $SQLcomando->set_AddCampo("cal_direccion",$_POST['cal_direccion']); 
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']); 

            $SQLcomando->set_AddCampo("cal_identificacion",$_POST['cal_identificacion']); 
            $SQLcomando->set_AddCampo("cal_nombre",$_POST['cal_nombre']); 
            $SQLcomando->set_AddCampo("cal_telefono",$_POST['cal_telefono']);

            // Agregar campos de tabla "detalles_agenda" 

            $SQLcomandoDet->set_AddCampo("jefe_id",$_POST['jefes']);
            $SQLcomandoDet->set_AddCampo("delegado_id",$_POST['delegado']);
            $SQLcomandoDet->set_AddCampo("n_personas",$_POST['n_personas']); 

             // Agregar campos de tabla "logistica"

            if ($_POST['sillas_dinero'] || $_POST['sillas_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("sillas_dinero",$_POST['sillas_dinero']);
                $SQLcomandoLogistica->set_AddCampo("sillas_cantidad",$_POST['sillas_cantidad']);
            }
            if ($_POST['sonido_dinero'] || $_POST['sonido_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("sonido_dinero",$_POST['sonido_dinero']);
                $SQLcomandoLogistica->set_AddCampo("sonido_cantidad",$_POST['sonido_cantidad']);
            }
            if ($_POST['tarima_dinero'] || $_POST['tarima_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("tarima_dinero",$_POST['tarima_dinero']);
                $SQLcomandoLogistica->set_AddCampo("tarima_cantidad",$_POST['tarima_cantidad']);
            }
            if ($_POST['carpa_dinero'] || $_POST['carpa_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("carpa_dinero",$_POST['carpa_dinero']);
                $SQLcomandoLogistica->set_AddCampo("carpa_cantidad",$_POST['carpa_cantidad']);
            }
            if ($_POST['video_dinero'] || $_POST['video_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("video_dinero",$_POST['video_dinero']);
                $SQLcomandoLogistica->set_AddCampo("video_cantidad",$_POST['video_cantidad']);
            }
            if ($_POST['pantalla_dinero'] || $_POST['pantalla_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("pantalla_dinero",$_POST['pantalla_dinero']);
                $SQLcomandoLogistica->set_AddCampo("pantalla_cantidad",$_POST['pantalla_cantidad']);
            }
            if ($_POST['artista_dinero'] || $_POST['artista_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("artista_dinero",$_POST['artista_dinero']);
                $SQLcomandoLogistica->set_AddCampo("artista_cantidad",$_POST['artista_cantidad']);
            }
            if ($_POST['avanzada_dinero'] || $_POST['avanzada_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("avanzada_dinero",$_POST['avanzada_dinero']);
                $SQLcomandoLogistica->set_AddCampo("avanzada_cantidad",$_POST['avanzada_cantidad']);
            }
            if ($_POST['transporte_dinero'] || $_POST['transporte_cantidad']) {
                $SQLcomandoLogistica->set_AddCampo("transporte_dinero",$_POST['transporte_dinero']);
                $SQLcomandoLogistica->set_AddCampo("transporte_cantidad",$_POST['transporte_cantidad']);
            }
            if ($_POST['observacion']) {
                $SQLcomandoLogistica->set_AddCampo("observacion",$_POST['observacion']);
            }
            // Agregar campos de tabla "Alimentos"

            if ($_POST['desayuno']){
                $SQLcomandoAlimentos->set_AddCampo("desayuno",$_POST['desayuno']);
            } 
            if($_POST['refrigerio']){
                $SQLcomandoAlimentos->set_AddCampo("refrigerio",$_POST['refrigerio']);
            }
            if($_POST['almuerzo']){
                $SQLcomandoAlimentos->set_AddCampo("almuerzo",$_POST['almuerzo']);
            } 
            if($_POST['cena']) {    
                $SQLcomandoAlimentos->set_AddCampo("cena",$_POST['cena']);
            }

            $SQLcomandoAlimentos->set_AddCampo("cantidad_alimentos",$_POST['n_personas']);

            // -----------------------------------
   
            if($_POST['id'] != ""){                            

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomandoDet->set_where("agenda_id = '".$ideValor."'");
                $SQLcomandoLogistica->set_where("calendario_id = '".$ideValor."'");
                $SQLcomandoAlimentos->set_where("calendario_id = '".$ideValor."'");

                $SQLcomando->set_AddCampo("cal_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                $SQLcomandoDet->set_AddCampo("agenda_id", $ideValor);
                $SQLcomandoLogistica->set_AddCampo("calendario_id", $ideValor);
                $SQLcomandoAlimentos->set_AddCampo("calendario_id", $ideValor);

                $SQLcomando->set_tipo_comando("UPDATE");
                $SQLcomandoDet->set_tipo_comando("UPDATE");  
                $SQLcomandoLogistica->set_tipo_comando("UPDATE");
                $SQLcomandoAlimentos->set_tipo_comando("UPDATE");                                      

            }else{
      

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);              
                                
                //session_start();
                $SQLcomando->set_AddCampo("cal_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                

                $SQLcomandoDet->set_AddCampo("agenda_id", $ideValor);
                $SQLcomandoLogistica->set_AddCampo("calendario_id", $ideValor);
                $SQLcomandoAlimentos->set_AddCampo("calendario_id", $ideValor);
                             
              
                $SQLcomando->set_tipo_comando("INSERT");
                $SQLcomandoDet->set_tipo_comando("INSERT");  
                $SQLcomandoLogistica->set_tipo_comando("INSERT");
                $SQLcomandoAlimentos->set_tipo_comando("INSERT");  

            }    

                    

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute();

            $resp2 = $conexion->prepare($SQLcomandoDet->get_sql());
            $resp2->execute(); 

            $resp3 = $conexion->prepare($SQLcomandoLogistica->get_sql());
            $resp3->execute(); 

            $respAlimentos = $conexion->prepare($SQLcomandoAlimentos->get_sql());
            $respAlimentos->execute();

            echo true;            
            break;   
            
        case 'D':                  
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT *  , $tabla.mun_codigo as mun_codigo_Per
                        FROM $tabla 
                        INNER JOIN municipios ON $tabla.mun_codigo = municipios.mun_codigo 
                        INNER JOIN usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        INNER JOIN detalles_agenda ON $tabla.cal_codigo = detalles_agenda.agenda_id
                        INNER JOIN logistica ON $tabla.cal_codigo = logistica.calendario_id
                        INNER JOIN alimentos ON $tabla.cal_codigo = alimentos.calendario_id 
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

    }

    $conexion->commit();   

} 
catch (Exception $e) {
    
    if($e->errorInfo[0] == '23000')
        echo "fk";    
    else
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";

    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}



?>