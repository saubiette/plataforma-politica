<?php 
session_start();
    require '../../../php/conexion.php';     
    require "../../generalesPHP/funciones.php";
    
    if(isset($_POST['id'])):
        //$strCampo = fncPermiso("CAL","U", new clsConexion()); 
        $strCampo = "-";
    else:
         $strCampo = fncPermiso("CAL","C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }  
?>


<div id="idModal" class="modal fade">

   <div class="modal-dialog modal-lg">  

      <div class="modal-content">

           <form role="form" id="idForm" >
            <div class="modal-header">
                <button type="button btn btn-primary" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar Agenda</b></h2> 
                    <?php else:?>
                        <h2><b>Registrar Agenda</b></h2>
                    <?php endif;?>   
                
                <?php else:?>  

                    <h2><b>Agenda</b></h2>
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif?>
                
            </div>
            <div class="modal-body">

                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#agenda">REGISTRAR AGENDA</a></li>
                    <li><a data-toggle="tab" href="#logis">LOGISTICA</a></li>
                </ul>
                <div class="tab-content">
                    <div id="agenda" class="tab-pane fade in active">

                        <div class="row">

                            <div class="col-xs-6 ">
                                <label for="jefe">JEFE(*):</label>
                                <select class="form-control input-sm " id="jefes" name="jefes">                        
                                </select>               
                            </div>

                            <div class="col-xs-6 ">
                                <label for="DELEGADO">DELEGADO:</label>
                                <select class=" form-control input-sm " name="delegado" id="delegado">
                                
                                </select>
                            </div>

                            <div class="col-xs-6 ">
                                <label for="titulo">TITULO(*):</label>
                                <input class="form-control input-sm required" required type="text" name="cal_titulo" id="cal_titulo">
                            </div>

                            <div class="col-xs-6 ">
                                <label for="npersonas">N°. PERSONAS:</label>
                                <input class="form-control input-sm" type="number" name="n_personas" id="n_personas">
                            </div>

                            <div class="col-xs-4 ">
                                <label for="fecha">FECHA(*):</label>
                                <input type="date" class="form-control input-sm required" required id="cal_fecha" require name="cal_fecha" placeholder="Introduce una fecha">     
                                <label for="cal_fecha" class="label label-danger error" generated="true"></label>   
                            </div>

                            <div class="col-xs-4 ">
                                <label for="inicio">HORA INICIO(*):</label>
                                <input type="time" class="form-control input-sm" required name="cal_hora_inicio" id="cal_hora_inicio" require>
                            </div>

                            <div class="col-xs-4 ">
                                <label for="final">HORA FINAL(*):</label>
                                <input type="time" class="form-control input-sm" required name="cal_hora_fin" id="cal_hora_fin" require>
                            </div>

                            <div class="col-xs-12">
                                <br>
                                <h3><strong>DATOS DEL ORGANIZADOR</strong></h3>
                            </div>

                            <div class="col-xs-6 ">
                                <label for="NOMBRE">NOMBRE:</label>
                                <input type="text" class="form-control input-sm" name="cal_nombre" id="cal_nombre">
                            </div>

                            <div class="col-xs-6 ">
                                <label for="celular">CELULAR:</label>
                                <input class="form-control input-sm" type="number" name="cal_telefono" id="cal_telefono">
                            </div>

                            <div class="col-xs-12">
                                <br>
                                <h3><strong>DIRECCION DE LA REUNIÓN</strong></h3>
                            </div>

                            <div class="col-xs-12 ">
                                <textarea class="form-control" rows="5" id="cal_direccion" name="cal_direccion"></textarea>
                            </div>

                            <div class="col-xs-6 ">
                                <label for="DEPARTAMENTO">DEPARTAMENTO:</label>
                                <select class="form-control input-sm opcion required" id="dep_codigo" name="dep_codigo">
                       
                                     <?php
                                        $con = new clsConexion();
                                        $res = $con->prepare("SELECT * FROM departamentos ");
                                        $res->execute();
                                        
                                        while($fila = $res->fetch()){
                                            if($fila['dep_codigo']==52){
                                                echo "<option  selected=\"selected\"  value='".$fila['dep_codigo']."'>".$fila['dep_nombre']."</option>";
                                            } else{
                                                echo "<option  value='".$fila['dep_codigo']."'>".$fila['dep_nombre']."</option>";
                                            }
                                            
                                        }           
                                     ?> 

                                </select>
                            </div>

                            <div class="col-xs-6 ">
                                <label for="">MUNICIPIO:</label>
                                <select class="form-control input-sm" id="mun_codigo" name="mun_codigo">                        
                                </select>  
                            </div>
                        </div>
                    </div>

                    <div id="logis" class="tab-pane fade in">
                        <div class="row">
                            
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>SILLAS</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="sillas_cantidad" id="sillas_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" onkeyup="format(this)" onchange="format(this)" type="text" name="sillas_dinero" id="sillas_dinero" placeholder="Dinero">
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>SONIDO</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="sonido_cantidad" id="sonido_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="sonido_dinero" id="sonido_dinero" placeholder="Dinero">
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>TARIMA</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="tarima_cantidad" id="tarima_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="tarima_dinero" id="tarima_dinero" placeholder="Dinero">
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>CARPA</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="carpa_cantidad" id="carpa_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="carpa_dinero" id="carpa_dinero" placeholder="Dinero">
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>VIDEOBEAM</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="video_cantidad" id="video_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="video_dinero" id="video_dinero" placeholder="Dinero">
                                    </div>                                   
                                </div>
                            </div>
                            
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>PANTALLA</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="pantalla_cantidad" id="pantalla_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="pantalla_dinero" id="pantalla_dinero" placeholder="Dinero">
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>ARTISTA</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="artista_cantidad" id="artista_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)"name="artista_dinero" id="artista_dinero" placeholder="Dinero">
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>AVANZADA</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="avanzada_cantidad" id="avanzada_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="avanzada_dinero" id="avanzada_dinero" placeholder="Dinero">
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>TRANSPORTE</label>
                                    <input  value="0" class="form-control input-sm" type="number" name="transporte_cantidad" id="transporte_cantidad" placeholder="Cantidad">
                                    <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input class="form-control input-sm" type="text" onkeyup="format(this)" onchange="format(this)" name="transporte_dinero" id="transporte_dinero" placeholder="Dinero">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <br>
                                <h3><strong>OBSERVACIONES</strong></h3>
                            </div>

                            <div class="col-xs-12 ">
                                <textarea class="form-control" rows="5" id="observacion" name="observacion"></textarea>
                            </div>        
                            <br>
                            <div class="col-xs-12">
                                <h3><strong>Alimentos</strong></h3>
                            </div>

                            <div class="col-xs-2">
                                <input type="checkbox" class="form-check-input" name="desayuno" id="desayuno" value="S">
                                <label class="form-check-label">DESAYUNO</label>
                            </div>

                            <div class="col-xs-2">
                                <input type="checkbox" class="form-check-input" name="refrigerio" id="refrigerio" value="S">
                                <label class="form-check-label">REFRIGERIO</label>
                            </div>

                            <div class="col-xs-2">
                                <input type="checkbox" class="form-check-input" name="almuerzo" id="almuerzo" value="S">
                                <label class="form-check-label">ALMUERZO</label>
                            </div>

                            <div class="col-xs-2">
                                <input type="checkbox" class="form-check-input" name="cena" id="cena" value="S">
                                <label class="form-check-label">CENA</label>
                            </div>

                            <div class="col-xs-2">
                                <label>CANTIDAD</label>
                                <input type="number" class="input-sm" id="cantidad_alimentos" name="cantidad_alimentos">
                            </div>  
           
                        </div>
                    </div>
                </div>                             
            </div>
            <div class="modal-footer">
                    <button type="submit" id='idGuardar' name="idGuardar" class="btn btn-success load">Enviar</button>
                    <button id='idEliminar' name="idEliminar" class="btn btn-danger load">Eliminar</button>

                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

   <input type="hidden" id="codigo" name="codigo" value="<?php echo $_SESSION["usu_codigo"] ?>">   

</div>
<style>
          . {
               position: relative;
               display: inline-block;
               }
            
               visibility: hidden;
               width: 250px;
               background-color: black;
               color: #fff;
               text-align: center;
               border-radius: 6px;
               padding: 5px 0;
               position: absolute;
               z-index: 1;
               top: 100%;
               left: 40%;
               margin-left: -60px;
            }
            
              content: "";
              position: absolute;
              bottom: 100%;
              left: 50%;
              margin-left: -5px;
              border-width: 5px;
              border-style: solid;
              border-color: transparent transparent black transparent;
            }
            
              visibility: visible;
            }
    </style>
/* <!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a--> */

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>