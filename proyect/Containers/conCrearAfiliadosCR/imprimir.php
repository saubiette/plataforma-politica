<?php
header('Content-Type: text/html; charset=UTF-8');

	require '../../generalesPHP/cerrarSesion.php';
	require '../../generalesPHP/fpdf/fpdf.php';
	require_once "../../../php/conexion.php";
	
	$conexion = new clsConexion();

	$id = $_GET["id"];	
		
	//CONSULTA A LA BD 
	$sql =	"SELECT * , 
					municipio_exp.mun_nombre as mun_nombre_exp, departamentos_exp.dep_nombre as dep_nombre_exp, 
					municipio_res.mun_nombre as mun_nombre_res, departamentos_res.dep_nombre as dep_nombre_res, 
					municipios.mun_nombre as mun_nombre, departamentos.dep_nombre as dep_nombre
		     FROM	personas INNER JOIN
					municipios ON personas.mun_codigo = municipios.mun_codigo INNER JOIN
					departamentos ON municipios.dep_codigo = departamentos.dep_codigo INNER JOIN
					municipios as municipio_exp ON personas.mun_codigo_exp = municipio_exp.mun_codigo INNER JOIN
					departamentos as departamentos_exp ON municipio_exp.dep_codigo = departamentos_exp.dep_codigo INNER JOIN
					municipios as municipio_res ON personas.mun_codigo_res = municipio_res.mun_codigo INNER JOIN
					departamentos as departamentos_res ON municipio_res.dep_codigo = departamentos_res.dep_codigo INNER JOIN
					paises ON personas.pai_codigo = paises.pai_codigo 	
			 WHERE  per_codigo = '$id'";
	
	$persona = $conexion->prepare($sql);
	$persona->execute();		
	$personaArray = $persona->fetchAll();
	
	class PDF extends FPDF {
		//Cabecera de p�gina
		function Header() {
			//Logo
			//$this->Image('../images/logocontrato.png',20,8,40);
			//Arial bold 15
			$this->SetFont('Arial','B',15);
			//Movernos a la derecha
			//$this->Cell(10);
			//T�tulo
			$this->Cell(0,0,'',0,0,'C');
			//Salto de l�nea			
		}

		function footer() {			
			$this->SetFont('Arial','B',11);	
			$this->Ln(4.95);		
			$this->Cell(3.3,0.5,'',0,0,'C');

			$conexion = new clsConexion();

			//CONSULTA A LA BD 
			$sql =	"SELECT per_credencial_numero FROM	personas WHERE  per_codigo = '".$_GET["id"]."'";
			
			$per = $conexion->prepare($sql);
			$per->execute();		
			$perArray = $per->fetchAll();

			$credenciales = explode("-",$perArray[0]['per_credencial_numero']);

			$this->Cell(3.4,0.5,(isset($credenciales[0]))?$credenciales[0]:"",0,0,'R');
			$this->Cell(0.6,0.5,'',0,0,'C');
			$this->Cell(3.5,0.5,(isset($credenciales[1]))?$credenciales[1]:"",0,0,'L');						
			$this->Cell(5.2,0.5,'PASTO',0,0,'C');						
			$this->Cell(2.9,0.5, utf8_decode('NARIÑO'),0,0,'C');						
		}
	}

	ob_end_clean();
	ob_start();	

	$pdf=new PDF('L','cm',array(27.9, 21.6));//tamaño carta
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(1,0,0,0);

	$pdf->SetFont('Arial','B',11);
	$pdf->Image('SAD.jpg','0','0','21.6','27.9','JPG');

	//AFILIACION / DESAFILIACION
	$pdf->Ln(2.53);	
	$pdf->Cell(6.4,0.5,"" ,0,0,'C');
	$pdf->Cell(1,0.5,"X" ,0,0,'C');	
	$pdf->Cell(4,0.5,"" ,0,0,'C');
	$pdf->Cell(1,0.5,"" ,0,0,'C');	

	//FECHA
	$datetime = new DateTime("America/Bogota");
	$dia = $datetime->format('d');
	$mes = $datetime->format('m');
	$anno = $datetime->format('Y');

	$pdf->Cell(3.4,0.5,"" ,0,0,'C');
	$pdf->Cell(1,0.5,$dia ,0,0,'C');
	$pdf->Cell(1,0.5,$mes ,0,0,'C');
	$pdf->Cell(1.5,0.5,$anno ,0,0,'C');

	//TIPO INDENTIFICO
	$pdf->Ln(1);
	$pdf->Cell(7,0.5,"" ,0,0,'C');
	$pdf->Cell(1,0.5,($personaArray[0]['tdo_codigo']=="1")?"X":'',0,0,'C');	
	$pdf->Cell(1.8,0.5,"" ,0,0,'C');
	$pdf->Cell(1,0.5,($personaArray[0]['tdo_codigo']=="2")?"X":'',0,0,'C');	
	$pdf->Cell(1.8,0.5,"" ,0,0,'C');
	$pdf->Cell(1.4,0.5,($personaArray[0]['tdo_codigo']=="4")?"X":'',0,0,'C');	

	//NUMERO IDENTIFICACION
	$pdf->Ln(1);
	$pdf->Cell(6.6,0.5,"" ,0,0,'C');

	$identificacionLetras = $personaArray[0]['per_identificacion'];
	$identificacionLetras = str_pad($identificacionLetras, 13, " ", STR_PAD_LEFT);

	$pdf->Cell(1,0.5,$identificacionLetras[0] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[1] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[2] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[3] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[4] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[5] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[6] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[7] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[8] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[9] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[10] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[11] ,0,0,'C');	
	$pdf->Cell(1,0.5,$identificacionLetras[12] ,0,0,'C');		

	//FECHA EXPEDICION CEDULA
	$pdf->Ln(0.9);
	$pdf->Cell(6.6,0.5,"" ,0,0,'C');

	$strFechaExpedicion = "";
	if($personaArray[0]['per_fecha_expedicion'] != ""){
		$fechaExpedicion = new DateTime($personaArray[0]['per_fecha_expedicion']);	
		$strFechaExpedicion = $fechaExpedicion->format('d-m-Y');
	}
	
	$pdf->Cell(13,0.5, $strFechaExpedicion ,0,0,'C');

	//LUGAR EXPEDICION CEDULA
	$pdf->Ln(0.9);
	$pdf->Cell(6.6,0.5,"" ,0,0,'C');
	$pdf->Cell(13,0.5, utf8_decode($personaArray[0]['dep_nombre_exp']." - ".$personaArray[0]['mun_nombre_exp']) ,0,0,'C');

	//PRIMER NOMBRE
	$nombres = explode(" ",utf8_decode($personaArray[0]['per_nombre']));

	$pdf->Ln(0.9);
	$pdf->Cell(4.6,0.5,"" ,0,0,'C');
	$pdf->Cell(15,0.5, $nombres[0] ,0,0,'C');

	//SEGUNDO NOMBRE
	$pdf->Ln(1);
	$pdf->Cell(5,0.5,"" ,0,0,'C');
	$pdf->Cell(14.5,0.5, (isset($nombres[1]))?$nombres[1]:"" ,0,0,'C');

	//PRIMER APELLIDO
	$apellidos = explode(" ",utf8_decode($personaArray[0]['per_apellido']));

	$pdf->Ln(0.9);
	$pdf->Cell(4.6,0.5,"" ,0,0,'C');
	$pdf->Cell(15,0.5, $apellidos[0] ,0,0,'C');

	//SEGUNDO APELLIDO
	$pdf->Ln(1);
	$pdf->Cell(5,0.5,"" ,0,0,'C');
	$pdf->Cell(14.5,0.5, (isset($apellidos[1]))?$apellidos[1]:"" ,0,0,'C');

	//FECHA NACIMIENTO
	$pdf->Ln(0.9);
	$pdf->Cell(5.7,0.5,"" ,0,0,'C');

	$strFNDia = "";
	$strFNMes = "";
	$strFNAnno = "";

	if($personaArray[0]['per_fecha_nacimiento'] != ""){
		$fechaExpedicion = new DateTime($personaArray[0]['per_fecha_nacimiento']);	
		$strFNDia = $fechaExpedicion->format('d');
		$strFNMes = $fechaExpedicion->format('m');
		$strFNAnno = $fechaExpedicion->format('Y');
	}
	
	$pdf->Cell(5.1,0.5, $strFNDia ,0,0,'C');
	$pdf->Cell(4.1,0.5, $strFNMes ,0,0,'C');
	$pdf->Cell(4.6,0.5, $strFNAnno ,0,0,'C');

	//SEXO
	$pdf->Ln(0.9);
	$pdf->Cell(2.8,0.5,"" ,0,0,'C');
	$pdf->Cell(3.1,0.5, utf8_decode(($personaArray[0]['per_genero']=="F")?"X":"") ,0,0,'C');
	$pdf->Cell(3.2,0.5, utf8_decode(($personaArray[0]['per_genero']=="M")?"X":"") ,0,0,'C');

	//DEPARTAMENTO
	$pdf->Cell(3.5,0.5,"" ,0,0,'C');
	$pdf->Cell(7,0.5, utf8_decode($personaArray[0]['dep_nombre']) ,0,0,'C');

	//MUNICIPIO
	$pdf->Ln(0.9);
	$pdf->Cell(3.2,0.5,"" ,0,0,'C');
	$pdf->Cell(5.9,0.5, utf8_decode($personaArray[0]['mun_nombre']) ,0,0,'C');

	//PAIS RESIDENCIA
	$pdf->Cell(4,0.5,"" ,0,0,'C');
	$pdf->Cell(6.5,0.5, utf8_decode($personaArray[0]['pai_nombre']) ,0,0,'C');

	//DEPARTAMENTO RESIDENCIA
	$pdf->Ln(0.9);
	$pdf->Cell(7.3,0.5,"" ,0,0,'C');
	$pdf->Cell(12.2,0.5, utf8_decode($personaArray[0]['dep_nombre_res']) ,0,0,'C');

	//MUNICIPIO RESIDENCIA
	$pdf->Ln(1);
	$pdf->Cell(6.3,0.5,"" ,0,0,'C');
	$pdf->Cell(13.2,0.5, utf8_decode($personaArray[0]['mun_nombre_res']) ,0,0,'C');

	//DIRECCION RESIDENCIA
	$pdf->Ln(1);
	$pdf->Cell(6.3,0.5,"" ,0,0,'C');

	$strDireccion = "";

	switch ($personaArray[0]['per_tipo_direccion']) {
		case 'CA': $strDireccion = "Calle "; break;
		case 'CR': $strDireccion = "Carrera "; break;
		case 'TR': $strDireccion = "Transversal "; break;
		case 'DI': $strDireccion = "Diagonal "; break;
		case 'AV': $strDireccion = "Avenida "; break;		
		default:  $strDireccion = "";	break;
	}

	$strDireccion .= $personaArray[0]['per_direccion_1']." # ". $personaArray[0]['per_direccion_2']." - ". $personaArray[0]['per_direccion_3']." ".$personaArray[0]['per_complemento_direccion']." Barrio: ".$personaArray[0]['per_barrio'];

	$pdf->Cell(13.2,0.5, utf8_decode($strDireccion) ,0,0,'L');

	//TELEFONO FIJO 1
	$telefonos = explode(",",utf8_decode($personaArray[0]['per_telefono_fijo']));

	$pdf->Ln(0.9);
	$pdf->Cell(4.7,0.5,"" ,0,0,'C');
	$pdf->Cell(5.3,0.5, (isset($telefonos[0]))?$telefonos[0]:"" ,0,0,'C');

	//TELEFONO FIJO 2
	$pdf->Cell(4,0.5,"" ,0,0,'C');
	$pdf->Cell(5.5,0.5, (isset($telefonos[1]))?$telefonos[1]:"" ,0,0,'C');

	//TELEFONO CELULAR 1
	$celulares = explode(",",utf8_decode($personaArray[0]['per_celular']));

	$pdf->Ln(1);
	$pdf->Cell(4.7,0.5,"" ,0,0,'C');
	$pdf->Cell(5.3,0.5, (isset($celulares[0]))?$celulares[0]:"" ,0,0,'C');

	//TELEFONO CELULAR 2
	$pdf->Cell(4,0.5,"" ,0,0,'C');
	$pdf->Cell(5.5,0.5, (isset($celulares[1]))?$celulares[1]:"" ,0,0,'C');

	//CORREO ELECTRONICO 1
	$pdf->Ln(0.9);
	$pdf->Cell(6.3,0.5,"" ,0,0,'C');
	$pdf->Cell(13.2,0.5, utf8_decode($personaArray[0]['per_email']) ,0,0,'C');

	//CORREO ELECTRONICO 1
	$pdf->Ln(1);
	$pdf->Cell(6.3,0.5,"" ,0,0,'C');
	$pdf->Cell(13.2,0.5, utf8_decode("----") ,0,0,'C');

	//RECOMENDADO POR
	$pdf->Ln(0.9);
	$pdf->Cell(5.3,0.5,"" ,0,0,'C');
	$pdf->Cell(14.2,0.5, utf8_decode($personaArray[0]['per_recomendado']) ,0,0,'C');	

	$pdf->Output();
	ob_end_flush(); 
?>