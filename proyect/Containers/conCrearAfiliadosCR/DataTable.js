$(document).ready(function() {
  rutaContenedorActual = "./Containers/" + rutaContenedorActual;
  var exp = (r == "S")?true:false;

  var table = $("#dtGeneric").DataTable({
    dom: "Bfrtip",
    ajax: {
      url: rutaContenedorActual + "/arrays.php",
      dataSrc: "" // obligatorio  para que cargue
    },
    order: [[2, "asc"]],
    columns: [
      { data: "per_codigo", width: "2%" },
      { data: "per_identificacion", width: "6%" },
      { data: "per_nombre", width: "12%" },
      { data: "per_apellido", width: "12%" },
      { data: "per_celular" },
      { data: "dep_nombre_vive", width: "12%" },
      { data: "mun_nombre_vive", width: "12%" },
      { data: "dep_nombre_res", width: "12%" },
      { data: "mun_nombre_res", width: "12%" },
      { data: "fun_nombre", width: "8%" },
      {
        data: "nota",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      },
      {
        data: "afil",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      },
      {
        data: "imp",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "mod",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      }, // , sClass: "alignRight" }
      {
        data: "eli",
        width: "1%",
        sClass: "alignCenter pagCero",
        bSortable: false
      } // , sClass: "alignRight" }
    ],
    columnDefs: [
      {
        targets: [0],
        visible: false,
        searchable: false
      }
    ],
    //buttons: ["pageLength", "copy", "csv", "excel", "pdf", "print"],
    buttons: [
      {
        extend: "collection",
        text: "Export",
        buttons: ["copy", "excel", "csv", "pdf", "print"],
        enabled: exp
      }
    ],
    lengthMenu: [
      [10, 25, 50, -1],
      ["10 filas", "25 filas", "50 filas", "Todas"]
    ],
    pageLength: 14,
    select: false
  });

  $(document).ready(function() {
    $("#dtGeneric")
      .removeClass("display")
      .addClass("table table-striped table-hover table-bordered");
  });

  // ->NUEVO PERSONA
  $("#nuevo").on("click", function() {
    var varJson = {};
    varJson.tp = "ACR";

    crearVM_JSON("vmPersonas", varJson);
    return false;
  });

  // ->ENVIO DE MENSAJES
  $("#enviarMsj").on("click", function() {
    var varJson = {};
    varJson.tp = "ACR";
    varJson.perTipo = "CR";

    crearVM_JSON("vmEnvioMsjPersonas", varJson);
    return false;
  });
});

// ->INICIO DATOS CLIENTES

function Eliminar(id) {
  swal(
    {
      title: "¿Seguro que deseas eliminar este Registro?",
      text: "No podrás deshacer este paso...",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Eliminar",
      closeOnConfirm: true
    },
    function() {
      $.post(
        "./Containers/vmPersonas/PHPCrud.php",
        { opc: "D", id: id, tp: "ACR" },
        function(data) {
          if (data == false) {
            swal(
              "No se puede eliminar",
              "Este registro esta enlazado con otros...",
              "warning"
            );
          } else if (data == "DN") {
            swal(
              "Necesita permisos para realizar esta accion.",
              "Contacte al administrador del sistema",
              "warning"
            );
          } else {
            $(".dataTable")
              .DataTable()
              .ajax.reload();
          }
        },
        "json"
      );
    }
  );

  return false;
}

function Desafiliarce(id) {
  swal(
    {
      title: "Información",
      text: "¿Desea Desafiliar de CR a este Afiliado?",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Desafiliar",
      closeOnConfirm: true
    },
    function() {
      $.post(
        "./Containers/vmPersonas/PHPCrud.php",
        { opc: "DFI", id: id },
        function(data) {
          $(".dataTable")
            .DataTable()
            .ajax.reload();
        }
      );
    }
  );
}

function AgregarNotas(id) { 
  var varJson = {};
  varJson.tp = "ACR";
  varJson.perCodigo = id;  

  crearVM_JSON("vmAnotaciones", varJson);
  return false;
}


function Modificar(id) {
  var varJson = {};
  varJson.tp = "ACR";

  modVM_JSON("vmPersonas", id, varJson);
  return false;
}

function Imprimir(id) {
  window.open(rutaContenedorActual + "/imprimir.php?id=" + id);
  return false;
}

function Ver(id) {
  var varJson = {};
  varJson.ver = true;
  varJson.tp = "ACR";

  modVM_JSON("vmPersonas", id, varJson);
  return false;
}

// ->FIN DATOS CLIENTES
