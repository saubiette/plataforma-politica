$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  $('#cal_fecha').val(auxJSON.fecha)

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    var formData = new FormData(document.getElementById('idForm'))
    $opc = 'IU'
    formData.append('opc', $opc)

    // alert($('cal_fecha').val())
    // console.log(formData)
    // return false

    $.ajax({
      url: rutaCarpetaActual + '/PHPCrud.php',
      type: 'POST',
      data: formData,
      dataType: 'html',
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function (data) {
        if (data == 'no') {
          swal('La hora que intenta establecer esta Bloqueada', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
          return false
        }
        $('#calendar').fullCalendar('refetchEvents')
        $('#idCerrar').click()
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idEliminar').on('click', function () {
    swal({
      title: '¿Seguro que deseas eliminar este Registro?',
      text: 'No podrás deshacer este paso...',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar',
    closeOnConfirm: true },
      function () {
        $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'D', id: $('#id').val() }, function (data) {
          if (data == false) {
            swal('No se puede eliminar', 'Este registro esta enlazado con otros...', 'warning')
          }else if (data == 'DN') {
            swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
          }else {
            $('#calendar').fullCalendar('refetchEvents')
            $('#idCerrar').click()
          }
        }, 'json')
      })
    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  $('#idEliminar').hide();

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val() }, function (data) {
      if (data == 'DN') {
        swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning')
        $('#idCerrar').click()
        return false
      }

      var info = data.cal_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#cal_fecha').val(data.cal_fecha)
      $('#cal_hora_inicio').val(data.cal_hora_inicio)
      $('#cal_hora_fin').val(data.cal_hora_fin)
      $('#cal_titulo').val(data.cal_titulo)
      $('#cal_comentario').val(data.cal_comentario)
      $('#dep_codigo').val(data.dep_codigo)
      $('#dep_codigo').change()
      $('#mun_codigo').val(data.mun_codigo_Per)

      $('#cal_identificacion').val(data.cal_identificacion)
      $('#cal_nombre').val(data.cal_nombre)
      $('#cal_telefono').val(data.cal_telefono)

      if (!(auxJSON.ver == undefined || data.usu_codigo_registra == $('#codigo').val())){
        $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
        $('#idGuardar').hide()
      }

      if (auxJSON.ver == undefined){
        $('#idEliminar').show()
      }
    }, 'json')
  }

  $('#dep_codigo').on('change', function () {
    $('#mun_codigo').val('')

    $.ajaxSetup({ async: false })

    $.post(rutaCarpetaActual + '/municipios.php', { dep_codigo: $('#dep_codigo').val() })
      .done(function (data) {
        $('#mun_codigo').html(data)
      })

    $.ajaxSetup({ async: true })
  })

  $('#dep_codigo').change()

  // $('.formatoMiles').numberFM(true, 2, ',', '.')

})
