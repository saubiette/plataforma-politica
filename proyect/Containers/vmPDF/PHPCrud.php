<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');

require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();     
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    $conexion->beginTransaction();   

    $tabla = 'calendario';
    $ideCampo = "cal_codigo";
    $ideValor = $_POST['id'];

    $agenda_id = MaxCampo($tabla,$ideCampo,$conexion);

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);


    $opc = $_POST['opc'];

    if(in_array($opc, array("IU","D","S"))){

        $aux = "";

        switch ($opc) {
            case 'IU': $aux= ($ideValor != "")?"U":"C"; break;
            case 'D':  $aux= "D"; break;
            case 'S':  $aux= "R"; break;
        }

        $strCampo = fncPermiso("CAL", $aux , new clsConexion());             
            
        if(!$strCampo){
            $conexion->rollBack();
            echo json_encode('DN');            
            exit();              
        }
    }    

    //session_start();   
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("cal_fecha",$_POST['cal_fecha']);              
            $SQLcomando->set_AddCampo("cal_hora_inicio",$_POST['cal_hora_inicio']); 
            $SQLcomando->set_AddCampo("cal_hora_fin",$_POST['cal_hora_fin']); 
            $SQLcomando->set_AddCampo("cal_titulo",$_POST['cal_titulo']);
            $SQLcomando->set_AddCampo("cal_direccion",$_POST['cal_direccion']); 
            $SQLcomando->set_AddCampo("mun_codigo",$_POST['mun_codigo']); 

            $SQLcomando->set_AddCampo("cal_identificacion",$_POST['cal_identificacion']); 
            $SQLcomando->set_AddCampo("cal_nombre",$_POST['cal_nombre']); 
            $SQLcomando->set_AddCampo("cal_telefono",$_POST['cal_telefono']); 
            
   
            if($_POST['id'] != ""){                            

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                      

            }else{             

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);              
                                
                //session_start();
                $SQLcomando->set_AddCampo("cal_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);

                

                             
              
                $SQLcomando->set_tipo_comando("INSERT");
                
            }    

                    

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute();

            

            echo true;            
            break;   
            
        case 'D':                  
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT *  , $tabla.mun_codigo as mun_codigo_Per
                        FROM $tabla 
                        INNER JOIN municipios ON $tabla.mun_codigo = municipios.mun_codigo 
                        INNER JOIN usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

    }

    $conexion->commit();   

} 
catch (Exception $e) {
    
    if($e->errorInfo[0] == '23000')
        echo "fk";    
    else
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";

    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}



?>