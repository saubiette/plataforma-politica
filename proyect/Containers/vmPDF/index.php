<?php 
session_start();
    require '../../../php/conexion.php';     
    require "../../generalesPHP/funciones.php";
    
    if(isset($_POST['id'])):
        //$strCampo = fncPermiso("CAL","U", new clsConexion()); 
        $strCampo = "-";
    else:
         $strCampo = fncPermiso("CAL","C", new clsConexion()); 
    endif;
    
    if(!$strCampo){
        echo "<script>swal('Necesita permisos para realizar esta accion.', 'Contacte al administrador del sistema', 'warning');</script>";   
        exit();
    }  
?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form  method="post" action="Containers/conCrearCalendario/pdf.php">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(!isset($strJSON->ver)): ?>

                    <?php if(isset($_POST['id'])):?>
                        <h2><b>Modificar Bloqueo de Agenda</b></h2> 
                    <?php else:?>
                        <h2><b>Buscador</b></h2>
                    <?php endif;?>   
                
                <?php else:?>  

                    <h2><b>Agenda</b></h2>
                    (Fecha: <b><span id='fecha_registro'></span></b> -
                     Registro: <b><span id='usu_nombre_registra'></span></b>)

                <?php endif?>
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                               
                          

                <div class="form-group">
                    <label class="col-lg-6 paddin-no-left" for="pdf_inicio">Desde:</label>
                    <label class="col-lg-6 paddin-no-left" for="pdf_fin">Hasta:</label>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="date" class="form-control input-sm required" required id="pdf_inicio" name="pdf_inicio" placeholder="Introduce un Tiempo Inicial">
                            <label for="pdf_inicio" class="label label-danger error" generated="true"></label>                     
                        </div>
                        <div class="col-lg-6 ">
                            <input type="date" class="form-control input-sm required" required  id="pdf_fin" name="pdf_fin" placeholder="Introduce un Tiempo Final">
                            <label for="pdf_fin" class="label label-danger error" generated="true"></label>                     
                        </div>
                    </div>                   
                </div>                         
            </div>
            <div class="modal-footer">
                    <button type="submit" id='idGuardar' name="idGuardar" class="btn btn-info load">Buscar</button>
                    <button id='idEliminar' name="idEliminar" class="btn btn-danger load">Eliminar</button>

                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>
         
<!-- <input type="hidden" value="#F30A0A" name="color" id="color"> -->
   	</div>
     
   </div>

   <input type="hidden" id="codigo" name="codigo" value="<?php echo $_SESSION["usu_codigo"] ?>">

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>