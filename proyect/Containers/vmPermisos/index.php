<?php require '../../../php/conexion.php'; ?>

<div id="idModal" class="modal fade">

   <div class="modal-dialog">  

      <div class="modal-content">

           <form role="form" id="idForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                ×
                </button>

                 <?php $strJSON = json_decode($_POST['objJSON']); ?>

                <?php if(isset($_POST['id'])):?>
                    <h3>Modificar Permiso</h3> 
                <?php else:?>
                    <h3>Registrar Permiso</h3>
                <?php endif?>             
                
            </div>
            <div class="modal-body">
            
                <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">                                 
                                                                 
                <div class="form-group">
                    <label for="usu_codigo">(*)Login/Usuario:</label>                    
                    <select class="form-control input-sm opcion" id="usu_codigo" name="usu_codigo" required>
                       
                         <?php
                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM usuario WHERE usu_nivel_acceso != 'S'");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['usu_codigo']."'>".$fila['usu_login']." -> ".$fila['usu_nombre']."</option>";
                            }           
                         ?> 

                    </select>                     
                </div>                
                
                <div class="form-group">
                    <label for="pmi_opcion">Módulo:</label>                    
                    <select class="form-control input-sm opcion" id="pmi_opcion" name="pmi_opcion" required>                       
                        <option value='CAL'>Calendario</option>
                        <option value='RVO'>Reg. Votantes</option>
                        <option value='RVL'>Reg. Votantes(Lider)</option>
                        <option value='LIV'>Lideres - Votantes</option>
                        <option value='COG'>Coordinadores y Gestionadores</option>
                        <option value='ADF'>Administración de Firmas</option>
                        <option value='ACR'>Afiliados CR</option>
                        <option value='REU'>Reuniones</option>
                        <option value='OTR'>Otro</option>
                    </select>                     
                </div>                 

                <div class="form-group">
                    <label for="pmi_select">Ingresar:</label>                    
                    <select class="form-control input-sm" id="pmi_select" name="pmi_select" required>
                        <option value='S'>Si</option>
                        <option value='N'>No</option>
                    </select>                     
                </div> 

                <div class="form-group">
                    <label for="pmi_insert">Crear:</label>                    
                    <select class="form-control input-sm permisos" id="pmi_insert" name="pmi_insert" required>
                        <option value='S'>Si</option>
                        <option value='N'>No</option>
                    </select>                     
                </div> 

                <div class="form-group">
                    <label for="pmi_update">Modificar:</label>                    
                    <select class="form-control input-sm permisos" id="pmi_update" name="pmi_update" required>
                        <option value='S'>Si</option>
                        <option value='N'>No</option>
                    </select>                     
                </div>  

                 <div class="form-group">
                    <label for="pmi_delete">Eliminar:</label>                    
                    <select class="form-control input-sm permisos" id="pmi_delete" name="pmi_delete" required>
                        <option value='S'>Si</option>
                        <option value='N'>No</option>
                    </select>                     
                </div>              
                                         
            </div>
            <div class="modal-footer">
                <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>            
                <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
            </div>
         </form>

   	</div>
     
   </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script> var auxJSON = (<?php echo $_POST['objJSON']; ?>); console.log(auxJSON); </script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>