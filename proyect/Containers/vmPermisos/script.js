$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    if (!$('#idForm').valid()) {return false}

    $('.load').LoadingOverlay('show')

    $.post(rutaCarpetaActual + '/PHPCrud.php', $('#idForm').serialize() + '&opc=IU')
      .done(function (data) {
        if (data == false) {
          swal('El Login/Usuario y Módulo ya se encuentran registrados.', 'Revisa e intenta nuevamente', 'warning')
          $('.load').LoadingOverlay('hide')
        }else {
          $('.dataTable').DataTable().ajax.reload()
          $('#idCerrar').click()
        }
      })
      .fail(function (xhr, textStatus, errorThrown) {
        alert(xhr.responseText)
      })

    return false
  })

  $('#idCerrar').on('click', function () {eliminarVM();})

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', { opc: 'S', id: $('#id').val(), id_pro: $('#id_pro').val() }, function (data) {
      $('#usu_codigo').val(data.usu_codigo)
      $('#usu_codigo').prop('disabled', true)

      $('#pmi_opcion').val(data.pmi_opcion)
      $('#pmi_opcion').prop('disabled', true)

      $('#pmi_select').val(data.pmi_select)
      $('#pmi_insert').val(data.pmi_insert)
      $('#pmi_update').val(data.pmi_update)
      $('#pmi_delete').val(data.pmi_delete)
    }, 'json')
  }

  $('#pmi_select').on('change', function () {
    if ($('#pmi_select').val() == 'N') {
      $('#pmi_insert').val('N')
      $('#pmi_update').val('N')
      $('#pmi_delete').val('N')
    }
  })

  $('.permisos').on('change', function () {
    if ($('#pmi_insert').val() == 'S' || $('#pmi_update').val() == 'S' || $('#pmi_delete').val() == 'S') {
      $('#pmi_select').val('S')
    }
  })
})
