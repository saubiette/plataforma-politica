<?php require '../../../php/conexion.php'; ?>

<div id="idModal" class="modal fade">

    <div class="modal-dialog">

        <div class="modal-content">

            <form role="form" id="idForm">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <?php $strJSON = json_decode($_POST['objJSON']); ?>
                    <h2><b>Reportes Lideres</b></h2>

                </div>
                <div class="modal-body">

                    <input type="hidden" id="id" name="id" value="<?php if(isset($_POST['id'])){echo $_POST['id'];}?>">

                    <div class="form-group">

                        <label for="chkCooCodigo"><input id="chkCooCodigo" type="checkbox" value=""> Coordinadores</label>
                        <select class="form-control input-sm" id="coo_codigo" name="coo_codigo" disabled>
                         
                         <?php                           

                            $con = new clsConexion();
                            $res = $con->prepare("SELECT * FROM coordinadores");
                            $res->execute();
                            
                            while($fila = $res->fetch()){
                                echo "<option value='".$fila['coo_codigo']."'>".$fila['coo_nombre']." ".$fila['coo_apellido']."</option>";
                            }           
                         ?>

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="chkGesCodigo"><input id="chkGesCodigo" type="checkbox" value=""> Gestionador</label>
                        <select class="form-control input-sm" id="ges_codigo" name="ges_codigo" disabled></select>
                    </div>

                </div>
                <div class="modal-footer">
                    <?php if(!isset($strJSON->ver)): ?>
                    <button type="submit" id='idGuardar' class="btn btn-success load">Enviar</button>

                    <?php endif?>
                    <a href="#" id="idCerrar" data-dismiss="modal" class="btn">Cerrar</a>
                </div>
            </form>

        </div>

    </div>

</div>

<!--a data-toggle="modal" id='btnIdEstudiante' href="#idEstudiante" class="btn btn-primary btn-large" style="display:none"></a-->

<script>
    var auxJSON = (<?php echo $_POST['objJSON']; ?>);
    console.log(auxJSON);
</script>
<script src="./Containers/<?php echo $strJSON->rutaVM; ?>/script.js"></script>