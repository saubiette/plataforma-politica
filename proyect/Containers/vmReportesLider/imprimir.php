<?php
header('Content-Type: text/html; charset=UTF-8');

	require '../../generalesPHP/cerrarSesion.php';
	require '../../generalesPHP/fpdf/fpdf.php';
	require_once "../../../php/conexion.php";
	
	$conexion = new clsConexion();
	
	class PDF extends FPDF {
		//Cabecera de p�gina
		function Header() {
			//Logo
			//$this->Image('../images/logocontrato.png',20,8,40);
			//Arial bold 15
			$this->SetFont('Arial','B',15);
			//Movernos a la derecha
			$this->Cell(80);
			//T�tulo
			$this->Cell(30,10,'',0,0,'C');
			//Salto de l�nea
			$this->Ln(3);
		}
	}

	ob_end_clean();
	ob_start();	
		
	$cooCodigo = (isset($_GET["cc"]))? $_GET["cc"]: "";	
	$gesCodigo = (isset($_GET["gc"]))? $_GET["gc"]: "";		
		
	//AUTOMOTORES
	$sql =	"SELECT *
			 FROM	coordinadores				
			 WHERE  coo_codigo LIKE '%$cooCodigo%'";
	
	$cordinadores = $conexion->prepare($sql);
	$cordinadores->execute();	
		
	while($coo = $cordinadores->fetch()){$coordinadoresArray[] = $coo;}	
	
	$pdf=new PDF('L','mm',array(300, 535));
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetMargins(20,0,10);

	$pdf->SetFont('Arial','B',11);
	$pdf->Ln(15);
	$pdf->Cell(500,6, utf8_decode('REPORTE DE LIDERES CONSOLIDADO'),1,0,'C');

	$pdf->SetFont('Arial','B',11);
	$pdf->Ln(10);	

	$TN_totForm = 0;
	$TN_totFormEnt = 0;
	$TN_totFormFal = 0;
	$TN_totFir = 0;
	$TN_totFirCon = 0;
	$TN_totFirErr = 0;

	$TN_SPtotValor = 0;
	$TN_PtotValor = 0;
	$TN_totValor = 0;

	foreach($coordinadoresArray as $clave => $valor){		

		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(170,5, "LIDERES->",1,0,'L');

		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(50,5, utf8_decode("IDENTIFICACIÓN:"),1,0,'L');

		$pdf->SetFont('Arial','',10);
		$pdf->Cell(45,5, utf8_decode(strtoupper($valor['coo_identificacion'])),1,0,'R');

		$pdf->SetFont('Arial','B',11);				
		$pdf->Cell(40,5, utf8_decode('NOMBRE:'),1,0,'L');	

		$pdf->SetFont('Arial','',10);	
		$pdf->Cell(90,5, utf8_decode(strtoupper($valor['coo_nombre']." ".$valor['coo_apellido'])),1,0,'L');

		$pdf->SetFont('Arial','B',11);				
		$pdf->Cell(75,5, utf8_decode('TIPO COORDINADOR:'),1,0,'L');	

		$pdf->SetFont('Arial','',10);	

		$tipCoordinador = "OTRO";

		switch ($valor['coo_tipo_coordinador']) {
			case 'RU': $tipCoordinador = "RURAL"; break;
			case 'UR': $tipCoordinador = "URBANO"; break;
			case 'MU': $tipCoordinador = "MUNICIPAL"; break;		
		}

		$pdf->Cell(30,5, utf8_decode(strtoupper($tipCoordinador)),1,0,'L');
		$pdf->Ln(10);

		$sqlTipDoc = "SELECT   *
					  FROM gestionadores
					  WHERE coo_codigo = '".$valor['coo_codigo']."' and ges_codigo LIKE '%$gesCodigo%' ";
		
		$gestionador = $conexion->prepare($sqlTipDoc);
		$gestionador->execute();	

		$gestionadorArray = null;
			
		while($ges = $gestionador->fetch()){$gestionadorArray[] = $ges;}	

		if(count($gestionadorArray) == 0) continue;

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(5,5, "",0,0,'C');		
		$pdf->Cell(165,5, "> VOTANTE/S",1,0,'C');		
		$pdf->Ln(5);

		$T_totForm = 0;
		$T_totFormEnt = 0;
		$T_totFormFal = 0;
		$T_totFir = 0;
		$T_totFirCon = 0;
		$T_totFirErr = 0;
		
		$T_SPtotValor = 0;
		$T_PtotValor = 0;
		$T_totValor = 0;

		foreach($gestionadorArray as $clave => $valorTP){			

			$pdf->SetFont('Arial','B',11);
			$pdf->Cell(5,5, "",0,0,'C');
			$pdf->Cell(36,5, utf8_decode("IDENTIFICACION:"),1,0,'C');

			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5, utf8_decode(strtoupper($valorTP['ges_identificacion'])),1,0,'R');

			$pdf->SetFont('Arial','B',11);
			$pdf->Cell(24,5, utf8_decode("NOMBRE:"),1,0,'C');

			$pdf->SetFont('Arial','',10);
			//$pdf->SetFillColor(255,100,255); 			
			$pdf->Cell(80,5, utf8_decode(strtoupper($valorTP['ges_nombre']." ".$valorTP['ges_apellido'])),1,0,'L');			
			$pdf->Ln(5);
		}	

		$pdf->Ln(10);	
	}

	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(170,5, "",0,0,'C');	
	
	$pdf->Ln(5);

	/* --> FIN: RECORRIDO FACTORES */
		
	$pdf->Output();
	ob_end_flush(); 
?>