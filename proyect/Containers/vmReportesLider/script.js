$(document).ready(function () {
  rutaCarpetaActual = './Containers/' + auxJSON.rutaVM

  // ->INSERTA Y ACTUALIZA LOS DATOS
  $('#idForm').on('submit', function () {
    
    var cooCodigo = ($("#coo_codigo").prop("disabled"))?"":$("#coo_codigo").val();
    var gesCodigo = ($("#ges_codigo").prop("disabled"))?"":$("#ges_codigo").val();

    window.open(rutaCarpetaActual + "/imprimir.php?cc=" + cooCodigo + "&gc=" + gesCodigo, "Reporte de Firmas", "width=1000, height=800")   

    return false
  })

  $('#idCerrar').on('click', function () {
    eliminarVM();
  })

  $('#idModal').modal('show')

  // -> EXTRAE LOS DATOS PARA MODIFICARLOS
  if ($('#id').val() != '') {
    $.post(rutaCarpetaActual + '/PHPCrud.php', {
      opc: 'S',
      id: $('#id').val()
    }, function (data) {
      var info = data.for_fecha_registro.split('-')
      fechaFormateada = info[2] + '/' + info[1] + '/' + info[0]

      $('#fecha_registro').html(fechaFormateada)
      $('#usu_nombre_registra').html(data.usu_nombre)

      $('#for_fecha').val(data.for_fecha)
      $('#for_numero').val(data.for_numero)
      $('#for_comentario').val(data.for_comentario)
      $('#ges_codigo').val(data.ges_codigo)

      if (auxJSON.ver != undefined) $('#idForm input, #idForm textarea, #idForm select').attr('readonly', 'readonly')
    }, 'json')
  }

  //$('#ges_codigo').val(auxJSON.ges_codigo)
  // $('.formatoMiles').numberFM(true, 2, ',', '.')

  $('#coo_codigo').on('change', function () {
    $('#ges_codigo').val('')

    $.ajaxSetup({
      async: false
    })

    $.post(rutaCarpetaActual + '/Gestionadores.php', {
        coo_codigo: $('#coo_codigo').val()
      })
      .done(function (data) {
        $('#ges_codigo').html(data);
      })

    $.ajaxSetup({
      async: true
    })
  })

  $('#coo_codigo').change();

  $("#chkCooCodigo").on("change", function () {
    $("#coo_codigo").prop("disabled", !$(this).is(':checked'));
  });

  $("#chkGesCodigo").on("change", function () {
    $("#ges_codigo").prop("disabled", !$(this).is(':checked'));
  });

})