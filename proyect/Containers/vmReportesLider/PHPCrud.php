<?php
 session_start();
require "../../../php/conexion.php";
require "../../generalesPHP/SQLComando.php";
require "../../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();     
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       
    $conexion->beginTransaction();   

    $tabla = 'numero_formularios';
    $ideCampo = "for_codigo";
    $ideValor = $_POST['id'];

    $SQLcomando = new SQLComando();
    $SQLcomando->set_tabla($tabla);

    $opc = $_POST['opc'];   
    
    switch ($opc) {
        
        case 'IU':   

            $SQLcomando->set_AddCampo("for_fecha",$_POST['for_fecha']);              
            $SQLcomando->set_AddCampo("for_numero",$_POST['for_numero']); 
            $SQLcomando->set_AddCampo("for_comentario",$_POST['for_comentario']);           
            $SQLcomando->set_AddCampo("ges_codigo",$_POST['ges_codigo']);          
                 
            if($_POST['id'] != ""){                            

                $SQLcomando->set_where($ideCampo ." = '".$ideValor."'");
                $SQLcomando->set_tipo_comando("UPDATE");                                      

            }else{             

                $ideValor = MaxCampo($tabla,$ideCampo,$conexion);//te devuelve +1                
                $SQLcomando->set_AddCampo($ideCampo,$ideValor);              
                                
                session_start();
                $SQLcomando->set_AddCampo("for_fecha_registro","CURRENT_DATE",false);
                $SQLcomando->set_AddCampo("usu_codigo_registra",$_SESSION['usu_codigo']);                
              
                $SQLcomando->set_tipo_comando("INSERT");   

            }       

            $resp = $conexion->prepare($SQLcomando->get_sql());
            $resp->execute(); 

            echo true;            
            break;   
            
        case 'D':                      
                                    
            $strSQL = "DELETE FROM $tabla WHERE $ideCampo ='$ideValor'";
            $resp = $conexion->prepare($strSQL);
            $resp->execute();   
                    
            echo true;                    
            break;
            
        case 'S':

             $strSQL = "SELECT *  
                        FROM $tabla INNER JOIN 
                             usuario ON $tabla.usu_codigo_registra = usuario.usu_codigo
                        WHERE $ideCampo = '$ideValor'";

             $resp = $conexion->prepare($strSQL);
             $resp->execute();
            
             $resM = $resp->fetchAll();
             echo json_encode($resM[0]);            
             break;

    }

    $conexion->commit();   

} 
catch (Exception $e) {
    
    if($e->errorInfo[0] == '23000')
        echo "fk";    
    else
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";

    $conexion->rollBack();
} 
catch (PDOException  $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>