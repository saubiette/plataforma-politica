<?php
	//Propiedades del documento
	$objPHPExcel->getProperties()->setCreator("Campaña")
								 ->setLastModifiedBy("Campaña")
								 ->setTitle("Office 2010 XLSX")
								 ->setSubject("Office 2010 XLSX")
								 ->setDescription("Reporte Generado por Campaña.")
								 ->setKeywords("office 2010 openxml")
								 ->setCategory("Archivo con resultado satisfactorio");

	/*** estilos para presentar la informacion en la hoja de excel *******/
	$estiloTituloReporte = array(
		'font' => array('name' => 'Arial', 'bold' => true, 'italic' => false, 'strike' => false, 'size' => 16, 'color' => array('rgb'=> '4c4a4c') // tipo de letra
		),
		'fill' 	=> array(
			'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR, 'rotation' => 90, 
			'startcolor' => array('rgb' => 'dbed0d' ),'endcolor' => array('argb' => '8f9452')
		),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_NONE) // estilo de bordes
		), 
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER) //alineacion del texto
	);		

	$estiloTituloColumnas = array(
		'font' => array('name' => 'Arial', 'bold' => true, 'size' => 11, 'color' => array('rgb' => '4c4a4c')
		),
		'fill' 	=> array(
			'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR, 'rotation' => 90, 
			'startcolor' => array('rgb' => 'BFCC30' ),'endcolor' => array('argb' => 'BFCC30')
		),
		'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_NONE) // estilo de bordes
		), 
		'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER) //alineacion del texto
	);
	
	$estiloInformacion = array(
		'font' => array('name' => 'Arial', 'size' => 11, 'color' => array('rgb' => '000000')
		),
		'fill' 	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID
		),
		'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => '000'))
		)
	);	
	/*** fin estilos para presentar la informacion en la hoja de excel ***/
?>