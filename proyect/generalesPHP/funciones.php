<?php

function MaxCampo($tabla,$campo,$conexion){

    $str = "SELECT COALESCE(MAX($campo),0) + 1 AS Max FROM $tabla";
    $respuestaMax = $conexion->prepare($str);
    $respuestaMax->execute();
    $maxV = $respuestaMax->fetchAll();
    return $maxV[0][0];
}

function fncExiste($tabla,$campoDevolver,$strWhere,$conexion){
    
    $resp = $conexion->prepare("SELECT $campoDevolver FROM $tabla WHERE $strWhere ");
    $resp->execute();

    $campoRetornar = "";

    if($resp->rowCount() > 0){
        $valor = $resp->fetchAll();
        $campoRetornar = $valor[0][0];
    }

    return $campoRetornar;
    
}

function fncPermiso($strModulo, $strCrud, $conexion){
    
    if($_SESSION['usu_nivel_acceso'] == "S" || $_SESSION['usu_nivel_acceso'] == "A") {
        return true;
    }

    $strTipoCrud = "";

    switch ($strCrud) {
        case 'C': $strTipoCrud = "pmi_insert"; break;
        case 'R': $strTipoCrud = "pmi_select"; break;
        case 'U': $strTipoCrud = "pmi_update"; break;
        case 'D': $strTipoCrud = "pmi_delete"; break;        
        default: $strTipoCrud = ""; break;
    }   
    
    $resp = $conexion->prepare("SELECT pmi_codigo
                                FROM permisos 
                                WHERE pmi_opcion = '$strModulo' AND
                                      $strTipoCrud = 'S' AND
                                      usu_codigo = '".$_SESSION['usu_codigo']."' ");
    $resp->execute();

    $campoRetornar = false;

    if($resp->rowCount() > 0){
        $valor = $resp->fetchAll();
        //$campoRetornar = $valor[0][0];
        $campoRetornar = true;
    }

    return $campoRetornar;
    
}

function fncFechaNULL($strFecha){
    return ($strFecha == "")?'NULL':"'".$strFecha."'";
}

function fncTipoUsuario($tipoUsuario){

    $tipo = "";

    switch($tipoUsuario){
        case 'S': $tipo = "Super Usuario";
                  break;        
        case 'A': $tipo = "Asistente";
                  break;
        case 'L': $tipo = "Lider";
                  break;
        case 'E': $tipo = "Secretaria";
                  break;
        case 'F': $tipo = "Secretaria Firmas";
                  break;
        default: $tipo = "Usuario";
                  break;
    }

    return $tipo;

}

function DiaSemanaNombre($dia){

    $Nombredias = array( "1" => 'Lunes',
                         "2" => 'Martes',
                         "3" => 'Miercoles',
                         "4" => 'Jueves',
                         "5" =>'Viernes',
                         "6" =>'Sabado',
                         "7" =>'Domingo');
    
    return $Nombredias[$dia];
   
}

function blnPerCerrado($idePeriodo, $conexion){

    $res = false;

    $resp = $conexion->prepare("SELECT per_cerrado FROM periodos WHERE ide_periodo = $idePeriodo ");
    $resp->execute();
    $maxV = $resp->fetchAll();
    
    if($maxV[0][0] == "N"){ $res = true;}
    
    return $res;
}

function fncfechaServidor(){    
    echo $hoy = date("Y/m/d");
} 

function fncEdad($fecha){
    list($Y,$m,$d) = explode("-",$fecha);
    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
}
?>