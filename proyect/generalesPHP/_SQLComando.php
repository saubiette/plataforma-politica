<?php

class SQLComando{

    private $tabla;
    private $tipo_comando = "INSERT"; //UPDATE
    private $where = "";
    private $sql;
    private $campos = array();

    function set_tabla($tabla){
        $this->tabla = $tabla;
    }

    function set_tipo_comando($tipo_comando){
        $this->tipo_comando = $tipo_comando;
    }

    function set_where($where){
        $this->where = $where;
    }

    function set_AddCampo($campo, $valor){
        $this->campos[$campo] = $valor;
    }

    function itemsClear(){
        $this->campos = array();
    }

    function get_sql(){

        $this->sql = "";

        if($this->tipo_comando == "INSERT"){

            $this->sql = "INSERT INTO $this->tabla ";

            $resCampos = "(";
            $resValores = "(";

            foreach ($this->campos as $campo => $value){
                $resCampos .= $campo.",";
                $resValores .= "'".$value."',";                
            }

            $resCampos = substr($resCampos,0,-1).")";
            $resValores = substr($resValores,0,-1).")";

            $this->sql .= $resCampos." VALUES ".$resValores;

        }
        elseif($this->tipo_comando == "UPDATE"){
            
            $this->sql = "UPDATE $this->tabla SET ";

            $resCamposvAL = "";    
                  
            foreach ($this->campos as $campo => $value){
                $resCamposvAL .= $campo." = '".$value."',";                         
            }

            $resCamposvAL = substr($resCamposvAL,0,-1); 

            if($this->where != ""){
                $this->where = " WHERE $this->where";
            }           

            $this->sql .= $resCamposvAL.$this->where;

        }

        return $this->sql;

    }

}

/*
$sqlComando = new SQLCOmando();

$sqlComando->set_tabla("tablilla");
$sqlComando->set_tipo_comando("UPDATE");

$sqlComando->set_AddCampo("ide","1");
$sqlComando->set_AddCampo("nombre","jose");

$sqlComando->set_where("ide_usuario = '10'");

echo $sqlComando->get_sql();
*/

?>