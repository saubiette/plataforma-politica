<?php
require 'conexion.php';
$conexion = new clsConexion();
$res = new stdClass();

$login = trim($_POST['login']);
$contrasenna = trim($_POST['contrasenna']);
$con_codigo = trim($_POST['usu_codigo_depende']);
$recue = (isset($_POST['recuerdame']))?"true":"false";

$usuAdmin = $con_codigo;

	/*$sql = "SELECT * 
			FROM usuario INNER JOIN
				 municipios ON usuario.mun_codigo = municipios.mun_codigo
			WHERE UPPER(usu_login) = '".strtoupper($login)."' AND 
				  usu_password = '".hash("sha256",$contrasenna)."' AND 
				  usu_activo = 'S' AND usu_nivel_acceso = '$usuAdmin' ";*/
	
	$sql = "SELECT * 
			FROM usuario INNER JOIN
				 municipios ON usuario.mun_codigo = municipios.mun_codigo
			WHERE UPPER(usu_login) = :Login AND 
				  usu_password = :Contrasenna AND 
				  usu_activo = :UsuActivo AND usu_nivel_acceso = :NivelAcceso ";

//$res->sql = $sql;

try{
		
	$resultado = $conexion->prepare($sql);
	$resultado->execute(array('Login' => strtoupper($login),
							  'Contrasenna' => hash("sha256", $contrasenna), 
							  'UsuActivo' => 'S', 
							  'NivelAcceso' => $usuAdmin ));
	
	$res->entro = 'N';

	while($usuario = $resultado->fetch()){
		$res->entro = 'S';
			
		session_start();
			
		$_SESSION["usu_codigo"] = $usuario['usu_codigo'];
		$_SESSION["usu_login"] = $usuario['usu_login'];
		$_SESSION["usu_nombre"] = $usuario['usu_nombre'];
		//$_SESSION["nombre"] = $usuario['usu_nombre'];			
		$_SESSION["usu_nivel_acceso"] = $usuario['usu_nivel_acceso'];
		$_SESSION["usu_activo"] = utf8_decode($usuario['usu_activo']);					
		$_SESSION["ruta_img"] = utf8_decode($usuario['usu_ruta_imagen']);

		$_SESSION["dep_codigo"] = $usuario['dep_codigo'];
		$_SESSION["mun_codigo"] = $usuario['mun_codigo'];

		$_SESSION["recuerdame"] = $recue;
		$_SESSION["con_codigo"] = $con_codigo;			
		
	}	

}catch(Exception $e){
   $res->entro = 'Error: '. $e->getMessage();   
}
catch(PDOException $e){
	$res->entro = 'Error: '. $e->getMessage();   
}

$_SESSION['dep'] = json_encode($res);
echo json_encode($res);

?>
