<?php

require "../../php/conexion.php";
require "../generalesPHP/SQLComando.php";
require "../generalesPHP/funciones.php";

//->->-> MANEJO DE CASOS

try {

    $conexion = new clsConexion();      
    $resultado = blnPerCerrado($_POST['idePeriodo'], $conexion);
    echo $resultado;      

} 
catch (Exception $e) {
    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    $conexion->rollBack();
}

?>