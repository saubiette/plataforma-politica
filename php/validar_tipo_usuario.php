<?php
require "conexion.php";
$conexion = new clsConexion();

$res = new stdClass();

$res->existeGrupo = false;

$login = $_POST["login"];

$sql = "select *
        from  usuario 
        where usu_activo = 'S' AND usu_login = '$login' ";

try{

    $consulta = $conexion->prepare($sql);
    $consulta->execute();

    $numEmpresas = $consulta->rowCount();

    if($numEmpresas > 0){

        $res->existeGrupo = true;
        $select = "";

        foreach( $consulta as $grupo){

            if($grupo['usu_nivel_acceso'] == 'A'){
                $select .= "<option value= 'A'>Asistente</option>";
            }
            else if($grupo['usu_nivel_acceso'] == 'L'){
                $select .= "<option value= 'L'>Lider</option>";
            }   
            else if($grupo['usu_nivel_acceso'] == 'E'){
                $select .= "<option value= 'E'>Secretaria</option>";
            } 
            else if($grupo['usu_nivel_acceso'] == 'F'){
                $select .= "<option value= 'F'>Secretaria Firmas</option>";
            }
            else if($grupo['usu_nivel_acceso'] == 'J'){
                $select .= "<option value= 'J'>Jefe / Cordinador</option>";
            }              
        }

        $res->html = $select;

    }

}
catch(Exception $e){
    $res->error = 'Error:'.$e->getMessage();
}
catch(PDOException $e){
    $res->error = 'Error'.$e->getMessage();
}

echo json_encode($res);
?>