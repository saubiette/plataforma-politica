$(document).ready(function(){

    localStorage.clear();

    $("#formLogin").on("submit",function(){
          
        $('.loading').LoadingOverlay("show", {
                        image       : "./proyect/images/loading.gif",
                        fontawesome : ""
        });     

        var blnGrupo = $("#usu_codigo_depende").prop("disabled");
        
        if(blnGrupo == true){
            swal("No tiene ninguna empresa asignada.", "Revisa su login nuevamente", "warning");
            $('.loading').LoadingOverlay("hide");
              return false;
        }              
              
        $.post("./php/validar_login.php", $( "#formLogin" ).serialize(),function(res){       
            if(res.entro == 'S'){
                window.location=('./proyect/mdi');
            }
            else if(res.entro == 'N'){                
                swal("Contraseña incorrecta", "Revisa e intenta nuevamente", "warning");
                $('.loading').LoadingOverlay("hide");
            }
            else{
                alert(res.entro);
            }
            
        },"json")
        .fail(function(xhr,textStatus,errorThrown){
            alert(xhr.responseText);
        });            
      
        return false;
    });

    document.oncontextmenu = function(){return false;}

    $("#login").focusout(function(){

        $("#usu_codigo_depende").html("");
        $("#usu_codigo_depende").prop("disabled",true);

        if( $.trim($("#login").val()) == "") return false;

        var login = $("#login").val().toLowerCase();

        if(login.search("admin") != -1){
            $("#usu_codigo_depende").prop("disabled",false);                
            $("#usu_codigo_depende").html("<option value='S'>Super Administrador</opction>");            
            return false;
        }
        
        $.post("./php/validar_tipo_usuario.php", {login: $("#login").val()}, function(res){

            if(res.existeGrupo == true){
                $("#usu_codigo_depende").prop("disabled",false);                
                $("#usu_codigo_depende").html(res.html);
            }

        },"json")
        .fail(function(xhr, textStatus, errorThown){
            alert(xhr.responseText);
        });

    });

});