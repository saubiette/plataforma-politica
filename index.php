<?php require "./php/cerrarSesion.php"; ?>

<!DOCTYPE html>
<html>
  
  <head>
    
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>C | Inicia sesión</title>

    <!--Icono-->
    <link rel="shortcut icon" href="./proyect/images/icono.ico">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="./plugins/bootstrap/css/bootstrap.min.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="./dist/css/style.css">
    
    <!-- iCheck -->
    <link rel="stylesheet" href="./plugins/iCheck/square/blue.css">

    <!-- Sweetalert -->
    <link rel="stylesheet" href="./plugins/sweetalert/sweetalert.css">
    
  </head>
  
  <body class="hold-transition login-page">
    
    <div class="login-box">      
      <div class="login-logo">
        <img src="./proyect/images/logo.png">
      </div>
      <div class="login-box-body">
        <!-- <h3 class="login-box-msg"><b>Plataforma Politica</b></h3> -->
        <p class="login-box-msg"><strong>Inicia sesión</strong> </p>
        
        <form id="formLogin" method="POST">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="login" id="login" placeholder="Login" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="contrasenna" id="contrasenna" placeholder="Contraseña" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <lable for="usu_codigo_depende">Tipo de Usuario</label>
            <select class="form-control input-sm" id="usu_codigo_depende" name="usu_codigo_depende" require disabled>
            </select>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="recuerdame" id="recuerdame"> Recordar
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat loading">Ingresar</button>
            </div><!-- /.col -->
          </div>
        </form>       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="./plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
    <!-- Bootstrap 3.3.5 -->
    <script src="./plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- iCheck -->
    <script src="./plugins/iCheck/icheck.min.js"></script>

    <!-- loadingoverlay -->
    <script src="./plugins/loadingoverlay/loadingoverlay.js"></script>

    <!-- Sweetalert -->
    <script src="./plugins/sweetalert/sweetalert.min.js"></script>

    <!--Mi script-->
    <script src="./dist/js/script.min.js"></script>
    
    <script>
      $(function () {        
        $('input').iCheck({          
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '10%' // optional          
        });        
      });
    </script>
    
  </body>
</html>
